EESchema Schematic File Version 4
LIBS:newfile-cache
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема электрическая принципиальная"
Date ""
Rev ""
Comp "МГТУ им. Н.Э.Баумана ИУ6-74"
Comment1 "Проектирование звукового синтезатора"
Comment2 "Абдурахманова Р.А."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L my_Library1:DS-201 XP1
U 1 1 5DD4FB5D
P 2200 9050
F 0 "XP1" H 2233 9165 50  0000 C CNN
F 1 "DS-201" H 2233 9074 50  0000 C CNN
F 2 "Connector:Banana_Jack_2Pin" H 2200 9050 50  0001 C CNN
F 3 "" H 2200 9050 50  0001 C CNN
	1    2200 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 9300 2600 9300
Wire Wire Line
	2600 9300 2600 9100
Wire Wire Line
	2950 9100 2950 9200
Wire Wire Line
	2500 9450 2600 9450
Wire Wire Line
	2600 9450 2600 9650
$Comp
L Device:CP C1
U 1 1 5DD598A2
P 2950 9350
F 0 "C1" H 3068 9396 50  0000 L CNN
F 1 "100 u" H 3068 9305 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.3" H 2988 9200 50  0001 C CNN
F 3 "~" H 2950 9350 50  0001 C CNN
	1    2950 9350
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal ZQ1
U 1 1 5DD61E2D
P 5100 3200
F 0 "ZQ1" H 5100 3468 50  0000 C CNN
F 1 "4 MHz" H 5100 3377 50  0000 C CNN
F 2 "Crystal:Crystal_HC49-U_Horizontal_1EP_style1" H 5100 3200 50  0001 C CNN
F 3 "~" H 5100 3200 50  0001 C CNN
	1    5100 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5DD62AD6
P 4800 3550
F 0 "C3" H 4915 3596 50  0000 L CNN
F 1 "22 p" H 4915 3505 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4838 3400 50  0001 C CNN
F 3 "~" H 4800 3550 50  0001 C CNN
	1    4800 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5DD6322B
P 5350 3550
F 0 "C5" H 5465 3596 50  0000 L CNN
F 1 "22 p" H 5465 3505 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 5388 3400 50  0001 C CNN
F 3 "~" H 5350 3550 50  0001 C CNN
	1    5350 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3400 4800 3200
Wire Wire Line
	4800 3200 4950 3200
Wire Wire Line
	5250 3200 5350 3200
Wire Wire Line
	5350 3200 5350 3400
Wire Wire Line
	5350 2600 5350 3200
Connection ~ 5350 3200
Wire Wire Line
	4800 2800 4800 3200
Connection ~ 4800 3200
$Comp
L Interface_UART:MAX232 DD3
U 1 1 5DD68D27
P 11450 4150
F 0 "DD3" H 11450 5531 50  0000 C CNN
F 1 "MAX232" H 11450 5440 50  0000 C CNN
F 2 "Transformer_SMD:Transformer_Ethernet_Bel_S558-5999-T7-F" H 11500 3100 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 11450 4250 50  0001 C CNN
	1    11450 4150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR014
U 1 1 5DD85D51
P 11700 2850
F 0 "#PWR014" H 11700 2700 50  0001 C CNN
F 1 "+5V" H 11715 3023 50  0000 C CNN
F 2 "" H 11700 2850 50  0001 C CNN
F 3 "" H 11700 2850 50  0001 C CNN
	1    11700 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	11700 2850 11700 2900
Wire Wire Line
	11700 2950 11450 2950
$Comp
L Device:CP C6
U 1 1 5DD875D5
P 10350 3400
F 0 "C6" H 10468 3446 50  0000 L CNN
F 1 "1 u" H 10468 3355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 10388 3250 50  0001 C CNN
F 3 "~" H 10350 3400 50  0001 C CNN
	1    10350 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C7
U 1 1 5DD88A12
P 12650 3400
F 0 "C7" H 12768 3446 50  0000 L CNN
F 1 "1 u" H 12768 3355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 12688 3250 50  0001 C CNN
F 3 "~" H 12650 3400 50  0001 C CNN
	1    12650 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C11
U 1 1 5DD89408
P 13300 3150
F 0 "C11" H 13418 3196 50  0000 L CNN
F 1 "1 u" H 13418 3105 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 13338 3000 50  0001 C CNN
F 3 "~" H 13300 3150 50  0001 C CNN
	1    13300 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C10
U 1 1 5DD8A4ED
P 13250 3900
F 0 "C10" H 13368 3946 50  0000 L CNN
F 1 "1 u" H 13368 3855 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 13288 3750 50  0001 C CNN
F 3 "~" H 13250 3900 50  0001 C CNN
	1    13250 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	12250 3250 12650 3250
Wire Wire Line
	12650 3550 12250 3550
Wire Wire Line
	10650 3250 10350 3250
Wire Wire Line
	10650 3550 10350 3550
Wire Wire Line
	12250 3750 12800 3750
Wire Wire Line
	12800 3750 12800 3600
Wire Wire Line
	12800 3600 13300 3600
Wire Wire Line
	13300 3600 13300 3300
Wire Wire Line
	13300 3000 13300 2900
Wire Wire Line
	13300 2900 11700 2900
Connection ~ 11700 2900
Wire Wire Line
	11700 2900 11700 2950
Wire Wire Line
	12250 4050 12950 4050
Wire Wire Line
	12950 4050 12950 3650
Wire Wire Line
	12950 3650 13250 3650
Wire Wire Line
	13250 3650 13250 3750
$Comp
L my_Library1:RS-232 XP3
U 1 1 5DD9B81A
P 14400 4000
F 0 "XP3" H 14350 4050 50  0000 L CNN
F 1 "RS-232" H 14300 3950 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Male_Vertical_P2.77x2.84mm" H 14400 4000 50  0001 C CNN
F 3 "" H 14400 4000 50  0001 C CNN
	1    14400 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 5250 13900 5250
Wire Wire Line
	13900 5250 13900 5400
Wire Wire Line
	13900 5400 14000 5400
Wire Wire Line
	14000 5550 13900 5550
Wire Wire Line
	14000 4350 13900 4350
Wire Wire Line
	14000 5100 13900 5100
Wire Wire Line
	14000 4800 13900 4800
Wire Wire Line
	14000 4950 13650 4950
$Comp
L Device:C C9
U 1 1 5DDB07DC
P 13100 5200
F 0 "C9" H 13215 5246 50  0000 L CNN
F 1 "1.2 n" H 13215 5155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 13138 5050 50  0001 C CNN
F 3 "~" H 13100 5200 50  0001 C CNN
	1    13100 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5DDB1099
P 12650 5200
F 0 "C8" H 12765 5246 50  0000 L CNN
F 1 "1.2 n" H 12765 5155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L20.0mm_D13.0mm_P26.00mm_Horizontal" H 12688 5050 50  0001 C CNN
F 3 "~" H 12650 5200 50  0001 C CNN
	1    12650 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5DDB24C3
P 12600 4450
F 0 "R11" V 12393 4450 50  0000 C CNN
F 1 "470" V 12484 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 12530 4450 50  0001 C CNN
F 3 "~" H 12600 4450 50  0001 C CNN
	1    12600 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5DDB2E05
P 12600 4850
F 0 "R12" V 12393 4850 50  0000 C CNN
F 1 "470" V 12484 4850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 12530 4850 50  0001 C CNN
F 3 "~" H 12600 4850 50  0001 C CNN
	1    12600 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	12450 4450 12250 4450
Wire Wire Line
	13700 4450 13700 4500
Wire Wire Line
	13700 4500 14000 4500
Wire Wire Line
	12750 4450 12900 4450
Wire Wire Line
	12450 4850 12250 4850
Wire Wire Line
	12750 4850 13100 4850
Wire Wire Line
	13700 4850 13700 4650
Wire Wire Line
	13700 4650 14000 4650
Wire Wire Line
	12650 5050 12650 5000
Wire Wire Line
	12650 5000 12900 5000
Wire Wire Line
	12900 5000 12900 4450
Connection ~ 12900 4450
Wire Wire Line
	12900 4450 13700 4450
Wire Wire Line
	13100 5050 13100 4850
Connection ~ 13100 4850
Wire Wire Line
	13100 4850 13700 4850
$Comp
L my_Library1:IDC-06MS XP2
U 1 1 5DDDDB61
P 6750 6700
F 0 "XP2" H 6700 6750 50  0000 L CNN
F 1 "IDC-06MS" H 6600 6650 50  0000 L CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Horizontal" H 6750 6700 50  0001 C CNN
F 3 "" H 6750 6700 50  0001 C CNN
	1    6750 6700
	1    0    0    -1  
$EndComp
Entry Wire Line
	5900 7600 6000 7500
Entry Wire Line
	5900 7450 6000 7350
Entry Wire Line
	5900 7150 6000 7050
Wire Wire Line
	6350 7050 6000 7050
Wire Wire Line
	6350 7350 6000 7350
Wire Wire Line
	6350 7500 6000 7500
$Comp
L power:+5V #PWR010
U 1 1 5DDF80B6
P 6250 6650
F 0 "#PWR010" H 6250 6500 50  0001 C CNN
F 1 "+5V" H 6265 6823 50  0000 C CNN
F 2 "" H 6250 6650 50  0001 C CNN
F 3 "" H 6250 6650 50  0001 C CNN
	1    6250 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 7800 6150 7800
Wire Wire Line
	6150 7800 6150 7950
Entry Wire Line
	7650 2700 7550 2600
Entry Wire Line
	7650 2800 7550 2700
Entry Wire Line
	7650 2900 7550 2800
Entry Wire Line
	7650 2600 7550 2500
Entry Wire Line
	7650 2500 7550 2400
Wire Bus Line
	4450 1700 7650 1700
$Comp
L Switch:SW_Push SW1
U 1 1 5DEA6633
P 2700 2600
F 0 "SW1" H 2700 2885 50  0000 C CNN
F 1 "\"Do\"" H 2700 2794 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 2800 50  0001 C CNN
F 3 "~" H 2700 2800 50  0001 C CNN
	1    2700 2600
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5DEA7345
P 2700 3050
F 0 "SW2" H 2700 3335 50  0000 C CNN
F 1 "\"Re\"" H 2700 3244 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 3250 50  0001 C CNN
F 3 "~" H 2700 3250 50  0001 C CNN
	1    2700 3050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5DEA7B72
P 2700 3500
F 0 "SW3" H 2700 3785 50  0000 C CNN
F 1 "\"Mi\"" H 2700 3694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 3700 50  0001 C CNN
F 3 "~" H 2700 3700 50  0001 C CNN
	1    2700 3500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5DEA830E
P 2700 4000
F 0 "SW4" H 2700 4285 50  0000 C CNN
F 1 "\"Fa\"" H 2700 4194 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 4200 50  0001 C CNN
F 3 "~" H 2700 4200 50  0001 C CNN
	1    2700 4000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5DEA878D
P 2700 4450
F 0 "SW5" H 2700 4735 50  0000 C CNN
F 1 "\"Sol\"" H 2700 4644 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 4650 50  0001 C CNN
F 3 "~" H 2700 4650 50  0001 C CNN
	1    2700 4450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5DEA8B53
P 2700 4850
F 0 "SW6" H 2700 5135 50  0000 C CNN
F 1 "\"La\"" H 2700 5044 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 5050 50  0001 C CNN
F 3 "~" H 2700 5050 50  0001 C CNN
	1    2700 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 2600 2150 2600
Wire Wire Line
	2150 2600 2150 3050
Wire Wire Line
	2500 3050 2150 3050
Connection ~ 2150 3050
Wire Wire Line
	2150 3050 2150 3500
Wire Wire Line
	2500 3500 2150 3500
Connection ~ 2150 3500
Wire Wire Line
	2150 3500 2150 4000
Wire Wire Line
	2500 4000 2150 4000
Connection ~ 2150 4000
Wire Wire Line
	2150 4000 2150 4450
Wire Wire Line
	2500 4450 2150 4450
Connection ~ 2150 4450
Wire Wire Line
	2150 4450 2150 4850
Wire Wire Line
	2500 4850 2150 4850
Connection ~ 2150 4850
$Comp
L Device:R R2
U 1 1 5DEE3953
P 3550 2600
F 0 "R2" V 3343 2600 50  0000 C CNN
F 1 "200" V 3434 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 2600 50  0001 C CNN
F 3 "~" H 3550 2600 50  0001 C CNN
	1    3550 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5DEEA2DB
P 3550 3050
F 0 "R3" V 3343 3050 50  0000 C CNN
F 1 "200" V 3434 3050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 3050 50  0001 C CNN
F 3 "~" H 3550 3050 50  0001 C CNN
	1    3550 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5DEEA711
P 3550 3500
F 0 "R4" V 3343 3500 50  0000 C CNN
F 1 "200" V 3434 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 3500 50  0001 C CNN
F 3 "~" H 3550 3500 50  0001 C CNN
	1    3550 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5DEEAB8F
P 3550 4000
F 0 "R5" V 3343 4000 50  0000 C CNN
F 1 "200" V 3434 4000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 4000 50  0001 C CNN
F 3 "~" H 3550 4000 50  0001 C CNN
	1    3550 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5DEEB207
P 3550 4450
F 0 "R6" V 3343 4450 50  0000 C CNN
F 1 "200" V 3434 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 4450 50  0001 C CNN
F 3 "~" H 3550 4450 50  0001 C CNN
	1    3550 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5DEEB595
P 3550 4850
F 0 "R7" V 3343 4850 50  0000 C CNN
F 1 "200" V 3434 4850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 4850 50  0001 C CNN
F 3 "~" H 3550 4850 50  0001 C CNN
	1    3550 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4850 2900 4850
Wire Wire Line
	2900 4450 3400 4450
Wire Wire Line
	3400 4000 2900 4000
Wire Wire Line
	2900 3500 3400 3500
Wire Wire Line
	2900 3050 3400 3050
Wire Wire Line
	2900 2600 3400 2600
Entry Wire Line
	4350 2600 4450 2700
Wire Wire Line
	4350 2600 3700 2600
Entry Wire Line
	4350 3050 4450 3150
Wire Wire Line
	4350 3050 3700 3050
Entry Wire Line
	4350 3500 4450 3600
Wire Wire Line
	4350 3500 3700 3500
Entry Wire Line
	4350 4000 4450 4100
Wire Wire Line
	4350 4000 3700 4000
Entry Wire Line
	4350 4450 4450 4550
Wire Wire Line
	4350 4450 3700 4450
Entry Wire Line
	4350 4850 4450 4950
Wire Wire Line
	4350 4850 3700 4850
Entry Wire Line
	4450 2500 4550 2400
Entry Wire Line
	4450 7750 4550 7650
Text Label 4250 3050 0    50   ~ 0
m1
Text Label 4250 3500 0    50   ~ 0
m2
Text Label 4250 4000 0    50   ~ 0
m3
Text Label 4250 4450 0    50   ~ 0
m4
Text Label 4250 2600 0    50   ~ 0
m0
Text Label 4550 7650 0    50   ~ 0
reset
Text Label 7450 2400 0    50   ~ 0
m0
Text Label 7450 2500 0    50   ~ 0
m1
Text Label 7450 2600 0    50   ~ 0
m2
Text Label 7450 2700 0    50   ~ 0
m3
Text Label 7450 2800 0    50   ~ 0
m4
Text Label 7450 2900 0    50   ~ 0
m5
Entry Wire Line
	7200 3800 7300 3900
Entry Wire Line
	7200 3900 7300 4000
Entry Wire Line
	7200 4000 7300 4100
Text Label 7000 3800 0    50   ~ 0
MOSI
Text Label 7000 3900 0    50   ~ 0
MISO
Text Label 7000 4000 0    50   ~ 0
SCK
Text Label 6000 7050 0    50   ~ 0
MISO
Text Label 6000 7350 0    50   ~ 0
SCK
Text Label 6000 7500 0    50   ~ 0
MOSI
Wire Wire Line
	6250 6650 6250 7200
Wire Wire Line
	6250 7200 6350 7200
Text Label 4550 2400 0    50   ~ 0
reset
NoConn ~ 12250 4250
NoConn ~ 12250 4650
NoConn ~ 13900 4350
NoConn ~ 13900 4800
NoConn ~ 13900 5100
NoConn ~ 13900 5550
NoConn ~ 10650 4650
NoConn ~ 10650 4250
$Comp
L power:+5V #PWR08
U 1 1 5DD5FD3F
P 6650 2000
F 0 "#PWR08" H 6650 1850 50  0001 C CNN
F 1 "+5V" H 6665 2173 50  0000 C CNN
F 2 "" H 6650 2000 50  0001 C CNN
F 3 "" H 6650 2000 50  0001 C CNN
	1    6650 2000
	1    0    0    -1  
$EndComp
Wire Bus Line
	7300 6400 5900 6400
Wire Wire Line
	6350 7650 4550 7650
Entry Wire Line
	7650 3000 7550 2900
Wire Wire Line
	6250 7950 6150 7950
Wire Wire Line
	5300 4000 5300 3700
Wire Wire Line
	5300 3700 5350 3700
Wire Wire Line
	11400 5350 11450 5350
Wire Wire Line
	4800 4000 4800 3700
Wire Wire Line
	6250 8250 6250 7950
Wire Wire Line
	13100 5650 13100 5350
Wire Wire Line
	13650 5250 13650 4950
Wire Wire Line
	13250 4050 13250 4150
NoConn ~ 11400 5350
$Comp
L power:GND #PWR0101
U 1 1 5E039DB1
P 12650 5700
F 0 "#PWR0101" H 12650 5450 50  0001 C CNN
F 1 "GND" H 12655 5527 50  0000 C CNN
F 2 "" H 12650 5700 50  0001 C CNN
F 3 "" H 12650 5700 50  0001 C CNN
	1    12650 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	12650 5350 12650 5700
$Comp
L power:GND #PWR0102
U 1 1 5E06297B
P 13100 5650
F 0 "#PWR0102" H 13100 5400 50  0001 C CNN
F 1 "GND" H 13105 5477 50  0000 C CNN
F 2 "" H 13100 5650 50  0001 C CNN
F 3 "" H 13100 5650 50  0001 C CNN
	1    13100 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E071AF0
P 13650 5250
F 0 "#PWR0103" H 13650 5000 50  0001 C CNN
F 1 "GND" H 13655 5077 50  0000 C CNN
F 2 "" H 13650 5250 50  0001 C CNN
F 3 "" H 13650 5250 50  0001 C CNN
	1    13650 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5E080E36
P 6250 8250
F 0 "#PWR0104" H 6250 8000 50  0001 C CNN
F 1 "GND" H 6255 8077 50  0000 C CNN
F 2 "" H 6250 8250 50  0001 C CNN
F 3 "" H 6250 8250 50  0001 C CNN
	1    6250 8250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5E0A6A2B
P 2150 7000
F 0 "#PWR0106" H 2150 6750 50  0001 C CNN
F 1 "GND" H 2155 6827 50  0000 C CNN
F 2 "" H 2150 7000 50  0001 C CNN
F 3 "" H 2150 7000 50  0001 C CNN
	1    2150 7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E0B5CC5
P 4800 4000
F 0 "#PWR0107" H 4800 3750 50  0001 C CNN
F 1 "GND" H 4805 3827 50  0000 C CNN
F 2 "" H 4800 4000 50  0001 C CNN
F 3 "" H 4800 4000 50  0001 C CNN
	1    4800 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E0BD62A
P 5300 4000
F 0 "#PWR0108" H 5300 3750 50  0001 C CNN
F 1 "GND" H 5305 3827 50  0000 C CNN
F 2 "" H 5300 4000 50  0001 C CNN
F 3 "" H 5300 4000 50  0001 C CNN
	1    5300 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E0CC708
P 13250 4150
F 0 "#PWR0109" H 13250 3900 50  0001 C CNN
F 1 "GND" H 13255 3977 50  0000 C CNN
F 2 "" H 13250 4150 50  0001 C CNN
F 3 "" H 13250 4150 50  0001 C CNN
	1    13250 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 9650 2950 9650
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E011221
P 1950 10200
F 0 "#FLG0101" H 1950 10275 50  0001 C CNN
F 1 "PWR_FLAG" H 1950 10373 50  0000 C CNN
F 2 "" H 1950 10200 50  0001 C CNN
F 3 "~" H 1950 10200 50  0001 C CNN
	1    1950 10200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5E0121AE
P 1950 10450
F 0 "#PWR0111" H 1950 10200 50  0001 C CNN
F 1 "GND" H 1955 10277 50  0000 C CNN
F 2 "" H 1950 10450 50  0001 C CNN
F 3 "" H 1950 10450 50  0001 C CNN
	1    1950 10450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 10200 1950 10450
$Comp
L MCU_Microchip_ATmega:ATmega8515-16AU DD2
U 1 1 5E022FAB
P 6150 4100
F 0 "DD2" H 6150 6281 50  0000 C CNN
F 1 "ATmega8515-16AU" H 6150 6190 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 6150 4100 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2512.pdf" H 6150 4100 50  0001 C CNN
	1    6150 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2800 4800 2800
Wire Wire Line
	5350 2600 5550 2600
Wire Wire Line
	5550 2400 4550 2400
Wire Wire Line
	6750 2400 7550 2400
Wire Wire Line
	6750 2500 7550 2500
Wire Wire Line
	6750 2600 7550 2600
Wire Wire Line
	6750 2700 7550 2700
Wire Wire Line
	6750 2800 7550 2800
Wire Wire Line
	6750 2900 7550 2900
Entry Wire Line
	7550 3000 7650 3100
Entry Wire Line
	7550 3100 7650 3200
Wire Wire Line
	6750 3000 7550 3000
Wire Wire Line
	6750 3100 7550 3100
Text Label 7450 3000 0    50   ~ 0
m6
Text Label 7450 3100 0    50   ~ 0
m7
Wire Wire Line
	6150 2100 6650 2100
Wire Wire Line
	6650 2100 6650 2000
NoConn ~ 5550 5100
NoConn ~ 5550 5200
NoConn ~ 5550 5300
$Comp
L Switch:SW_Push SW7
U 1 1 5E0CAB16
P 2700 2000
F 0 "SW7" H 2700 2285 50  0000 C CNN
F 1 "\"Reset\"" H 2700 2194 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 2200 50  0001 C CNN
F 3 "~" H 2700 2200 50  0001 C CNN
	1    2700 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 2000 2150 2000
$Comp
L Device:R R8
U 1 1 5E0CAB1D
P 3550 2000
F 0 "R8" V 3343 2000 50  0000 C CNN
F 1 "200" V 3434 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 2000 50  0001 C CNN
F 3 "~" H 3550 2000 50  0001 C CNN
	1    3550 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	2900 2000 3150 2000
Wire Wire Line
	4350 2000 3700 2000
Wire Wire Line
	2150 2000 2150 2600
Connection ~ 2150 2600
Entry Wire Line
	4350 2000 4450 2100
$Comp
L Switch:SW_Push SW8
U 1 1 5E0D78CE
P 2700 5250
F 0 "SW8" H 2700 5535 50  0000 C CNN
F 1 "\"Si\"" H 2700 5444 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 5450 50  0001 C CNN
F 3 "~" H 2700 5450 50  0001 C CNN
	1    2700 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5E0D78D5
P 3550 5250
F 0 "R9" V 3343 5250 50  0000 C CNN
F 1 "200" V 3434 5250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 5250 50  0001 C CNN
F 3 "~" H 3550 5250 50  0001 C CNN
	1    3550 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 5250 2900 5250
Wire Wire Line
	4350 5250 3700 5250
$Comp
L Switch:SW_Push SW9
U 1 1 5E0DEC00
P 2700 5750
F 0 "SW9" H 2700 6035 50  0000 C CNN
F 1 "\"Do\"" H 2700 5944 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 5950 50  0001 C CNN
F 3 "~" H 2700 5950 50  0001 C CNN
	1    2700 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5E0DEC07
P 3550 5750
F 0 "R10" V 3343 5750 50  0000 C CNN
F 1 "200" V 3434 5750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 5750 50  0001 C CNN
F 3 "~" H 3550 5750 50  0001 C CNN
	1    3550 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 5750 2900 5750
Wire Wire Line
	4350 5750 3700 5750
Wire Wire Line
	2150 4850 2150 5250
Wire Wire Line
	2500 5250 2150 5250
Connection ~ 2150 5250
Wire Wire Line
	2150 5250 2150 5750
Wire Wire Line
	2500 5750 2150 5750
Connection ~ 2150 5750
Wire Wire Line
	2150 5750 2150 7000
Entry Wire Line
	4350 5250 4450 5350
Entry Wire Line
	4350 5750 4450 5850
Text Label 4150 2000 0    50   ~ 0
reset
Text Label 4250 4850 0    50   ~ 0
m5
Text Label 4250 5250 0    50   ~ 0
m6
Text Label 4250 5750 0    50   ~ 0
m7
$Comp
L Device:R R1
U 1 1 5E10634E
P 3150 1550
F 0 "R1" H 3220 1596 50  0000 L CNN
F 1 "1K" H 3220 1505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3080 1550 50  0001 C CNN
F 3 "~" H 3150 1550 50  0001 C CNN
	1    3150 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 1700 3150 2000
Connection ~ 3150 2000
Wire Wire Line
	3150 2000 3400 2000
$Comp
L power:+5V #PWR0112
U 1 1 5E10D494
P 3500 1250
F 0 "#PWR0112" H 3500 1100 50  0001 C CNN
F 1 "+5V" V 3515 1378 50  0000 L CNN
F 2 "" H 3500 1250 50  0001 C CNN
F 3 "" H 3500 1250 50  0001 C CNN
	1    3500 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 1400 3150 1250
Wire Wire Line
	3150 1250 3500 1250
Wire Wire Line
	6750 3800 7200 3800
Wire Wire Line
	6750 3900 7200 3900
Wire Wire Line
	6750 4000 7200 4000
NoConn ~ 6750 5500
NoConn ~ 6750 5700
NoConn ~ 6750 5800
Entry Wire Line
	7600 5200 7700 5300
Entry Wire Line
	7600 5300 7700 5400
Entry Wire Line
	7600 5400 7700 5500
Entry Wire Line
	7600 5600 7700 5700
Wire Wire Line
	6750 5200 7600 5200
Wire Wire Line
	6750 5300 7600 5300
Wire Wire Line
	6750 5400 7600 5400
Wire Wire Line
	6750 5600 7600 5600
Text Label 7500 5200 0    50   ~ 0
TXD
Text Label 7500 5300 0    50   ~ 0
pl
Text Label 7500 5400 0    50   ~ 0
wr
Text Label 7500 5600 0    50   ~ 0
ls1
Entry Wire Line
	7700 5050 7800 5150
Wire Wire Line
	7800 5150 10050 5150
Wire Wire Line
	10050 4450 10650 4450
Wire Wire Line
	10050 4450 10050 5150
Wire Wire Line
	10300 5350 10300 4850
Wire Wire Line
	10300 4850 10650 4850
Text Label 7800 5150 0    50   ~ 0
TXD
$Comp
L Switch:SW_Push SW10
U 1 1 5E1E92AA
P 9100 5950
F 0 "SW10" H 9100 6235 50  0000 C CNN
F 1 "\"Play\"" H 9100 6144 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9100 6150 50  0001 C CNN
F 3 "~" H 9100 6150 50  0001 C CNN
	1    9100 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5E1E92B0
P 8400 5950
F 0 "R13" V 8193 5950 50  0000 C CNN
F 1 "200" V 8284 5950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8330 5950 50  0001 C CNN
F 3 "~" H 8400 5950 50  0001 C CNN
	1    8400 5950
	0    1    1    0   
$EndComp
Entry Wire Line
	7700 5850 7800 5950
Wire Wire Line
	7800 5950 8250 5950
Wire Wire Line
	8550 5950 8900 5950
$Comp
L Switch:SW_Push SW11
U 1 1 5E22E8C4
P 9100 6500
F 0 "SW11" H 9100 6785 50  0000 C CNN
F 1 "\"Write\"" H 9100 6694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9100 6700 50  0001 C CNN
F 3 "~" H 9100 6700 50  0001 C CNN
	1    9100 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5E22E8CA
P 8400 6500
F 0 "R14" V 8193 6500 50  0000 C CNN
F 1 "200" V 8284 6500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8330 6500 50  0001 C CNN
F 3 "~" H 8400 6500 50  0001 C CNN
	1    8400 6500
	0    1    1    0   
$EndComp
Wire Wire Line
	7800 6500 8250 6500
Wire Wire Line
	8550 6500 8900 6500
Entry Wire Line
	7700 6400 7800 6500
$Comp
L power:GND #PWR0113
U 1 1 5E23715E
P 9650 6600
F 0 "#PWR0113" H 9650 6350 50  0001 C CNN
F 1 "GND" H 9655 6427 50  0000 C CNN
F 2 "" H 9650 6600 50  0001 C CNN
F 3 "" H 9650 6600 50  0001 C CNN
	1    9650 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5950 9650 5950
Wire Wire Line
	9650 5950 9650 6500
Wire Wire Line
	9300 6500 9650 6500
Connection ~ 9650 6500
Wire Wire Line
	9650 6500 9650 6600
Text Label 7800 5950 0    50   ~ 0
pl
Text Label 7800 6500 0    50   ~ 0
wr
$Comp
L Device:Speaker LS1
U 1 1 5E250B94
P 9100 7150
F 0 "LS1" H 9270 7146 50  0000 L CNN
F 1 "Speaker" H 9270 7055 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_TDK_PS1240P02BT_D12.2mm_H6.5mm" H 9100 6950 50  0001 C CNN
F 3 "~" H 9090 7100 50  0001 C CNN
	1    9100 7150
	1    0    0    -1  
$EndComp
Entry Wire Line
	7700 7050 7800 7150
Wire Wire Line
	7800 7150 8900 7150
$Comp
L power:GND #PWR0114
U 1 1 5E262C45
P 8700 7450
F 0 "#PWR0114" H 8700 7200 50  0001 C CNN
F 1 "GND" H 8705 7277 50  0000 C CNN
F 2 "" H 8700 7450 50  0001 C CNN
F 3 "" H 8700 7450 50  0001 C CNN
	1    8700 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 7250 8700 7250
Wire Wire Line
	8700 7250 8700 7450
Text Label 7800 7150 0    50   ~ 0
ls1
NoConn ~ 6750 4200
NoConn ~ 6750 4300
NoConn ~ 6750 4400
NoConn ~ 6750 4500
NoConn ~ 6750 4600
NoConn ~ 6750 4700
NoConn ~ 6750 4800
NoConn ~ 6750 4900
NoConn ~ 6750 3300
NoConn ~ 6750 3400
NoConn ~ 6750 3500
NoConn ~ 6750 3600
NoConn ~ 6750 3700
$Comp
L power:GND #PWR0115
U 1 1 5E2D87AA
P 6350 6150
F 0 "#PWR0115" H 6350 5900 50  0001 C CNN
F 1 "GND" H 6355 5977 50  0000 C CNN
F 2 "" H 6350 6150 50  0001 C CNN
F 3 "" H 6350 6150 50  0001 C CNN
	1    6350 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 6100 6150 6150
Wire Wire Line
	6150 6150 6350 6150
Wire Wire Line
	7800 5350 10300 5350
Entry Wire Line
	7700 5250 7800 5350
Entry Wire Line
	7600 5100 7700 5200
Text Label 7800 5350 0    50   ~ 0
RXD
Text Label 7500 5100 0    50   ~ 0
RXD
Wire Wire Line
	6750 5100 7600 5100
$Comp
L power:GND #PWR0110
U 1 1 5E0E4376
P 4650 9400
F 0 "#PWR0110" H 4650 9150 50  0001 C CNN
F 1 "GND" H 4655 9227 50  0000 C CNN
F 2 "" H 4650 9400 50  0001 C CNN
F 3 "" H 4650 9400 50  0001 C CNN
	1    4650 9400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E0978F2
P 4650 10050
F 0 "#PWR0105" H 4650 9800 50  0001 C CNN
F 1 "GND" H 4655 9877 50  0000 C CNN
F 2 "" H 4650 10050 50  0001 C CNN
F 3 "" H 4650 10050 50  0001 C CNN
	1    4650 10050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 9650 4650 10050
$Comp
L Device:CP C4
U 1 1 5DD5B013
P 5650 9400
F 0 "C4" H 5768 9446 50  0000 L CNN
F 1 "1 u" H 5768 9355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 5688 9250 50  0001 C CNN
F 3 "~" H 5650 9400 50  0001 C CNN
	1    5650 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5DD5A843
P 5150 9400
F 0 "C2" H 5268 9446 50  0000 L CNN
F 1 "10 u" H 5268 9355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.3" H 5188 9250 50  0001 C CNN
F 3 "~" H 5150 9400 50  0001 C CNN
	1    5150 9400
	1    0    0    -1  
$EndComp
Connection ~ 5650 9100
Wire Wire Line
	5650 8850 5650 9100
$Comp
L power:+5V #PWR05
U 1 1 5DD58EEC
P 5650 8850
F 0 "#PWR05" H 5650 8700 50  0001 C CNN
F 1 "+5V" H 5665 9023 50  0000 C CNN
F 2 "" H 5650 8850 50  0001 C CNN
F 3 "" H 5650 8850 50  0001 C CNN
	1    5650 8850
	1    0    0    -1  
$EndComp
Connection ~ 5150 9650
Wire Wire Line
	5650 9650 5150 9650
Wire Wire Line
	5650 9550 5650 9650
Connection ~ 5150 9100
Wire Wire Line
	5650 9100 5150 9100
Wire Wire Line
	5650 9250 5650 9100
Wire Wire Line
	5150 9650 4650 9650
Wire Wire Line
	5150 9550 5150 9650
Wire Wire Line
	5150 9100 5150 9250
Wire Wire Line
	4950 9100 5150 9100
$Comp
L Regulator_Linear:LM7805_TO220 DD1
U 1 1 5DD54E4A
P 4650 9100
F 0 "DD1" H 4650 9342 50  0000 C CNN
F 1 "LM7805_TO220" H 4650 9251 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4650 9325 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 4650 9050 50  0001 C CNN
	1    4650 9100
	1    0    0    -1  
$EndComp
$Comp
L Filter:BNX025 FL1
U 1 1 5E05BCBE
P 3750 9350
F 0 "FL1" H 3750 9675 50  0000 C CNN
F 1 "BNX025" H 3750 9584 50  0000 C CNN
F 2 "Filter:Filter_Murata_BNX025" H 3750 9150 50  0001 C CNN
F 3 "https://www.murata.com/en-us/products/productdetail.aspx?cate=luNoiseSupprFilteBlockType&partno=BNX025H01%23" V 3725 9325 50  0001 C CNN
	1    3750 9350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 9250 3350 9100
Wire Wire Line
	2600 9100 2950 9100
Connection ~ 2950 9100
Wire Wire Line
	2950 9100 3350 9100
Wire Wire Line
	4200 9450 4200 9650
Wire Wire Line
	4200 9650 4650 9650
Connection ~ 4650 9650
Wire Wire Line
	4150 9250 4150 9100
Wire Wire Line
	4150 9100 4350 9100
Wire Wire Line
	4200 9450 4150 9450
Wire Wire Line
	2950 9650 2950 9500
Wire Wire Line
	3350 9450 3350 9650
Wire Wire Line
	3350 9650 2950 9650
Wire Bus Line
	5900 6400 5900 7600
Wire Bus Line
	7300 3900 7300 6400
Wire Bus Line
	7650 1700 7650 3350
Wire Bus Line
	7700 5000 7700 7350
Wire Bus Line
	4450 1700 4450 7850
Connection ~ 2950 9650
$EndSCHEMATC
