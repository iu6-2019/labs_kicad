EESchema Schematic File Version 4
LIBS:dz_kicad-cache
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема приципиальная"
Date ""
Rev ""
Comp "МГТУ им. Н.Э. Баумана"
Comment1 ""
Comment2 "Булдин Н.В."
Comment3 "Хохлов С.А."
Comment4 ""
$EndDescr
$Comp
L Device:Resonator Y1
U 1 1 5E028CB5
P 6300 3500
F 0 "Y1" V 6254 3610 50  0000 L CNN
F 1 "4MHz" V 6345 3610 50  0000 L CNN
F 2 "Crystal:Resonator-3Pin_W6.0mm_H3.0mm" H 6275 3500 50  0001 C CNN
F 3 "~" H 6275 3500 50  0001 C CNN
	1    6300 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	6300 3350 7000 3350
Wire Wire Line
	7000 3350 7000 3400
Wire Wire Line
	6300 3650 7000 3650
Wire Wire Line
	7000 3650 7000 3600
$Comp
L power:GND #PWR0101
U 1 1 5E02CA46
P 6050 3600
F 0 "#PWR0101" H 6050 3350 50  0001 C CNN
F 1 "GND" H 6055 3427 50  0000 C CNN
F 2 "" H 6050 3600 50  0001 C CNN
F 3 "" H 6050 3600 50  0001 C CNN
	1    6050 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3500 6050 3500
Wire Wire Line
	6050 3500 6050 3600
Entry Wire Line
	8350 3200 8450 3100
Entry Wire Line
	8350 3300 8450 3200
Entry Wire Line
	8350 3400 8450 3300
Entry Wire Line
	8350 3500 8450 3400
Entry Wire Line
	8350 3600 8450 3500
Entry Wire Line
	8350 3700 8450 3600
Entry Wire Line
	8350 3800 8450 3700
Entry Wire Line
	8350 3900 8450 3800
Wire Bus Line
	6800 6950 8450 6950
Entry Wire Line
	8450 2850 8550 2950
Entry Wire Line
	8450 2950 8550 3050
Entry Wire Line
	8450 3050 8550 3150
Entry Wire Line
	8450 3150 8550 3250
Entry Wire Line
	8450 3250 8550 3350
Entry Wire Line
	8450 3350 8550 3450
Entry Wire Line
	8450 3450 8550 3550
Entry Wire Line
	8450 2750 8550 2850
Entry Wire Line
	8450 2250 8550 2350
Entry Wire Line
	8450 2350 8550 2450
Entry Wire Line
	8450 2450 8550 2550
Entry Wire Line
	6800 6200 6900 6100
Entry Wire Line
	6800 6000 6900 5900
Entry Wire Line
	6800 6100 6900 6000
Text Label 6900 5900 0    50   ~ 0
RS
Text Label 6900 6000 0    50   ~ 0
RW
Text Label 6900 6100 0    50   ~ 0
E
Wire Wire Line
	7000 6100 6900 6100
Wire Wire Line
	7000 6000 6900 6000
Wire Wire Line
	7000 5900 6900 5900
Wire Wire Line
	8550 2350 8650 2350
Wire Wire Line
	8550 2450 8650 2450
Wire Wire Line
	8550 2550 8650 2550
Wire Wire Line
	8550 2850 8650 2850
Wire Wire Line
	8550 2950 8650 2950
Wire Wire Line
	8550 3050 8650 3050
Wire Wire Line
	8550 3150 8650 3150
Wire Wire Line
	8550 3250 8650 3250
Wire Wire Line
	8550 3350 8650 3350
Wire Wire Line
	8550 3450 8650 3450
Wire Wire Line
	8550 3550 8650 3550
Text Label 8550 2350 0    50   ~ 0
E
Text Label 8550 2450 0    50   ~ 0
RW
Text Label 8550 2550 0    50   ~ 0
RS
Wire Wire Line
	8200 3200 8350 3200
Wire Wire Line
	8200 3300 8350 3300
Wire Wire Line
	8200 3400 8350 3400
Wire Wire Line
	8200 3500 8350 3500
Wire Wire Line
	8200 3600 8350 3600
Wire Wire Line
	8200 3700 8350 3700
Wire Wire Line
	8200 3800 8350 3800
Wire Wire Line
	8200 3900 8350 3900
Text Label 8250 3200 0    50   ~ 0
D0
Text Label 8250 3300 0    50   ~ 0
D1
Text Label 8250 3400 0    50   ~ 0
D2
Text Label 8250 3500 0    50   ~ 0
D3
Text Label 8250 3600 0    50   ~ 0
D4
Text Label 8250 3700 0    50   ~ 0
D5
Text Label 8250 3800 0    50   ~ 0
D6
Text Label 8250 3900 0    50   ~ 0
D7
$Comp
L power:GND #PWR0103
U 1 1 5E04D3A6
P 9050 3800
F 0 "#PWR0103" H 9050 3550 50  0001 C CNN
F 1 "GND" H 9055 3627 50  0000 C CNN
F 2 "" H 9050 3800 50  0001 C CNN
F 3 "" H 9050 3800 50  0001 C CNN
	1    9050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 3750 9050 3800
$Comp
L power:GND #PWR0105
U 1 1 5E06F9EA
P 9550 2800
F 0 "#PWR0105" H 9550 2550 50  0001 C CNN
F 1 "GND" H 9555 2627 50  0000 C CNN
F 2 "" H 9550 2800 50  0001 C CNN
F 3 "" H 9550 2800 50  0001 C CNN
	1    9550 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 2650 9550 2650
Wire Wire Line
	9550 2650 9550 2600
Wire Wire Line
	9450 2750 9550 2750
Wire Wire Line
	9550 2750 9550 2800
Entry Wire Line
	8450 4900 8350 5000
Wire Wire Line
	8200 5000 8350 5000
Wire Wire Line
	8200 5100 8350 5100
Wire Wire Line
	8200 5200 8350 5200
Wire Wire Line
	8200 5300 8350 5300
Entry Wire Line
	8350 5100 8450 5000
Entry Wire Line
	8350 5200 8450 5100
Entry Wire Line
	8350 5300 8450 5200
Text Label 8250 5000 0    50   ~ 0
EN
Text Label 8250 5100 0    50   ~ 0
E3
Text Label 8250 5200 0    50   ~ 0
E2
Text Label 8250 5300 0    50   ~ 0
E1
Entry Wire Line
	8450 4900 8550 5000
Wire Wire Line
	8550 5000 8700 5000
Wire Wire Line
	8700 5100 8550 5100
Wire Wire Line
	8700 5200 8550 5200
Entry Wire Line
	8450 5000 8550 5100
Entry Wire Line
	8450 5100 8550 5200
Text Label 8550 5000 0    50   ~ 0
E1
Text Label 8550 5100 0    50   ~ 0
E2
Text Label 8550 5200 0    50   ~ 0
E3
$Comp
L power:GND #PWR0106
U 1 1 5E0816CF
P 8650 5400
F 0 "#PWR0106" H 8650 5150 50  0001 C CNN
F 1 "GND" V 8550 5450 50  0000 R CNN
F 2 "" H 8650 5400 50  0001 C CNN
F 3 "" H 8650 5400 50  0001 C CNN
	1    8650 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	8700 5400 8650 5400
Wire Wire Line
	8700 5600 8550 5600
Wire Wire Line
	8700 5700 8600 5700
Entry Wire Line
	8450 5500 8550 5600
Text Label 8550 5600 0    50   ~ 0
EN
$Comp
L power:GND #PWR0109
U 1 1 5E08DBA9
P 9000 6050
F 0 "#PWR0109" H 9000 5800 50  0001 C CNN
F 1 "GND" V 8900 6100 50  0000 R CNN
F 2 "" H 9000 6050 50  0001 C CNN
F 3 "" H 9000 6050 50  0001 C CNN
	1    9000 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	9100 5900 9100 6050
Wire Wire Line
	9100 6050 9000 6050
Wire Wire Line
	9500 5000 9650 5000
Wire Wire Line
	9500 5100 9650 5100
Wire Wire Line
	9500 5200 9650 5200
Wire Wire Line
	9500 5300 9650 5300
Wire Wire Line
	9500 5400 9650 5400
$Comp
L LED:HDSP-4850_2 BAR1
U 1 1 5E09801F
P 10400 5400
F 0 "BAR1" H 10400 6067 50  0000 C CNN
F 1 "HDSP-4850_2" H 10400 5976 50  0000 C CNN
F 2 "Display:HDSP-4850" H 10400 4600 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 8400 5600 50  0001 C CNN
	1    10400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 5500 9650 5500
Wire Wire Line
	9500 5600 9650 5600
Wire Wire Line
	9500 5700 9650 5700
Wire Wire Line
	10050 5000 10200 5000
Wire Wire Line
	10050 5100 10200 5100
Wire Wire Line
	10050 5200 10200 5200
Wire Wire Line
	10050 5300 10200 5300
Wire Wire Line
	10050 5400 10200 5400
Wire Wire Line
	10050 5500 10200 5500
Wire Wire Line
	10050 5600 10200 5600
Wire Wire Line
	10050 5700 10200 5700
$Comp
L power:GND #PWR0110
U 1 1 5E0ACE77
P 10900 5800
F 0 "#PWR0110" H 10900 5550 50  0001 C CNN
F 1 "GND" V 10800 5850 50  0000 R CNN
F 2 "" H 10900 5800 50  0001 C CNN
F 3 "" H 10900 5800 50  0001 C CNN
	1    10900 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 5000 10900 5000
Wire Wire Line
	10900 5000 10900 5100
Wire Wire Line
	10600 5100 10900 5100
Connection ~ 10900 5100
Wire Wire Line
	10900 5100 10900 5200
Wire Wire Line
	10600 5200 10900 5200
Connection ~ 10900 5200
Wire Wire Line
	10900 5200 10900 5300
Wire Wire Line
	10600 5300 10900 5300
Connection ~ 10900 5300
Wire Wire Line
	10900 5300 10900 5400
Wire Wire Line
	10600 5400 10900 5400
Connection ~ 10900 5400
Wire Wire Line
	10900 5400 10900 5500
Wire Wire Line
	10600 5500 10900 5500
Connection ~ 10900 5500
Wire Wire Line
	10900 5500 10900 5600
Wire Wire Line
	10600 5600 10900 5600
Connection ~ 10900 5600
Wire Wire Line
	10900 5600 10900 5700
Wire Wire Line
	10600 5700 10900 5700
Connection ~ 10900 5700
Wire Wire Line
	10900 5700 10900 5800
$Comp
L Switch:SW_Push SW5
U 1 1 5E0CD854
P 11700 2750
F 0 "SW5" V 11654 2898 50  0000 L CNN
F 1 "LPCB208" V 11745 2898 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 11700 2950 50  0001 C CNN
F 3 "~" H 11700 2950 50  0001 C CNN
	1    11700 2750
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5E0D48C4
P 10650 3650
F 0 "SW2" V 10604 3798 50  0000 L CNN
F 1 "LPCB208" V 10695 3798 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 10650 3850 50  0001 C CNN
F 3 "~" H 10650 3850 50  0001 C CNN
	1    10650 3650
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5E0D51CA
P 11200 3650
F 0 "SW4" V 11154 3798 50  0000 L CNN
F 1 "LPCB208" V 11245 3798 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 11200 3850 50  0001 C CNN
F 3 "~" H 11200 3850 50  0001 C CNN
	1    11200 3650
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5E0D56A0
P 11700 3650
F 0 "SW6" V 11654 3798 50  0000 L CNN
F 1 "LPCB208" V 11745 3798 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 11700 3850 50  0001 C CNN
F 3 "~" H 11700 3850 50  0001 C CNN
	1    11700 3650
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5E0D5B14
P 12250 3650
F 0 "SW8" V 12204 3798 50  0000 L CNN
F 1 "LPCB208" V 12295 3798 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 12250 3850 50  0001 C CNN
F 3 "~" H 12250 3850 50  0001 C CNN
	1    12250 3650
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5E0D623D
P 10650 2750
F 0 "SW1" V 10604 2898 50  0000 L CNN
F 1 "LPCB208" V 10695 2898 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 10650 2950 50  0001 C CNN
F 3 "~" H 10650 2950 50  0001 C CNN
	1    10650 2750
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5E0D8824
P 11200 2750
F 0 "SW3" V 11154 2898 50  0000 L CNN
F 1 "LPCB208" V 11245 2898 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 11200 2950 50  0001 C CNN
F 3 "~" H 11200 2950 50  0001 C CNN
	1    11200 2750
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5E0DB5BE
P 12250 2750
F 0 "SW7" V 12204 2898 50  0000 L CNN
F 1 "LPCB208" V 12295 2898 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 12250 2950 50  0001 C CNN
F 3 "~" H 12250 2950 50  0001 C CNN
	1    12250 2750
	0    1    1    0   
$EndComp
Connection ~ 8450 4100
Entry Wire Line
	10650 4000 10750 4100
Entry Wire Line
	11200 4000 11300 4100
Entry Wire Line
	11700 4000 11800 4100
Entry Wire Line
	12250 4000 12350 4100
Entry Wire Line
	10650 3100 10750 3200
Entry Wire Line
	11200 3100 11300 3200
Entry Wire Line
	11700 3100 11800 3200
Entry Wire Line
	12250 3100 12350 3200
Text Label 10650 3950 0    50   ~ 0
B1
Text Label 11200 3950 0    50   ~ 0
B2
Text Label 11700 3950 0    50   ~ 0
B3
Text Label 12250 3950 0    50   ~ 0
B4
Entry Wire Line
	10550 3200 10650 3300
Entry Wire Line
	11200 3300 11100 3200
Entry Wire Line
	11700 3300 11600 3200
Entry Wire Line
	12250 3300 12150 3200
Entry Wire Line
	10650 2400 10550 2300
Entry Wire Line
	11200 2400 11100 2300
Entry Wire Line
	11700 2400 11600 2300
Entry Wire Line
	12250 2400 12150 2300
Text Label 10650 3050 0    50   ~ 0
B5
Text Label 11200 3050 0    50   ~ 0
B6
Text Label 11700 3050 0    50   ~ 0
B7
Text Label 12250 3050 0    50   ~ 0
B8
Wire Wire Line
	8200 4100 8350 4100
Entry Wire Line
	8350 4100 8450 4000
Text Label 8250 4100 0    50   ~ 0
C1
Text Label 10650 2450 0    50   ~ 0
C1
Wire Wire Line
	10650 3850 10650 4000
Wire Wire Line
	11200 3850 11200 4000
Wire Wire Line
	11700 3850 11700 4000
Wire Wire Line
	12250 3850 12250 4000
Wire Wire Line
	10650 3450 10650 3300
Wire Wire Line
	11200 3450 11200 3300
Wire Wire Line
	11700 3450 11700 3300
Wire Wire Line
	12250 3450 12250 3300
Text Label 10650 3350 0    50   ~ 0
C1
Text Label 11200 3350 0    50   ~ 0
C1
Text Label 11700 3350 0    50   ~ 0
C1
Text Label 12250 3350 0    50   ~ 0
C1
Text Label 11200 2450 0    50   ~ 0
C1
Text Label 11700 2450 0    50   ~ 0
C1
Text Label 12250 2450 0    50   ~ 0
C1
Wire Bus Line
	12800 2300 12800 3200
Connection ~ 12800 3200
Wire Bus Line
	12800 3200 12800 4100
Wire Wire Line
	10650 2950 10650 3100
Wire Wire Line
	11200 2950 11200 3100
Wire Wire Line
	11700 2950 11700 3100
Wire Wire Line
	12250 2950 12250 3100
Wire Wire Line
	12250 2400 12250 2550
Wire Wire Line
	11700 2400 11700 2550
Wire Wire Line
	11200 2400 11200 2550
Wire Wire Line
	10650 2400 10650 2550
Wire Wire Line
	8200 5900 8350 5900
Wire Wire Line
	8200 6000 8350 6000
Wire Wire Line
	8200 6100 8350 6100
Wire Wire Line
	8200 6200 8350 6200
Wire Wire Line
	8200 6300 8350 6300
Wire Wire Line
	8200 6400 8350 6400
Wire Wire Line
	8200 6500 8350 6500
Wire Wire Line
	8200 6600 8350 6600
Entry Wire Line
	8350 5900 8450 5800
Entry Wire Line
	8350 6000 8450 5900
Entry Wire Line
	8350 6100 8450 6000
Entry Wire Line
	8350 6200 8450 6100
Entry Wire Line
	8350 6300 8450 6200
Entry Wire Line
	8350 6400 8450 6300
Entry Wire Line
	8350 6500 8450 6400
Entry Wire Line
	8350 6600 8450 6500
Text Label 8250 5900 0    50   ~ 0
B1
Text Label 8250 6000 0    50   ~ 0
B2
Text Label 8250 6100 0    50   ~ 0
B3
Text Label 8250 6200 0    50   ~ 0
B4
Text Label 8250 6300 0    50   ~ 0
B5
Text Label 8250 6400 0    50   ~ 0
B6
Text Label 8250 6500 0    50   ~ 0
B7
Text Label 8250 6600 0    50   ~ 0
B8
$Comp
L Device:R_Pack08 RN1
U 1 1 5E099BD6
P 9850 5400
F 0 "RN1" V 9233 5400 50  0000 C CNN
F 1 "145" V 9324 5400 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" V 10325 5400 50  0001 C CNN
F 3 "~" H 9850 5400 50  0001 C CNN
	1    9850 5400
	0    1    1    0   
$EndComp
NoConn ~ 8200 4200
NoConn ~ 8200 4300
NoConn ~ 8200 4400
NoConn ~ 8200 4500
NoConn ~ 8200 5400
NoConn ~ 8200 5500
NoConn ~ 8200 5600
NoConn ~ 8200 5700
$Comp
L power:GND #PWR02
U 1 1 5E1ED5F1
P 7600 7050
F 0 "#PWR02" H 7600 6800 50  0001 C CNN
F 1 "GND" V 7500 7100 50  0000 R CNN
F 2 "" H 7600 7050 50  0001 C CNN
F 3 "" H 7600 7050 50  0001 C CNN
	1    7600 7050
	1    0    0    -1  
$EndComp
NoConn ~ 9450 2350
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E2045F8
P 4350 2950
F 0 "#FLG0101" H 4350 3025 50  0001 C CNN
F 1 "PWR_FLAG" H 4350 3123 50  0000 C CNN
F 2 "" H 4350 2950 50  0001 C CNN
F 3 "~" H 4350 2950 50  0001 C CNN
	1    4350 2950
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E2052BD
P 4750 2950
F 0 "#FLG0102" H 4750 3025 50  0001 C CNN
F 1 "PWR_FLAG" H 4750 3123 50  0000 C CNN
F 2 "" H 4750 2950 50  0001 C CNN
F 3 "~" H 4750 2950 50  0001 C CNN
	1    4750 2950
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0112
U 1 1 5E2057A1
P 4350 3100
F 0 "#PWR0112" H 4350 2950 50  0001 C CNN
F 1 "VCC" H 4368 3273 50  0000 C CNN
F 2 "" H 4350 3100 50  0001 C CNN
F 3 "" H 4350 3100 50  0001 C CNN
	1    4350 3100
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5E205F20
P 4750 3100
F 0 "#PWR0113" H 4750 2850 50  0001 C CNN
F 1 "GND" H 4755 2927 50  0000 C CNN
F 2 "" H 4750 3100 50  0001 C CNN
F 3 "" H 4750 3100 50  0001 C CNN
	1    4750 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2950 4350 3100
Wire Wire Line
	4750 2950 4750 3100
NoConn ~ 13650 5200
NoConn ~ 10600 5800
NoConn ~ 10600 5900
NoConn ~ 10200 5900
NoConn ~ 10200 5800
Entry Wire Line
	6700 4550 6800 4650
Entry Wire Line
	6700 4950 6800 5050
Entry Wire Line
	6700 5150 6800 5250
Wire Wire Line
	6050 4750 6550 4750
Wire Wire Line
	6050 4950 6700 4950
Wire Wire Line
	6050 5150 6700 5150
Text Label 6050 4950 0    50   ~ 0
SPI_SCK
Text Label 6050 5150 0    50   ~ 0
SPI_MOSI
Text Label 6250 5350 0    50   ~ 0
RST
Wire Wire Line
	6050 5550 6550 5550
Wire Wire Line
	6550 5550 6550 5650
$Comp
L power:GND #PWR07
U 1 1 5E0E9EFE
P 6550 5650
F 0 "#PWR07" H 6550 5400 50  0001 C CNN
F 1 "GND" H 6600 5500 50  0000 R CNN
F 2 "" H 6550 5650 50  0001 C CNN
F 3 "" H 6550 5650 50  0001 C CNN
	1    6550 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 7050 7600 6900
Wire Wire Line
	6550 4750 6550 4250
$Comp
L power:+5V #PWR06
U 1 1 5E114756
P 6550 4250
F 0 "#PWR06" H 6550 4100 50  0001 C CNN
F 1 "+5V" H 6565 4423 50  0000 C CNN
F 2 "" H 6550 4250 50  0001 C CNN
F 3 "" H 6550 4250 50  0001 C CNN
	1    6550 4250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 5E114E7A
P 6000 3200
F 0 "SW9" H 5950 3450 50  0000 L CNN
F 1 "LPCB208" H 5850 3350 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 6000 3400 50  0001 C CNN
F 3 "~" H 6000 3400 50  0001 C CNN
	1    6000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3200 6400 3200
Wire Wire Line
	6050 5350 6700 5350
Text Label 6050 4550 0    50   ~ 0
SPI_MISO
$Comp
L Device:R R1
U 1 1 5E151A6D
P 6400 2950
F 0 "R1" H 6470 2996 50  0000 L CNN
F 1 "10k" H 6470 2905 50  0000 L CNN
F 2 "Resistor_THT:R_Box_L14.0mm_W5.0mm_P9.00mm" V 6330 2950 50  0001 C CNN
F 3 "~" H 6400 2950 50  0001 C CNN
	1    6400 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3100 6400 3200
Connection ~ 6400 3200
Wire Wire Line
	6400 3200 6800 3200
$Comp
L power:+5V #PWR05
U 1 1 5E1578CB
P 6400 2700
F 0 "#PWR05" H 6400 2550 50  0001 C CNN
F 1 "+5V" H 6415 2873 50  0000 C CNN
F 2 "" H 6400 2700 50  0001 C CNN
F 3 "" H 6400 2700 50  0001 C CNN
	1    6400 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2700 6400 2800
$Comp
L power:GND #PWR03
U 1 1 5E18028F
P 5750 3250
F 0 "#PWR03" H 5750 3000 50  0001 C CNN
F 1 "GND" H 5755 3077 50  0000 C CNN
F 2 "" H 5750 3250 50  0001 C CNN
F 3 "" H 5750 3250 50  0001 C CNN
	1    5750 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3250 5750 3200
Wire Wire Line
	5750 3200 5800 3200
Entry Wire Line
	8450 2250 8350 2350
Wire Wire Line
	8350 2350 6800 2350
Wire Wire Line
	6800 2350 6800 3200
Connection ~ 6800 3200
Wire Wire Line
	6800 3200 7000 3200
Entry Wire Line
	6700 5350 6800 5450
Text Label 8200 2350 0    50   ~ 0
RST
$Comp
L conn_gost_local:CONN_2-Connector_Generic_GOST X1
U 1 1 5E18E0BA
P 1700 7500
F 0 "X1" H 1700 8300 79  0000 C CNN
F 1 "CONN_2-Connector_Generic_GOST" H 1700 6700 79  0001 C CNN
F 2 "Connector:Banana_Jack_2Pin" H 2300 8300 79  0001 C CNN
F 3 "" H 2300 6700 79  0001 C CNN
F 4 "Ucc" H 1500 7900 79  0000 C CNN "PIN1"
F 5 "GND" H 1500 7700 79  0000 C CNN "PIN2"
	1    1700 7500
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM78M05_TO252 U3
U 1 1 5E1A9E28
P 4450 6900
F 0 "U3" H 4450 7142 50  0000 C CNN
F 1 "LM78M05_TO252" H 4450 7051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 4450 7125 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM78M05.pdf" H 4450 6850 50  0001 C CNN
	1    4450 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5E1AB1AB
P 2950 7200
F 0 "C1" H 3068 7246 50  0000 L CNN
F 1 "100µ" H 3068 7155 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 2988 7050 50  0001 C CNN
F 3 "~" H 2950 7200 50  0001 C CNN
	1    2950 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5E1AC05C
P 3400 7200
F 0 "C2" H 3515 7246 50  0000 L CNN
F 1 "100n" H 3515 7155 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 3438 7050 50  0001 C CNN
F 3 "~" H 3400 7200 50  0001 C CNN
	1    3400 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5E1AE7DA
P 5000 7200
F 0 "C3" H 5115 7246 50  0000 L CNN
F 1 "1µ" H 5115 7155 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 5038 7050 50  0001 C CNN
F 3 "~" H 5000 7200 50  0001 C CNN
	1    5000 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 7100 2550 7100
Wire Wire Line
	2550 7100 2550 6900
Wire Wire Line
	2550 6900 2950 6900
Wire Wire Line
	4750 6900 5000 6900
Wire Wire Line
	2950 7050 2950 6900
Connection ~ 2950 6900
Wire Wire Line
	2950 6900 3400 6900
Wire Wire Line
	3400 7050 3400 6900
Connection ~ 3400 6900
Wire Wire Line
	5000 7050 5000 6900
Connection ~ 5000 6900
Wire Wire Line
	2400 7300 2550 7300
Wire Wire Line
	2550 7300 2550 7500
Wire Wire Line
	2550 7500 2950 7500
Wire Wire Line
	5000 7350 5000 7500
Wire Wire Line
	3400 7350 3400 7500
Wire Wire Line
	3400 7500 4450 7500
Wire Wire Line
	2950 7350 2950 7500
Connection ~ 2950 7500
Wire Wire Line
	2950 7500 3400 7500
Wire Wire Line
	4450 7200 4450 7500
Connection ~ 4450 7500
Wire Wire Line
	4450 7500 5000 7500
$Comp
L power:GND #PWR01
U 1 1 5E22C5F7
P 4450 7600
F 0 "#PWR01" H 4450 7350 50  0001 C CNN
F 1 "GND" V 4350 7650 50  0000 R CNN
F 2 "" H 4450 7600 50  0001 C CNN
F 3 "" H 4450 7600 50  0001 C CNN
	1    4450 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 7500 4450 7600
$Comp
L power:+5V #PWR012
U 1 1 5E23ECBC
P 9550 2600
F 0 "#PWR012" H 9550 2450 50  0001 C CNN
F 1 "+5V" H 9565 2773 50  0000 C CNN
F 2 "" H 9550 2600 50  0001 C CNN
F 3 "" H 9550 2600 50  0001 C CNN
	1    9550 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 4600 8350 4600
Wire Wire Line
	8200 4700 8350 4700
Wire Wire Line
	8200 4800 8350 4800
Entry Wire Line
	8350 4600 8450 4500
Entry Wire Line
	8350 4700 8450 4600
Entry Wire Line
	8350 4800 8450 4700
Text Label 8200 4700 0    50   ~ 0
SPI_MISO
Text Label 8200 4600 0    50   ~ 0
SPI_MOSI
Text Label 8200 4800 0    50   ~ 0
SPI_SCK
$Comp
L power:+5V #PWR09
U 1 1 5E2A45E6
P 8550 5950
F 0 "#PWR09" H 8550 5800 50  0001 C CNN
F 1 "+5V" H 8565 6123 50  0000 C CNN
F 2 "" H 8550 5950 50  0001 C CNN
F 3 "" H 8550 5950 50  0001 C CNN
	1    8550 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 5700 8600 6000
Wire Wire Line
	8600 6000 8550 6000
Wire Wire Line
	8550 6000 8550 5950
Wire Wire Line
	6050 4550 6700 4550
Connection ~ 3400 7500
$Comp
L power:+15V #PWR0104
U 1 1 5E3ABE3D
P 4100 6900
F 0 "#PWR0104" H 4100 6750 50  0001 C CNN
F 1 "+15V" V 4000 6950 50  0000 L CNN
F 2 "" H 4100 6900 50  0001 C CNN
F 3 "" H 4100 6900 50  0001 C CNN
	1    4100 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 6900 4150 6900
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5E3B5333
P 3750 2950
F 0 "#FLG0103" H 3750 3025 50  0001 C CNN
F 1 "PWR_FLAG" H 3750 3123 50  0000 C CNN
F 2 "" H 3750 2950 50  0001 C CNN
F 3 "~" H 3750 2950 50  0001 C CNN
	1    3750 2950
	1    0    0    -1  
$EndComp
$Comp
L power:+15V #PWR0107
U 1 1 5E3B559E
P 3750 3150
F 0 "#PWR0107" H 3750 3000 50  0001 C CNN
F 1 "+15V" V 3650 3200 50  0000 L CNN
F 2 "" H 3750 3150 50  0001 C CNN
F 3 "" H 3750 3150 50  0001 C CNN
	1    3750 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 2950 3750 3150
Text Label 8550 3250 0    50   ~ 0
D4
Text Label 8550 3150 0    50   ~ 0
D3
Text Label 8550 3350 0    50   ~ 0
D5
Text Label 8550 3050 0    50   ~ 0
D2
Text Label 8550 3450 0    50   ~ 0
D6
Text Label 8550 2950 0    50   ~ 0
D1
Text Label 8550 3550 0    50   ~ 0
D7
Text Label 8550 2850 0    50   ~ 0
D0
$Comp
L conn_gost_local:CONN_6-Connector_Generic_GOST X2
U 1 1 5E01F455
P 5250 4950
F 0 "X2" H 5250 5800 79  0000 C CNN
F 1 "CONN_6-Connector_Generic_GOST" H 5250 4150 79  0001 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 5850 5750 79  0001 C CNN
F 3 "" H 5850 4150 79  0001 C CNN
F 4 "MISO" H 5050 5350 79  0000 C CNN "PIN1"
F 5 "VCC" H 5050 5150 79  0000 C CNN "PIN2"
F 6 "SCK" H 5050 4950 79  0000 C CNN "PIN3"
F 7 "MOSI" H 5050 4750 79  0000 C CNN "PIN4"
F 8 "Reset" H 5050 4550 79  0000 C CNN "PIN5"
F 9 "GND" H 5050 4350 79  0000 C CNN "PIN6"
	1    5250 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6900 3600 6900
$Comp
L power:+15V #PWR0102
U 1 1 5E36EEF8
P 3600 6900
F 0 "#PWR0102" H 3600 6750 50  0001 C CNN
F 1 "+15V" V 3500 6950 50  0000 L CNN
F 2 "" H 3600 6900 50  0001 C CNN
F 3 "" H 3600 6900 50  0001 C CNN
	1    3600 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 6900 5000 6700
$Comp
L power:+5V #PWR04
U 1 1 5E235300
P 5000 6700
F 0 "#PWR04" H 5000 6550 50  0001 C CNN
F 1 "+5V" H 5015 6873 50  0000 C CNN
F 2 "" H 5000 6700 50  0001 C CNN
F 3 "" H 5000 6700 50  0001 C CNN
	1    5000 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5E1AEE0C
P 7400 2700
F 0 "C4" H 7515 2746 50  0000 L CNN
F 1 "100n" H 7515 2655 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 7438 2550 50  0001 C CNN
F 3 "~" H 7400 2700 50  0001 C CNN
	1    7400 2700
	0    1    1    0   
$EndComp
$Comp
L MCU_Microchip_ATmega:ATmega8515-16JU U1
U 1 1 5E01D64C
P 7600 4900
F 0 "U1" H 8050 7000 50  0000 C CNN
F 1 "ATmega8515-16JU" H 8050 6900 50  0000 C CNN
F 2 "Package_LCC:PLCC-44" H 7600 4900 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2512.pdf" H 7600 4900 50  0001 C CNN
	1    7600 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR08
U 1 1 5E247217
P 7600 2550
F 0 "#PWR08" H 7600 2400 50  0001 C CNN
F 1 "+5V" H 7615 2723 50  0000 C CNN
F 2 "" H 7600 2550 50  0001 C CNN
F 3 "" H 7600 2550 50  0001 C CNN
	1    7600 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR010
U 1 1 5E2365E1
P 9050 1600
F 0 "#PWR010" H 9050 1450 50  0001 C CNN
F 1 "+5V" H 9065 1773 50  0000 C CNN
F 2 "" H 9050 1600 50  0001 C CNN
F 3 "" H 9050 1600 50  0001 C CNN
	1    9050 1600
	1    0    0    -1  
$EndComp
$Comp
L Display_Character:WC1602A DS1
U 1 1 5E02E278
P 9050 2950
F 0 "DS1" H 9250 3900 50  0000 C CNN
F 1 "WC1602A" H 9250 3800 50  0000 C CNN
F 2 "Display:WC1602A" H 9050 2050 50  0001 C CIN
F 3 "http://www.wincomlcd.com/pdf/WC1602A-SFYLYHTC06.pdf" H 9750 2950 50  0001 C CNN
	1    9050 2950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HCT137 U2
U 1 1 5E071D7B
P 9100 5400
F 0 "U2" H 9350 6100 50  0000 C CNN
F 1 "74HCT137" H 9350 6000 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 9100 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc237.pdf" H 9100 5400 50  0001 C CNN
	1    9100 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR011
U 1 1 5E23F2EC
P 9100 4350
F 0 "#PWR011" H 9100 4200 50  0001 C CNN
F 1 "+5V" H 9115 4523 50  0000 C CNN
F 2 "" H 9100 4350 50  0001 C CNN
F 3 "" H 9100 4350 50  0001 C CNN
	1    9100 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5E101E71
P 8850 4550
F 0 "C6" H 8965 4596 50  0000 L CNN
F 1 "100n" H 8965 4505 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 8888 4400 50  0001 C CNN
F 3 "~" H 8850 4550 50  0001 C CNN
	1    8850 4550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E041434
P 7150 2700
F 0 "#PWR?" H 7150 2450 50  0001 C CNN
F 1 "GND" H 7155 2527 50  0000 C CNN
F 2 "" H 7150 2700 50  0001 C CNN
F 3 "" H 7150 2700 50  0001 C CNN
	1    7150 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	7150 2700 7250 2700
Wire Wire Line
	7600 2550 7600 2700
Wire Wire Line
	7550 2700 7600 2700
Connection ~ 7600 2700
Wire Wire Line
	7600 2700 7600 2900
$Comp
L Device:C C5
U 1 1 5E0AFDD4
P 8750 1900
F 0 "C5" H 8865 1946 50  0000 L CNN
F 1 "100n" H 8865 1855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 8788 1750 50  0001 C CNN
F 3 "~" H 8750 1900 50  0001 C CNN
	1    8750 1900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E0B03B4
P 8450 1900
F 0 "#PWR?" H 8450 1650 50  0001 C CNN
F 1 "GND" H 8455 1727 50  0000 C CNN
F 2 "" H 8450 1900 50  0001 C CNN
F 3 "" H 8450 1900 50  0001 C CNN
	1    8450 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	8450 1900 8600 1900
Wire Wire Line
	9050 1600 9050 1900
Wire Wire Line
	8900 1900 9050 1900
Connection ~ 9050 1900
Wire Wire Line
	9050 1900 9050 2150
$Comp
L power:GND #PWR?
U 1 1 5E0CA7C8
P 8650 4700
F 0 "#PWR?" H 8650 4450 50  0001 C CNN
F 1 "GND" H 8655 4527 50  0000 C CNN
F 2 "" H 8650 4700 50  0001 C CNN
F 3 "" H 8650 4700 50  0001 C CNN
	1    8650 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 4700 8650 4550
Wire Wire Line
	8650 4550 8700 4550
Wire Wire Line
	9100 4350 9100 4550
Wire Wire Line
	9000 4550 9100 4550
Wire Bus Line
	10500 2300 12800 2300
Wire Bus Line
	8450 4100 12800 4100
Wire Bus Line
	10450 3200 12800 3200
Wire Bus Line
	6800 4350 6800 6950
Wire Bus Line
	8450 2100 8450 4100
Wire Bus Line
	8450 4100 8450 6950
Connection ~ 9100 4550
Wire Wire Line
	9100 4550 9100 4800
$EndSCHEMATC
