EESchema Schematic File Version 4
LIBS:Clock2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Электронные часы с секундомером"
Date "2019-12-27"
Rev ""
Comp "МГТУ им. Н.Э. Баумана"
Comment1 ""
Comment2 "Александров И.А."
Comment3 "Хохлов С.А."
Comment4 ""
$EndDescr
$Comp
L Interface_UART:MAX232 U2
U 1 1 5E053B76
P 4750 6450
F 0 "U2" H 4750 7831 50  0000 C CNN
F 1 "MAX232" H 4750 7740 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4800 5400 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 4750 6550 50  0001 C CNN
	1    4750 6450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5E056B24
P 1850 2800
F 0 "C2" H 1965 2846 50  0000 L CNN
F 1 "22p" H 1965 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 1888 2650 50  0001 C CNN
F 3 "~" H 1850 2800 50  0001 C CNN
	1    1850 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal ZQ1
U 1 1 5E0572FA
P 1500 2650
F 0 "ZQ1" H 1500 2918 50  0000 C CNN
F 1 "Crystal" H 1500 2827 50  0000 C CNN
F 2 "Crystal:Crystal_AT310_D3.0mm_L10.0mm_Horizontal" H 1500 2650 50  0001 C CNN
F 3 "~" H 1500 2650 50  0001 C CNN
	1    1500 2650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push Start1
U 1 1 5E058861
P 5550 1750
F 0 "Start1" H 5550 2035 50  0000 C CNN
F 1 "SW_Push" H 5550 1944 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5550 1950 50  0001 C CNN
F 3 "~" H 5550 1950 50  0001 C CNN
	1    5550 1750
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push Clock1
U 1 1 5E05EE26
P 5550 950
F 0 "Clock1" H 5550 1235 50  0000 C CNN
F 1 "SW_Push" H 5550 1144 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5550 1150 50  0001 C CNN
F 3 "~" H 5550 1150 50  0001 C CNN
	1    5550 950 
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push Send1
U 1 1 5E05F29D
P 5550 1350
F 0 "Send1" H 5550 1635 50  0000 C CNN
F 1 "SW_Push" H 5550 1544 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5550 1550 50  0001 C CNN
F 3 "~" H 5550 1550 50  0001 C CNN
	1    5550 1350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push Clear1
U 1 1 5E05FFF5
P 5550 2550
F 0 "Clear1" H 5550 2835 50  0000 C CNN
F 1 "SW_Push" H 5550 2744 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5550 2750 50  0001 C CNN
F 3 "~" H 5550 2750 50  0001 C CNN
	1    5550 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 950  5750 950 
Wire Wire Line
	5750 1350 5950 1350
Connection ~ 5950 1350
Wire Wire Line
	5950 1350 5950 950 
Wire Wire Line
	5750 1750 5950 1750
Connection ~ 5950 1750
Wire Wire Line
	5950 1750 5950 1350
Wire Wire Line
	5750 2150 5950 2150
Connection ~ 5950 2150
Wire Wire Line
	5950 2150 5950 1750
Wire Wire Line
	5750 2550 5950 2550
Wire Wire Line
	5950 2550 5950 2150
Wire Wire Line
	3250 1800 3550 1800
Wire Wire Line
	3550 1800 3550 950 
Wire Wire Line
	3250 1900 3600 1900
Wire Wire Line
	3600 1900 3600 1350
Wire Wire Line
	3250 2000 3700 2000
Wire Wire Line
	3700 2000 3700 1750
Wire Wire Line
	3250 2100 3750 2100
Wire Wire Line
	3750 2100 3750 2150
Wire Wire Line
	3750 2150 5350 2150
Wire Wire Line
	3250 2200 3750 2200
Wire Wire Line
	3750 2200 3750 2550
Wire Wire Line
	3750 2550 5350 2550
$Comp
L MCU_Microchip_ATmega:ATmega8515-16AU U1
U 1 1 5E05273D
P 2650 3500
F 0 "U1" H 2650 5681 50  0000 C CNN
F 1 "ATmega8515-16AU" H 2650 5590 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 2650 3500 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2512.pdf" H 2650 3500 50  0001 C CNN
	1    2650 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E0B624B
P 1150 2800
F 0 "C1" H 1265 2846 50  0000 L CNN
F 1 "22p" H 1265 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 1188 2650 50  0001 C CNN
F 3 "~" H 1150 2800 50  0001 C CNN
	1    1150 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2650 1650 2650
Wire Wire Line
	1150 2650 1350 2650
Wire Wire Line
	1850 2650 1850 2200
Connection ~ 1850 2650
Wire Wire Line
	1150 2650 1150 2000
Connection ~ 1150 2650
$Comp
L Switch:SW_Push RESET1
U 1 1 5E0D238C
P 1050 1900
F 0 "RESET1" H 1050 2185 50  0000 C CNN
F 1 "SW_Push" H 1050 2094 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 1050 2100 50  0001 C CNN
F 3 "~" H 1050 2100 50  0001 C CNN
	1    1050 1900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1250 1900 1900 1900
Wire Wire Line
	1900 1900 1900 1800
Wire Wire Line
	1900 1800 2050 1800
Wire Wire Line
	3600 4500 3250 4500
Wire Wire Line
	3600 4500 3600 6750
Wire Wire Line
	3550 4600 3250 4600
Wire Wire Line
	3550 4600 3550 6950
Wire Wire Line
	3250 2700 6000 2700
Wire Wire Line
	6000 2700 6000 1350
Wire Wire Line
	6050 1450 6050 2800
Wire Wire Line
	6050 2800 3250 2800
Wire Wire Line
	3250 2900 6100 2900
Wire Wire Line
	6100 2900 6100 1550
Wire Wire Line
	3250 3600 6800 3600
Wire Wire Line
	3250 3700 6750 3700
Wire Wire Line
	6750 3700 6750 2450
Wire Wire Line
	6750 2450 6800 2450
Wire Wire Line
	3250 3800 6700 3800
Wire Wire Line
	6700 2350 6800 2350
Wire Wire Line
	3250 3900 6650 3900
Wire Wire Line
	6650 3900 6650 2250
NoConn ~ 3950 6550
NoConn ~ 3950 7150
NoConn ~ 2050 4500
NoConn ~ 2050 4600
NoConn ~ 2050 4700
NoConn ~ 3250 4700
NoConn ~ 3250 4800
NoConn ~ 3250 4900
NoConn ~ 3250 5000
NoConn ~ 3250 5100
NoConn ~ 3250 5200
NoConn ~ 7600 2350
NoConn ~ 7600 2250
NoConn ~ 7600 2050
NoConn ~ 7600 1350
NoConn ~ 7600 1650
NoConn ~ 7600 1750
NoConn ~ 3250 3300
NoConn ~ 3250 3400
NoConn ~ 3250 2300
NoConn ~ 3250 2400
NoConn ~ 3250 2500
$Comp
L Connector:DB9_Female J2
U 1 1 5E3B838D
P 6250 6950
F 0 "J2" H 6430 6996 50  0000 L CNN
F 1 "DB9_Female" H 6430 6905 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Female_Horizontal_P2.77x2.54mm_EdgePinOffset9.40mm" H 6250 6950 50  0001 C CNN
F 3 " ~" H 6250 6950 50  0001 C CNN
	1    6250 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5850 3950 5850
Wire Wire Line
	3750 5550 3950 5550
Wire Wire Line
	3600 6750 3950 6750
Wire Wire Line
	3550 6950 3950 6950
$Comp
L Switch:SW_Push Stop1
U 1 1 5E05F0D1
P 5550 2150
F 0 "Stop1" H 5550 2435 50  0000 C CNN
F 1 "SW_Push" H 5550 2344 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5550 2350 50  0001 C CNN
F 3 "~" H 5550 2350 50  0001 C CNN
	1    5550 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 950  5350 950 
Wire Wire Line
	5350 1350 3600 1350
Wire Wire Line
	5350 1750 3700 1750
Wire Wire Line
	5550 6050 5850 6050
Wire Wire Line
	5550 5550 5700 5550
Wire Wire Line
	5550 5850 5700 5850
Wire Wire Line
	5550 6550 5950 6550
Wire Wire Line
	5550 6950 5950 6950
Wire Wire Line
	1850 2950 1850 5600
Wire Wire Line
	1150 5600 1600 5600
Wire Wire Line
	1150 2950 1150 5600
NoConn ~ 5550 6750
NoConn ~ 5550 7150
Wire Wire Line
	850  5600 1150 5600
Wire Wire Line
	850  1900 850  5600
Connection ~ 1150 5600
Wire Wire Line
	6800 3600 6800 2550
$Comp
L Display_Character:LCD-016N002L Clock2
U 1 1 5E05643E
P 7200 1950
F 0 "Clock2" H 7200 2931 50  0000 C CNN
F 1 "LCD-016N002L" H 7200 2840 50  0000 C CNN
F 2 "Display:LCD-016N002L" H 7220 1030 50  0001 C CNN
F 3 "http://www.vishay.com/docs/37299/37299.pdf" H 7700 1650 50  0001 C CNN
	1    7200 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 6350 5550 6350
NoConn ~ 6800 1850
NoConn ~ 6800 1950
NoConn ~ 6800 2050
NoConn ~ 6800 2150
NoConn ~ 3250 3000
NoConn ~ 3250 3100
NoConn ~ 3250 3200
NoConn ~ 3250 4000
NoConn ~ 3250 4100
NoConn ~ 3250 4200
NoConn ~ 3250 4300
Wire Wire Line
	6000 1350 6800 1350
Wire Wire Line
	6050 1450 6800 1450
Wire Wire Line
	6100 1550 6800 1550
Wire Wire Line
	6650 2250 6800 2250
NoConn ~ 7600 1950
$Comp
L Regulator_Linear:LM7805_TO220 U3
U 1 1 5E18BDF2
P 8150 4700
F 0 "U3" H 8150 4942 50  0000 C CNN
F 1 "LM7805_TO220" H 8150 4851 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8150 4925 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 8150 4650 50  0001 C CNN
	1    8150 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E18FE44
P 6150 950
F 0 "#PWR0101" H 6150 700 50  0001 C CNN
F 1 "GND" H 6155 777 50  0000 C CNN
F 2 "" H 6150 950 50  0001 C CNN
F 3 "" H 6150 950 50  0001 C CNN
	1    6150 950 
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5E1904D5
P 9350 4550
F 0 "#PWR0102" H 9350 4400 50  0001 C CNN
F 1 "+5V" H 9365 4723 50  0000 C CNN
F 2 "" H 9350 4550 50  0001 C CNN
F 3 "" H 9350 4550 50  0001 C CNN
	1    9350 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2200 2050 2200
Wire Wire Line
	1150 2000 2050 2000
$Comp
L power:GND #PWR0103
U 1 1 5E1B2C20
P 1600 5850
F 0 "#PWR0103" H 1600 5600 50  0001 C CNN
F 1 "GND" H 1605 5677 50  0000 C CNN
F 2 "" H 1600 5850 50  0001 C CNN
F 3 "" H 1600 5850 50  0001 C CNN
	1    1600 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 5850 1600 5600
Connection ~ 1600 5600
Wire Wire Line
	1600 5600 1850 5600
$Comp
L power:GND #PWR0104
U 1 1 5E1B6619
P 5150 7650
F 0 "#PWR0104" H 5150 7400 50  0001 C CNN
F 1 "GND" H 5155 7477 50  0000 C CNN
F 2 "" H 5150 7650 50  0001 C CNN
F 3 "" H 5150 7650 50  0001 C CNN
	1    5150 7650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E1BC7F2
P 6700 6350
F 0 "#PWR0105" H 6700 6100 50  0001 C CNN
F 1 "GND" H 6705 6177 50  0000 C CNN
F 2 "" H 6700 6350 50  0001 C CNN
F 3 "" H 6700 6350 50  0001 C CNN
	1    6700 6350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6450 6350 6700 6350
Wire Wire Line
	7200 2750 7200 2950
$Comp
L power:+5V #PWR0106
U 1 1 5E1C99F5
P 5250 5250
F 0 "#PWR0106" H 5250 5100 50  0001 C CNN
F 1 "+5V" H 5265 5423 50  0000 C CNN
F 2 "" H 5250 5250 50  0001 C CNN
F 3 "" H 5250 5250 50  0001 C CNN
	1    5250 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 5250 5250 5250
$Comp
L power:+5V #PWR0107
U 1 1 5E1D3642
P 2100 1500
F 0 "#PWR0107" H 2100 1350 50  0001 C CNN
F 1 "+5V" H 2115 1673 50  0000 C CNN
F 2 "" H 2100 1500 50  0001 C CNN
F 3 "" H 2100 1500 50  0001 C CNN
	1    2100 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2650 1500 2100 1500
Wire Wire Line
	5950 950  6150 950 
Connection ~ 5950 950 
Wire Wire Line
	9350 4700 9350 4550
$Comp
L power:GND #PWR0108
U 1 1 5E1C2FC7
P 7200 2950
F 0 "#PWR0108" H 7200 2700 50  0001 C CNN
F 1 "GND" H 7205 2777 50  0000 C CNN
F 2 "" H 7200 2950 50  0001 C CNN
F 3 "" H 7200 2950 50  0001 C CNN
	1    7200 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E1E49D4
P 8150 5250
F 0 "#PWR0109" H 8150 5000 50  0001 C CNN
F 1 "GND" H 8155 5077 50  0000 C CNN
F 2 "" H 8150 5250 50  0001 C CNN
F 3 "" H 8150 5250 50  0001 C CNN
	1    8150 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 5250 8150 5100
$Comp
L Device:C C10
U 1 1 5E1E95F4
P 9350 4850
F 0 "C10" H 9465 4896 50  0000 L CNN
F 1 "100n" H 9465 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9388 4700 50  0001 C CNN
F 3 "~" H 9350 4850 50  0001 C CNN
	1    9350 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 5000 9350 5100
Wire Wire Line
	9350 5100 8800 5100
Connection ~ 8150 5100
Wire Wire Line
	8150 5100 8150 5000
Wire Wire Line
	8800 5000 8800 5100
Connection ~ 8800 5100
Wire Wire Line
	8800 5100 8150 5100
$Comp
L Device:C C8
U 1 1 5E1F6061
P 7550 4850
F 0 "C8" H 7665 4896 50  0000 L CNN
F 1 "100n" H 7665 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7588 4700 50  0001 C CNN
F 3 "~" H 7550 4850 50  0001 C CNN
	1    7550 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 5000 7100 5100
Wire Wire Line
	7100 5100 7550 5100
Wire Wire Line
	7550 5000 7550 5100
Connection ~ 7550 5100
Wire Wire Line
	7550 5100 8150 5100
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5E20D0D2
P 6450 4900
F 0 "J1" H 6368 4575 50  0000 C CNN
F 1 "Conn_01x02" H 6368 4666 50  0000 C CNN
F 2 "Connector:Banana_Jack_2Pin" H 6450 4900 50  0001 C CNN
F 3 "~" H 6450 4900 50  0001 C CNN
	1    6450 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6650 4800 6800 4800
Wire Wire Line
	6800 4800 6800 4700
Wire Wire Line
	7100 5100 6800 5100
Wire Wire Line
	6800 5100 6800 4900
Wire Wire Line
	6800 4900 6650 4900
Connection ~ 7100 5100
Wire Wire Line
	6700 2350 6700 3800
$Comp
L power:+5V #PWR0110
U 1 1 5E21E58E
P 6450 6050
F 0 "#PWR0110" H 6450 5900 50  0001 C CNN
F 1 "+5V" H 6465 6223 50  0000 C CNN
F 2 "" H 6450 6050 50  0001 C CNN
F 3 "" H 6450 6050 50  0001 C CNN
	1    6450 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 6050 6450 6050
Wire Wire Line
	2650 5500 2650 5600
Wire Wire Line
	2650 5600 1850 5600
Connection ~ 1850 5600
Wire Wire Line
	7850 4700 7550 4700
Connection ~ 7550 4700
Wire Wire Line
	4750 7650 5150 7650
Text GLabel 6400 4800 0    50   Input ~ 0
VCC
Text GLabel 6400 4900 0    50   Input ~ 0
GND
$Comp
L power:+5V #PWR0111
U 1 1 5E2605AF
P 7550 1150
F 0 "#PWR0111" H 7550 1000 50  0001 C CNN
F 1 "+5V" H 7565 1323 50  0000 C CNN
F 2 "" H 7550 1150 50  0001 C CNN
F 3 "" H 7550 1150 50  0001 C CNN
	1    7550 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 1150 7550 1150
$Comp
L Device:CP C7
U 1 1 5E2873FC
P 7100 4850
F 0 "C7" H 6982 4804 50  0000 R CNN
F 1 "1u" H 6982 4895 50  0000 R CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 7138 4700 50  0001 C CNN
F 3 "~" H 7100 4850 50  0001 C CNN
	1    7100 4850
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C9
U 1 1 5E288314
P 8800 4850
F 0 "C9" H 8918 4896 50  0000 L CNN
F 1 "1u" H 8918 4805 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 8838 4700 50  0001 C CNN
F 3 "~" H 8800 4850 50  0001 C CNN
	1    8800 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C6
U 1 1 5E28B324
P 6300 6350
F 0 "C6" V 6045 6350 50  0000 C CNN
F 1 "1u" V 6136 6350 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 6338 6200 50  0001 C CNN
F 3 "~" H 6300 6350 50  0001 C CNN
	1    6300 6350
	0    1    1    0   
$EndComp
$Comp
L Device:CP C4
U 1 1 5E28E35A
P 5700 5700
F 0 "C4" H 5818 5746 50  0000 L CNN
F 1 "1u" H 5818 5655 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 5738 5550 50  0001 C CNN
F 3 "~" H 5700 5700 50  0001 C CNN
	1    5700 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4700 7100 4700
Wire Wire Line
	8450 4700 8800 4700
Connection ~ 7100 4700
Wire Wire Line
	7100 4700 7550 4700
Connection ~ 8800 4700
Wire Wire Line
	8800 4700 9350 4700
$Comp
L Device:CP C5
U 1 1 5E2A6161
P 6000 6050
F 0 "C5" V 6255 6050 50  0000 C CNN
F 1 "1u" V 6164 6050 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 6038 5900 50  0001 C CNN
F 3 "~" H 6000 6050 50  0001 C CNN
	1    6000 6050
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C3
U 1 1 5E2C781F
P 3750 5700
F 0 "C3" H 3868 5746 50  0000 L CNN
F 1 "1u" H 3868 5655 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 3788 5550 50  0001 C CNN
F 3 "~" H 3750 5700 50  0001 C CNN
	1    3750 5700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
