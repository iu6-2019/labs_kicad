EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема электрическая принципиальная"
Date ""
Rev ""
Comp "МГТУ им. Н.Э.Баумана ИУ6-73б"
Comment1 "Аналоговый регистратор"
Comment2 "Мамадаев И.М."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L my_Library1:DS-201 XP1
U 1 1 5DD4FB5D
P 2200 9050
F 0 "XP1" H 2233 9165 50  0000 C CNN
F 1 "DS-201" H 2233 9074 50  0000 C CNN
F 2 "Connector:Banana_Jack_2Pin" H 2200 9050 50  0001 C CNN
F 3 "" H 2200 9050 50  0001 C CNN
	1    2200 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 9300 2600 9300
Wire Wire Line
	2600 9300 2600 9100
Wire Wire Line
	2950 9100 2950 9200
Wire Wire Line
	2500 9450 2600 9450
Wire Wire Line
	2600 9450 2600 9650
Wire Wire Line
	2950 9650 2950 9500
$Comp
L Regulator_Linear:LM7805_TO220 DD1
U 1 1 5DD54E4A
P 3750 9100
F 0 "DD1" H 3750 9342 50  0000 C CNN
F 1 "LM7805_TO220" H 3750 9251 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3750 9325 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 3750 9050 50  0001 C CNN
	1    3750 9100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 9100 2950 9100
Wire Wire Line
	3750 9650 2950 9650
Connection ~ 2950 9650
Connection ~ 3750 9650
Wire Wire Line
	4050 9100 4250 9100
Wire Wire Line
	4250 9100 4250 9250
Wire Wire Line
	4250 9550 4250 9650
Wire Wire Line
	4250 9650 3750 9650
Wire Wire Line
	4750 9250 4750 9100
Wire Wire Line
	4750 9100 4250 9100
Connection ~ 4250 9100
Wire Wire Line
	4750 9550 4750 9650
Wire Wire Line
	4750 9650 4250 9650
Connection ~ 4250 9650
$Comp
L power:+5V #PWR05
U 1 1 5DD58EEC
P 4750 8850
F 0 "#PWR05" H 4750 8700 50  0001 C CNN
F 1 "+5V" H 4765 9023 50  0000 C CNN
F 2 "" H 4750 8850 50  0001 C CNN
F 3 "" H 4750 8850 50  0001 C CNN
	1    4750 8850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 8850 4750 9100
Connection ~ 4750 9100
$Comp
L Device:CP C1
U 1 1 5DD598A2
P 2950 9350
F 0 "C1" H 3068 9396 50  0000 L CNN
F 1 "0.33 u" H 3068 9305 50  0000 L CNN
F 2 "Capacitor_SMD:C_Trimmer_Murata_TZB4-A" H 2988 9200 50  0001 C CNN
F 3 "~" H 2950 9350 50  0001 C CNN
	1    2950 9350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5DD5A843
P 4250 9400
F 0 "C2" H 4368 9446 50  0000 L CNN
F 1 "0.1 u" H 4368 9355 50  0000 L CNN
F 2 "Capacitor_SMD:C_Trimmer_Murata_TZB4-A" H 4288 9250 50  0001 C CNN
F 3 "~" H 4250 9400 50  0001 C CNN
	1    4250 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 5DD5B013
P 4750 9400
F 0 "C4" H 4868 9446 50  0000 L CNN
F 1 "1 u" H 4868 9355 50  0000 L CNN
F 2 "Capacitor_SMD:C_Trimmer_Murata_TZB4-A" H 4788 9250 50  0001 C CNN
F 3 "~" H 4750 9400 50  0001 C CNN
	1    4750 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal ZQ1
U 1 1 5DD61E2D
P 5100 3200
F 0 "ZQ1" H 5100 3468 50  0000 C CNN
F 1 "8 MHz" H 5100 3377 50  0000 C CNN
F 2 "Crystal:Crystal_HC49-U_Horizontal_1EP_style1" H 5100 3200 50  0001 C CNN
F 3 "~" H 5100 3200 50  0001 C CNN
	1    5100 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5DD62AD6
P 4800 3550
F 0 "C3" H 4915 3596 50  0000 L CNN
F 1 "22 p" H 4915 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_Trimmer_Murata_TZR1" H 4838 3400 50  0001 C CNN
F 3 "~" H 4800 3550 50  0001 C CNN
	1    4800 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5DD6322B
P 5350 3550
F 0 "C5" H 5465 3596 50  0000 L CNN
F 1 "22 p" H 5465 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_Trimmer_Murata_TZR1" H 5388 3400 50  0001 C CNN
F 3 "~" H 5350 3550 50  0001 C CNN
	1    5350 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3400 4800 3200
Wire Wire Line
	4800 3200 4950 3200
Wire Wire Line
	5250 3200 5350 3200
Wire Wire Line
	5350 3200 5350 3400
Wire Wire Line
	5600 2600 5350 2600
Wire Wire Line
	5350 2600 5350 3200
Connection ~ 5350 3200
Wire Wire Line
	5600 2800 4800 2800
Wire Wire Line
	4800 2800 4800 3200
Connection ~ 4800 3200
$Comp
L Display_Character:NHD-0420H1Z HG1
U 1 1 5DD67ED8
P 9150 2350
F 0 "HG1" H 9100 3400 50  0000 C CNN
F 1 "LM016L" H 9100 3300 50  0000 C CNN
F 2 "Display:NHD-0420H1Z" H 9150 1450 50  0001 C CNN
F 3 "http://www.newhavendisplay.com/specs/NHD-0420H1Z-FSW-GBW-33V3.pdf" H 9250 2250 50  0001 C CNN
	1    9150 2350
	1    0    0    -1  
$EndComp
$Comp
L Interface_UART:MAX232 DD3
U 1 1 5DD68D27
P 10250 5050
F 0 "DD3" H 10250 6431 50  0000 C CNN
F 1 "MAX232" H 10250 6340 50  0000 C CNN
F 2 "Transformer_SMD:Transformer_Ethernet_Bel_S558-5999-T7-F" H 10300 4000 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 10250 5150 50  0001 C CNN
	1    10250 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1750 8450 1750
Entry Wire Line
	8350 1850 8450 1750
Wire Wire Line
	8450 1850 8750 1850
Wire Wire Line
	8450 1950 8750 1950
Wire Wire Line
	8450 2650 8750 2650
Wire Wire Line
	8450 2750 8750 2750
Wire Wire Line
	8450 2850 8750 2850
Wire Wire Line
	8450 2950 8750 2950
$Comp
L power:+5V #PWR012
U 1 1 5DD7217E
P 9700 1400
F 0 "#PWR012" H 9700 1250 50  0001 C CNN
F 1 "+5V" H 9715 1573 50  0000 C CNN
F 2 "" H 9700 1400 50  0001 C CNN
F 3 "" H 9700 1400 50  0001 C CNN
	1    9700 1400
	1    0    0    -1  
$EndComp
Entry Wire Line
	8250 3300 8350 3400
Entry Wire Line
	8250 3400 8350 3500
Entry Wire Line
	8250 3500 8350 3600
Wire Wire Line
	6800 3300 8250 3300
Wire Wire Line
	8250 3400 6800 3400
Wire Wire Line
	6800 3500 8250 3500
Entry Wire Line
	8250 4200 8350 4300
Entry Wire Line
	8250 4300 8350 4400
Entry Wire Line
	8250 4400 8350 4500
Wire Wire Line
	6800 4200 8250 4200
Wire Wire Line
	8250 4300 6800 4300
Wire Wire Line
	6800 4400 8250 4400
Wire Wire Line
	6800 4500 8250 4500
Entry Wire Line
	8250 4500 8350 4600
$Comp
L power:+5V #PWR014
U 1 1 5DD85D51
P 10500 3750
F 0 "#PWR014" H 10500 3600 50  0001 C CNN
F 1 "+5V" H 10515 3923 50  0000 C CNN
F 2 "" H 10500 3750 50  0001 C CNN
F 3 "" H 10500 3750 50  0001 C CNN
	1    10500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 3750 10500 3800
Wire Wire Line
	10500 3850 10250 3850
$Comp
L Device:CP C6
U 1 1 5DD875D5
P 9150 4300
F 0 "C6" H 9268 4346 50  0000 L CNN
F 1 "1 u" H 9268 4255 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 9188 4150 50  0001 C CNN
F 3 "~" H 9150 4300 50  0001 C CNN
	1    9150 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C7
U 1 1 5DD88A12
P 11450 4300
F 0 "C7" H 11568 4346 50  0000 L CNN
F 1 "1 u" H 11568 4255 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 11488 4150 50  0001 C CNN
F 3 "~" H 11450 4300 50  0001 C CNN
	1    11450 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C11
U 1 1 5DD89408
P 12100 4050
F 0 "C11" H 12218 4096 50  0000 L CNN
F 1 "1 u" H 12218 4005 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 12138 3900 50  0001 C CNN
F 3 "~" H 12100 4050 50  0001 C CNN
	1    12100 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C10
U 1 1 5DD8A4ED
P 12050 4800
F 0 "C10" H 12168 4846 50  0000 L CNN
F 1 "1 u" H 12168 4755 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 12088 4650 50  0001 C CNN
F 3 "~" H 12050 4800 50  0001 C CNN
	1    12050 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	11050 4150 11450 4150
Wire Wire Line
	11450 4450 11050 4450
Wire Wire Line
	9450 4150 9150 4150
Wire Wire Line
	9450 4450 9150 4450
Wire Wire Line
	11050 4650 11600 4650
Wire Wire Line
	11600 4650 11600 4500
Wire Wire Line
	11600 4500 12100 4500
Wire Wire Line
	12100 4500 12100 4200
Wire Wire Line
	12100 3900 12100 3800
Wire Wire Line
	12100 3800 10500 3800
Connection ~ 10500 3800
Wire Wire Line
	10500 3800 10500 3850
Wire Wire Line
	11050 4950 11750 4950
Wire Wire Line
	11750 4950 11750 4550
Wire Wire Line
	11750 4550 12050 4550
Wire Wire Line
	12050 4550 12050 4650
$Comp
L my_Library1:RS-232 XP3
U 1 1 5DD9B81A
P 13200 4900
F 0 "XP3" H 13150 4950 50  0000 L CNN
F 1 "RS-232" H 13100 4850 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Male_Vertical_P2.77x2.84mm" H 13200 4900 50  0001 C CNN
F 3 "" H 13200 4900 50  0001 C CNN
	1    13200 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	12800 6150 12700 6150
Wire Wire Line
	12700 6150 12700 6300
Wire Wire Line
	12700 6300 12800 6300
Wire Wire Line
	12800 6450 12700 6450
Wire Wire Line
	12800 5250 12700 5250
Wire Wire Line
	12800 6000 12700 6000
Wire Wire Line
	12800 5700 12700 5700
Wire Wire Line
	12800 5850 12450 5850
$Comp
L Device:C C9
U 1 1 5DDB07DC
P 11900 6100
F 0 "C9" H 12015 6146 50  0000 L CNN
F 1 "120 u" H 12015 6055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L10.0mm_D4.5mm_P15.00mm_Horizontal" H 11938 5950 50  0001 C CNN
F 3 "~" H 11900 6100 50  0001 C CNN
	1    11900 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5DDB1099
P 11450 6100
F 0 "C8" H 11565 6146 50  0000 L CNN
F 1 "120 u" H 11565 6055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L11.0mm_D8.0mm_P15.00mm_Horizontal" H 11488 5950 50  0001 C CNN
F 3 "~" H 11450 6100 50  0001 C CNN
	1    11450 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5DDB24C3
P 11400 5350
F 0 "R11" V 11193 5350 50  0000 C CNN
F 1 "470" V 11284 5350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 11330 5350 50  0001 C CNN
F 3 "~" H 11400 5350 50  0001 C CNN
	1    11400 5350
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5DDB2E05
P 11400 5750
F 0 "R12" V 11193 5750 50  0000 C CNN
F 1 "470" V 11284 5750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 11330 5750 50  0001 C CNN
F 3 "~" H 11400 5750 50  0001 C CNN
	1    11400 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	11250 5350 11050 5350
Wire Wire Line
	12500 5350 12500 5400
Wire Wire Line
	12500 5400 12800 5400
Wire Wire Line
	11550 5350 11700 5350
Wire Wire Line
	11250 5750 11050 5750
Wire Wire Line
	11550 5750 11900 5750
Wire Wire Line
	12500 5750 12500 5550
Wire Wire Line
	12500 5550 12800 5550
Wire Wire Line
	11450 5950 11450 5900
Wire Wire Line
	11450 5900 11700 5900
Wire Wire Line
	11700 5900 11700 5350
Connection ~ 11700 5350
Wire Wire Line
	11700 5350 12500 5350
Wire Wire Line
	11900 5950 11900 5750
Connection ~ 11900 5750
Wire Wire Line
	11900 5750 12500 5750
$Comp
L my_Library1:IDC-06MS XP2
U 1 1 5DDDDB61
P 6750 6700
F 0 "XP2" H 6700 6750 50  0000 L CNN
F 1 "IDC-06MS" H 6600 6650 50  0000 L CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Horizontal" H 6750 6700 50  0001 C CNN
F 3 "" H 6750 6700 50  0001 C CNN
	1    6750 6700
	1    0    0    -1  
$EndComp
Entry Wire Line
	5900 7600 6000 7500
Entry Wire Line
	5900 7450 6000 7350
Entry Wire Line
	5900 7150 6000 7050
Wire Wire Line
	6350 7050 6000 7050
Wire Wire Line
	6350 7350 6000 7350
Wire Wire Line
	6350 7500 6000 7500
$Comp
L power:+5V #PWR010
U 1 1 5DDF80B6
P 6250 6650
F 0 "#PWR010" H 6250 6500 50  0001 C CNN
F 1 "+5V" H 6265 6823 50  0000 C CNN
F 2 "" H 6250 6650 50  0001 C CNN
F 3 "" H 6250 6650 50  0001 C CNN
	1    6250 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 7800 6150 7800
Wire Wire Line
	6150 7800 6150 7950
Entry Wire Line
	7650 2700 7550 2600
Entry Wire Line
	7650 2800 7550 2700
Entry Wire Line
	7650 2900 7550 2800
Entry Wire Line
	7650 2600 7550 2500
Entry Wire Line
	7650 2500 7550 2400
Wire Bus Line
	4450 1700 7650 1700
Wire Wire Line
	6800 2900 7550 2900
Wire Wire Line
	6800 2800 7550 2800
Wire Wire Line
	6800 2700 7550 2700
Wire Wire Line
	6800 2600 7550 2600
Wire Wire Line
	6800 2500 7550 2500
Wire Wire Line
	6800 2400 7550 2400
Wire Wire Line
	9150 3200 9150 3150
Text Label 8450 1750 0    50   ~ 0
RS
Wire Wire Line
	9700 1400 9700 1500
Wire Wire Line
	9700 1500 9150 1500
Wire Wire Line
	9150 1500 9150 1550
$Comp
L Switch:SW_Push SW1
U 1 1 5DEA6633
P 2700 2600
F 0 "SW1" H 2700 2885 50  0000 C CNN
F 1 "\"Канал 1\"" H 2700 2794 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 2800 50  0001 C CNN
F 3 "~" H 2700 2800 50  0001 C CNN
	1    2700 2600
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5DEA7345
P 2700 3050
F 0 "SW2" H 2700 3335 50  0000 C CNN
F 1 "\"Канал 2\"" H 2700 3244 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 3250 50  0001 C CNN
F 3 "~" H 2700 3250 50  0001 C CNN
	1    2700 3050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5DEA7B72
P 2700 3500
F 0 "SW3" H 2700 3785 50  0000 C CNN
F 1 "\"Канал 3\"" H 2700 3694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 3700 50  0001 C CNN
F 3 "~" H 2700 3700 50  0001 C CNN
	1    2700 3500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5DEA830E
P 2700 4000
F 0 "SW4" H 2700 4285 50  0000 C CNN
F 1 "\"NEXT\"" H 2700 4194 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 4200 50  0001 C CNN
F 3 "~" H 2700 4200 50  0001 C CNN
	1    2700 4000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5DEA878D
P 2700 4450
F 0 "SW5" H 2700 4735 50  0000 C CNN
F 1 "\"PREV\"" H 2700 4644 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 4650 50  0001 C CNN
F 3 "~" H 2700 4650 50  0001 C CNN
	1    2700 4450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5DEA8B53
P 2700 4850
F 0 "SW6" H 2700 5135 50  0000 C CNN
F 1 "\"RESET MEM\"" H 2700 5044 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 5050 50  0001 C CNN
F 3 "~" H 2700 5050 50  0001 C CNN
	1    2700 4850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 5DEAA6A2
P 2700 6700
F 0 "SW10" H 2700 6985 50  0000 C CNN
F 1 "\"RESET\"" H 2700 6894 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2700 6900 50  0001 C CNN
F 3 "~" H 2700 6900 50  0001 C CNN
	1    2700 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 2600 2150 2600
Wire Wire Line
	2150 2600 2150 3050
Wire Wire Line
	2500 3050 2150 3050
Connection ~ 2150 3050
Wire Wire Line
	2150 3050 2150 3500
Wire Wire Line
	2500 3500 2150 3500
Connection ~ 2150 3500
Wire Wire Line
	2150 3500 2150 4000
Wire Wire Line
	2500 4000 2150 4000
Connection ~ 2150 4000
Wire Wire Line
	2150 4000 2150 4450
Wire Wire Line
	2500 4450 2150 4450
Connection ~ 2150 4450
Wire Wire Line
	2150 4450 2150 4850
Wire Wire Line
	2500 4850 2150 4850
Connection ~ 2150 4850
Wire Wire Line
	2500 6700 2150 6700
$Comp
L Device:R R2
U 1 1 5DEE3953
P 3550 2600
F 0 "R2" V 3343 2600 50  0000 C CNN
F 1 "1 k" V 3434 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 2600 50  0001 C CNN
F 3 "~" H 3550 2600 50  0001 C CNN
	1    3550 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5DEEA2DB
P 3550 3050
F 0 "R3" V 3343 3050 50  0000 C CNN
F 1 "1 k" V 3434 3050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 3050 50  0001 C CNN
F 3 "~" H 3550 3050 50  0001 C CNN
	1    3550 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5DEEA711
P 3550 3500
F 0 "R4" V 3343 3500 50  0000 C CNN
F 1 "1 k" V 3434 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 3500 50  0001 C CNN
F 3 "~" H 3550 3500 50  0001 C CNN
	1    3550 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5DEEAB8F
P 3550 4000
F 0 "R5" V 3343 4000 50  0000 C CNN
F 1 "1 k" V 3434 4000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 4000 50  0001 C CNN
F 3 "~" H 3550 4000 50  0001 C CNN
	1    3550 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5DEEB207
P 3550 4450
F 0 "R6" V 3343 4450 50  0000 C CNN
F 1 "1 k" V 3434 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 4450 50  0001 C CNN
F 3 "~" H 3550 4450 50  0001 C CNN
	1    3550 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5DEEB595
P 3550 4850
F 0 "R7" V 3343 4850 50  0000 C CNN
F 1 "1 k" V 3434 4850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 4850 50  0001 C CNN
F 3 "~" H 3550 4850 50  0001 C CNN
	1    3550 4850
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5DEF21C2
P 3500 6650
F 0 "R1" H 3430 6604 50  0000 R CNN
F 1 "10 k" H 3430 6695 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3430 6650 50  0001 C CNN
F 3 "~" H 3500 6650 50  0001 C CNN
	1    3500 6650
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 5DEF2C08
P 3500 6450
F 0 "#PWR02" H 3500 6300 50  0001 C CNN
F 1 "+5V" H 3600 6550 50  0000 C CNN
F 2 "" H 3500 6450 50  0001 C CNN
F 3 "" H 3500 6450 50  0001 C CNN
	1    3500 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6500 3500 6450
Wire Wire Line
	3500 6800 3500 6850
Wire Wire Line
	3500 6850 3050 6850
Wire Wire Line
	3050 6850 3050 6700
Wire Wire Line
	3050 6700 2900 6700
Wire Wire Line
	3400 4850 2900 4850
Wire Wire Line
	2900 4450 3400 4450
Wire Wire Line
	3400 4000 2900 4000
Wire Wire Line
	2900 3500 3400 3500
Wire Wire Line
	2900 3050 3400 3050
Wire Wire Line
	2900 2600 3400 2600
Entry Wire Line
	4350 2600 4450 2700
Wire Wire Line
	4350 2600 3700 2600
Entry Wire Line
	4350 3050 4450 3150
Wire Wire Line
	4350 3050 3700 3050
Entry Wire Line
	4350 3500 4450 3600
Wire Wire Line
	4350 3500 3700 3500
Entry Wire Line
	4350 4000 4450 4100
Wire Wire Line
	4350 4000 3700 4000
Entry Wire Line
	4350 4450 4450 4550
Wire Wire Line
	4350 4450 3700 4450
Entry Wire Line
	4350 4850 4450 4950
Wire Wire Line
	4350 4850 3700 4850
Entry Wire Line
	4350 6850 4450 6950
Wire Wire Line
	3500 6850 4350 6850
Connection ~ 3500 6850
Entry Wire Line
	4450 2500 4550 2400
Wire Wire Line
	5600 2400 4550 2400
Entry Wire Line
	4450 7750 4550 7650
Text Label 4250 3050 0    50   ~ 0
a1
Text Label 4250 3500 0    50   ~ 0
a2
Text Label 4250 4000 0    50   ~ 0
a3
Text Label 4250 4450 0    50   ~ 0
a4
Text Label 4250 4850 0    50   ~ 0
rst_mem
Text Label 4200 6850 0    50   ~ 0
reset
Text Label 4250 2600 0    50   ~ 0
a0
Text Label 4550 7650 0    50   ~ 0
reset
Text Label 7450 2400 0    50   ~ 0
a0
Text Label 7450 2500 0    50   ~ 0
a1
Text Label 7450 2600 0    50   ~ 0
a2
Text Label 7450 2700 0    50   ~ 0
a3
Text Label 7450 2800 0    50   ~ 0
a4
Text Label 7450 2900 0    50   ~ 0
rst_mem
Text Label 8150 3300 0    50   ~ 0
RS
Text Label 8150 3400 0    50   ~ 0
RW
Text Label 8150 3500 0    50   ~ 0
E
Text Label 8150 4200 0    50   ~ 0
S0
Text Label 8150 4300 0    50   ~ 0
S1
Text Label 8150 4400 0    50   ~ 0
S2
Text Label 8150 4500 0    50   ~ 0
S3
Text Label 8450 1850 0    50   ~ 0
RW
Text Label 8450 1950 0    50   ~ 0
E
Text Label 8450 2650 0    50   ~ 0
S0
Text Label 8450 2750 0    50   ~ 0
S1
Text Label 8450 2850 0    50   ~ 0
S2
Text Label 8450 2950 0    50   ~ 0
S3
Wire Wire Line
	6800 3800 7200 3800
Wire Wire Line
	6800 3900 7200 3900
Wire Wire Line
	6800 4000 7200 4000
Entry Wire Line
	7200 3800 7300 3900
Entry Wire Line
	7200 3900 7300 4000
Entry Wire Line
	7200 4000 7300 4100
Text Label 7000 3800 0    50   ~ 0
MOSI
Text Label 7000 3900 0    50   ~ 0
MISO
Text Label 7000 4000 0    50   ~ 0
SCK
Text Label 6000 7050 0    50   ~ 0
MISO
Text Label 6000 7350 0    50   ~ 0
SCK
Text Label 6000 7500 0    50   ~ 0
MOSI
Wire Wire Line
	6250 6650 6250 7200
Wire Wire Line
	6250 7200 6350 7200
Text Label 4550 2400 0    50   ~ 0
reset
Entry Wire Line
	8350 2050 8450 1950
Entry Wire Line
	8350 1950 8450 1850
Entry Wire Line
	8350 2750 8450 2650
Entry Wire Line
	8350 2850 8450 2750
Entry Wire Line
	8350 2950 8450 2850
Entry Wire Line
	8350 3050 8450 2950
NoConn ~ 11050 5150
NoConn ~ 11050 5550
NoConn ~ 12700 5250
NoConn ~ 12700 5700
NoConn ~ 12700 6000
NoConn ~ 12700 6450
NoConn ~ 6800 5400
NoConn ~ 6800 5500
NoConn ~ 6800 5600
NoConn ~ 6800 5700
NoConn ~ 6800 5800
NoConn ~ 6800 4900
NoConn ~ 6800 4800
NoConn ~ 6800 4700
NoConn ~ 6800 4600
NoConn ~ 6800 3600
NoConn ~ 6800 3700
NoConn ~ 8750 2250
NoConn ~ 8750 2350
NoConn ~ 8750 2450
NoConn ~ 8750 2550
NoConn ~ 9550 2550
NoConn ~ 9550 2650
NoConn ~ 9250 1550
Connection ~ 2950 9100
NoConn ~ 9450 5550
NoConn ~ 9450 5150
Text Label 8400 5200 0    50   ~ 0
TxD
$Comp
L power:+5V #PWR08
U 1 1 5DD5FD3F
P 6000 1900
F 0 "#PWR08" H 6000 1750 50  0001 C CNN
F 1 "+5V" H 6015 2073 50  0000 C CNN
F 2 "" H 6000 1900 50  0001 C CNN
F 3 "" H 6000 1900 50  0001 C CNN
	1    6000 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1900 6200 1900
Wire Wire Line
	6200 1900 6200 2100
NoConn ~ 5600 3000
NoConn ~ 6300 2100
Wire Bus Line
	7300 6400 5900 6400
Wire Wire Line
	6350 7650 4550 7650
Wire Wire Line
	9100 5200 9100 5350
Wire Wire Line
	9100 5350 9450 5350
Wire Wire Line
	6800 5200 9100 5200
NoConn ~ 6800 5100
Wire Wire Line
	2150 4850 2150 6700
Entry Wire Line
	7650 3000 7550 2900
NoConn ~ 6800 3000
NoConn ~ 6800 3100
Wire Wire Line
	6250 7950 6150 7950
Wire Wire Line
	5300 4000 5300 3700
Wire Wire Line
	5300 3700 5350 3700
Wire Wire Line
	10200 6250 10250 6250
NoConn ~ 9450 5750
Wire Wire Line
	4800 4000 4800 3700
Wire Wire Line
	2150 7000 2150 6700
Wire Wire Line
	6250 8250 6250 7950
Wire Wire Line
	3750 9650 3750 10050
Wire Wire Line
	11900 6550 11900 6250
Wire Wire Line
	12450 6150 12450 5850
Wire Wire Line
	12050 4950 12050 5050
NoConn ~ 6800 5300
NoConn ~ 10200 6250
NoConn ~ 9150 3200
$Comp
L newfile-rescue:ATmega8535-16AU-MCU_Microchip_ATmega U0
U 1 1 5DF16E4C
P 6200 4100
F 0 "U0" H 6200 6281 50  0000 C CNN
F 1 "ATmega8535-16AU" H 6200 6190 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 6200 4100 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2502.pdf" H 6200 4100 50  0001 C CNN
	1    6200 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E039DB1
P 11450 6600
F 0 "#PWR0101" H 11450 6350 50  0001 C CNN
F 1 "GND" H 11455 6427 50  0000 C CNN
F 2 "" H 11450 6600 50  0001 C CNN
F 3 "" H 11450 6600 50  0001 C CNN
	1    11450 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	11450 6250 11450 6600
$Comp
L power:GND #PWR0102
U 1 1 5E06297B
P 11900 6550
F 0 "#PWR0102" H 11900 6300 50  0001 C CNN
F 1 "GND" H 11905 6377 50  0000 C CNN
F 2 "" H 11900 6550 50  0001 C CNN
F 3 "" H 11900 6550 50  0001 C CNN
	1    11900 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E071AF0
P 12450 6150
F 0 "#PWR0103" H 12450 5900 50  0001 C CNN
F 1 "GND" H 12455 5977 50  0000 C CNN
F 2 "" H 12450 6150 50  0001 C CNN
F 3 "" H 12450 6150 50  0001 C CNN
	1    12450 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5E080E36
P 6250 8250
F 0 "#PWR0104" H 6250 8000 50  0001 C CNN
F 1 "GND" H 6255 8077 50  0000 C CNN
F 2 "" H 6250 8250 50  0001 C CNN
F 3 "" H 6250 8250 50  0001 C CNN
	1    6250 8250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E0978F2
P 3750 10050
F 0 "#PWR0105" H 3750 9800 50  0001 C CNN
F 1 "GND" H 3755 9877 50  0000 C CNN
F 2 "" H 3750 10050 50  0001 C CNN
F 3 "" H 3750 10050 50  0001 C CNN
	1    3750 10050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5E0A6A2B
P 2150 7000
F 0 "#PWR0106" H 2150 6750 50  0001 C CNN
F 1 "GND" H 2155 6827 50  0000 C CNN
F 2 "" H 2150 7000 50  0001 C CNN
F 3 "" H 2150 7000 50  0001 C CNN
	1    2150 7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E0B5CC5
P 4800 4000
F 0 "#PWR0107" H 4800 3750 50  0001 C CNN
F 1 "GND" H 4805 3827 50  0000 C CNN
F 2 "" H 4800 4000 50  0001 C CNN
F 3 "" H 4800 4000 50  0001 C CNN
	1    4800 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E0BD62A
P 5300 4000
F 0 "#PWR0108" H 5300 3750 50  0001 C CNN
F 1 "GND" H 5305 3827 50  0000 C CNN
F 2 "" H 5300 4000 50  0001 C CNN
F 3 "" H 5300 4000 50  0001 C CNN
	1    5300 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E0CC708
P 12050 5050
F 0 "#PWR0109" H 12050 4800 50  0001 C CNN
F 1 "GND" H 12055 4877 50  0000 C CNN
F 2 "" H 12050 5050 50  0001 C CNN
F 3 "" H 12050 5050 50  0001 C CNN
	1    12050 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5E0E4376
P 3750 9400
F 0 "#PWR0110" H 3750 9150 50  0001 C CNN
F 1 "GND" H 3755 9227 50  0000 C CNN
F 2 "" H 3750 9400 50  0001 C CNN
F 3 "" H 3750 9400 50  0001 C CNN
	1    3750 9400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 9100 2950 9100
Wire Wire Line
	2600 9650 2950 9650
Wire Bus Line
	5900 6400 5900 7600
Wire Bus Line
	7300 3900 7300 6400
Wire Bus Line
	7650 1700 7650 5450
Wire Bus Line
	4450 1700 4450 7850
Wire Bus Line
	8350 1550 8350 4650
$EndSCHEMATC
