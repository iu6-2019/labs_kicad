EESchema Schematic File Version 4
LIBS:test-cache
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема принципиальная\\nэлектрическая"
Date ""
Rev ""
Comp "МГТУ им. Н.Э. Баумана\\nИУ6-74"
Comment1 "Электронные часы\\nдля настольной игры"
Comment2 "Добрянская Е.М."
Comment3 "Хартов В.Я."
Comment4 ""
$EndDescr
$Comp
L Device:CP_Small C1
U 1 1 5DCBEE91
P 3400 1600
F 0 "C1" H 3488 1646 50  0000 L CNN
F 1 "100 нФ" H 3488 1555 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.3" H 3400 1600 50  0001 C CNN
F 3 "~" H 3400 1600 50  0001 C CNN
	1    3400 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C2
U 1 1 5DCBF507
P 4650 1600
F 0 "C2" H 4738 1646 50  0000 L CNN
F 1 "10 нФ" H 4738 1555 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.3" H 4650 1600 50  0001 C CNN
F 3 "~" H 4650 1600 50  0001 C CNN
	1    4650 1600
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7805 U2
U 1 1 5DCC199A
P 4150 1400
F 0 "U2" H 4150 1642 50  0000 C CNN
F 1 "L7805" H 4150 1551 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4175 1250 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 4150 1350 50  0001 C CNN
	1    4150 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1400 3400 1400
Wire Wire Line
	3400 1500 3400 1400
Wire Wire Line
	4450 1400 4650 1400
Wire Wire Line
	4650 1500 4650 1400
Wire Wire Line
	4150 1700 4150 1750
Connection ~ 4150 1750
Wire Wire Line
	4150 1750 4650 1750
Wire Wire Line
	4650 1700 4650 1750
Connection ~ 4650 1750
Wire Wire Line
	4650 1750 5250 1750
Wire Wire Line
	3400 1700 3400 1750
Wire Wire Line
	3400 1750 4150 1750
$Comp
L Display_Character:CC56-12EWA HG1
U 1 1 5DDB54C0
P 10350 2350
F 0 "HG1" H 10350 3017 50  0000 C CNN
F 1 "CC56-12EWA" H 10350 2926 50  0000 C CNN
F 2 "Display_7Segment:CA56-12EWA" H 10350 1750 50  0001 C CNN
F 3 "http://www.kingbrightusa.com/images/catalog/SPEC/CA56-12EWA.pdf" H 9920 2380 50  0001 C CNN
	1    10350 2350
	1    0    0    -1  
$EndComp
$Comp
L test-rescue:table3-tables XP3
U 1 1 5E390272
P 12750 7150
F 0 "XP3" H 13100 7200 50  0000 L CNN
F 1 "table3" H 12750 7150 50  0001 C CNN
F 2 "Connector_Dsub:DSUB-9_Male_Vertical_P2.77x2.84mm" H 12750 7150 50  0001 C CNN
F 3 "" H 12750 7150 50  0001 C CNN
	1    12750 7150
	1    0    0    -1  
$EndComp
Entry Wire Line
	13150 2750 13250 2650
Text Label 13150 2550 2    50   ~ 0
digit1
Text Label 13150 2450 2    50   ~ 0
digit0
$Comp
L power:+5V #PWR04
U 1 1 5DC5FD1B
P 3450 2900
F 0 "#PWR04" H 3450 2750 50  0001 C CNN
F 1 "+5V" H 3465 3073 50  0000 C CNN
F 2 "" H 3450 2900 50  0001 C CNN
F 3 "" H 3450 2900 50  0001 C CNN
	1    3450 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3550 3450 3550
Wire Wire Line
	3450 3550 3450 3650
Text Label 4550 3250 2    50   ~ 0
SCK
Text Label 4550 3350 2    50   ~ 0
MOSI
Entry Wire Line
	4550 3250 4650 3350
Entry Wire Line
	4550 3350 4650 3450
Text Label 7300 3950 2    50   ~ 0
a
Text Label 7300 4050 2    50   ~ 0
b
Text Label 7300 4150 2    50   ~ 0
c
Text Label 7300 4250 2    50   ~ 0
d
Text Label 7300 4350 2    50   ~ 0
e
Text Label 7300 4550 2    50   ~ 0
g
Text Label 7300 4450 2    50   ~ 0
f
Text Label 7300 4650 2    50   ~ 0
dp
Text Label 4550 3150 2    50   ~ 0
MISO
Entry Wire Line
	4550 3150 4650 3250
Text Label 7500 2050 0    50   ~ 0
a
Text Label 7500 2450 0    50   ~ 0
e
Text Label 7500 2150 0    50   ~ 0
b
Text Label 7500 2250 0    50   ~ 0
c
Text Label 7500 2350 0    50   ~ 0
d
Text Label 7500 2750 0    50   ~ 0
dp
Text Label 7500 2550 0    50   ~ 0
f
Text Label 7500 2650 0    50   ~ 0
g
Wire Wire Line
	6400 3950 7300 3950
Wire Wire Line
	6400 4050 7300 4050
Wire Wire Line
	6400 4150 7300 4150
Wire Wire Line
	6400 4250 7300 4250
Wire Wire Line
	6400 4350 7300 4350
Wire Wire Line
	6400 4450 7300 4450
Wire Wire Line
	6400 4550 7300 4550
Wire Wire Line
	6400 4650 7300 4650
Wire Wire Line
	3100 3050 3450 3050
Wire Wire Line
	3450 3050 3450 2900
Wire Wire Line
	2650 4450 2350 4450
Wire Wire Line
	2350 4450 2350 4600
$Comp
L Device:C_Small C4
U 1 1 5DC6E890
P 4250 5550
F 0 "C4" V 4021 5550 50  0000 C CNN
F 1 "22 пФ" V 4112 5550 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4250 5550 50  0001 C CNN
F 3 "~" H 4250 5550 50  0001 C CNN
	1    4250 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5DC7074B
P 4250 5900
F 0 "C5" V 4021 5900 50  0000 C CNN
F 1 "22 пФ" V 4112 5900 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4250 5900 50  0001 C CNN
F 3 "~" H 4250 5900 50  0001 C CNN
	1    4250 5900
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5DC71A7C
P 4700 5700
F 0 "Y1" V 4654 5831 50  0000 L CNN
F 1 "4 МГц" V 4745 5831 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-U_Horizontal_1EP_style1" H 4700 5700 50  0001 C CNN
F 3 "~" H 4700 5700 50  0001 C CNN
	1    4700 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 5550 3950 5550
Wire Wire Line
	4150 5900 3950 5900
Wire Wire Line
	4700 5850 4700 5900
Wire Wire Line
	4700 5900 4350 5900
Wire Wire Line
	4350 5550 4700 5550
$Comp
L power:+5V #PWR03
U 1 1 5DDFC44A
P 3300 4050
F 0 "#PWR03" H 3300 3900 50  0001 C CNN
F 1 "+5V" H 3315 4223 50  0000 C CNN
F 2 "" H 3300 4050 50  0001 C CNN
F 3 "" H 3300 4050 50  0001 C CNN
	1    3300 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5DE05FC4
P 3300 4250
F 0 "R1" H 3359 4296 50  0000 L CNN
F 1 "10 кОм" H 3359 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 3300 4250 50  0001 C CNN
F 3 "~" H 3300 4250 50  0001 C CNN
	1    3300 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 4150 3300 4050
Text Label 3700 8850 2    50   ~ 0
INT1
$Comp
L tables:SPI-XP XP2
U 1 1 5E4F4CC7
P 3000 2800
F 0 "XP2" H 2708 2773 50  0000 C CNN
F 1 "SPI-XP" H 3000 2800 50  0001 C CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Horizontal" H 3000 2800 50  0001 C CNN
F 3 "" H 3000 2800 50  0001 C CNN
	1    3000 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 8300 4500 8350
Wire Wire Line
	12150 7750 11850 7750
Wire Wire Line
	12150 7950 12150 8050
Wire Wire Line
	2250 8950 1950 8950
$Comp
L Switch:SW_Push SW4
U 1 1 5E3B8CE7
P 2450 8950
F 0 "SW4" H 2450 9235 50  0000 C CNN
F 1 "\"Игрок Б\"" H 2450 9144 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2450 9150 50  0001 C CNN
F 3 "~" H 2450 9150 50  0001 C CNN
	1    2450 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 8550 1950 8700
Wire Wire Line
	2250 8550 1950 8550
$Comp
L Switch:SW_Push SW3
U 1 1 5E3AD4CB
P 2450 8550
F 0 "SW3" H 2450 8835 50  0000 C CNN
F 1 "\"Игрок А\"" H 2450 8744 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2450 8750 50  0001 C CNN
F 3 "~" H 2450 8750 50  0001 C CNN
	1    2450 8550
	1    0    0    -1  
$EndComp
Text Label 13150 2650 2    50   ~ 0
digit2
Text Label 13150 2750 2    50   ~ 0
digit3
Entry Wire Line
	13150 2650 13250 2550
Entry Wire Line
	13150 2550 13250 2450
$Comp
L power:+5V #PWR012
U 1 1 5E4536EF
P 6500 3700
F 0 "#PWR012" H 6500 3550 50  0001 C CNN
F 1 "+5V" H 6515 3873 50  0000 C CNN
F 2 "" H 6500 3700 50  0001 C CNN
F 3 "" H 6500 3700 50  0001 C CNN
	1    6500 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 4350 3300 4450
Wire Wire Line
	3300 4450 3050 4450
$Comp
L Switch:SW_Push SW5
U 1 1 5DC6866E
P 2850 4450
F 0 "SW5" H 2850 4735 50  0000 C CNN
F 1 "\"reset\"" H 2850 4644 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2850 4650 50  0001 C CNN
F 3 "~" H 2850 4650 50  0001 C CNN
	1    2850 4450
	1    0    0    -1  
$EndComp
Text Label 6850 5650 2    50   ~ 0
digit1
Text Label 6850 5750 2    50   ~ 0
digit0
Wire Wire Line
	6400 5650 6850 5650
Wire Wire Line
	6400 5750 6850 5750
Text Label 6850 5450 2    50   ~ 0
digit3
Text Label 6850 5550 2    50   ~ 0
digit2
Wire Wire Line
	6400 5450 6850 5450
Wire Wire Line
	6400 5550 6850 5550
$Comp
L atmega8515-pdip:ATmega8515L-8PU DD1
U 1 1 5E113515
P 5800 5550
F 0 "DD1" H 5825 7617 50  0000 C CNN
F 1 "ATmega8515L-8PU" H 5825 7526 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 5850 5850 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2512.pdf" H 5800 5550 50  0001 C CNN
	1    5800 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3700 6500 3850
Wire Wire Line
	6500 3850 6400 3850
Wire Wire Line
	5200 5550 4700 5550
Connection ~ 4700 5550
Wire Wire Line
	5200 5650 5100 5650
Wire Wire Line
	5100 5650 5100 5900
Wire Wire Line
	5100 5900 4700 5900
Connection ~ 4700 5900
Wire Wire Line
	5200 5750 5200 6100
Entry Wire Line
	6950 5550 6850 5650
Entry Wire Line
	6950 5450 6850 5550
Entry Wire Line
	6950 5650 6850 5750
Entry Wire Line
	6950 5350 6850 5450
Entry Wire Line
	7400 4450 7300 4550
Entry Wire Line
	7400 4350 7300 4450
Entry Wire Line
	7400 4550 7300 4650
Entry Wire Line
	7400 4150 7300 4250
Entry Wire Line
	7400 4050 7300 4150
Entry Wire Line
	7400 3950 7300 4050
Entry Wire Line
	7400 4250 7300 4350
Entry Wire Line
	7400 3850 7300 3950
Entry Wire Line
	7400 2250 7500 2150
Entry Wire Line
	7400 2150 7500 2050
Entry Wire Line
	7400 2350 7500 2250
Entry Wire Line
	7400 2450 7500 2350
Entry Wire Line
	7400 2550 7500 2450
Entry Wire Line
	7400 2650 7500 2550
Entry Wire Line
	7400 2750 7500 2650
Entry Wire Line
	7400 2850 7500 2750
Wire Wire Line
	3100 3150 4550 3150
Wire Wire Line
	3100 3250 4550 3250
Wire Wire Line
	3100 3350 4550 3350
Text Label 4950 4250 2    50   ~ 0
SCK
Text Label 4950 4050 2    50   ~ 0
MOSI
Entry Wire Line
	4650 4050 4750 4150
Entry Wire Line
	4650 4150 4750 4250
Text Label 4950 4150 2    50   ~ 0
MISO
Entry Wire Line
	4650 3950 4750 4050
Wire Wire Line
	5200 4050 4750 4050
Wire Wire Line
	5200 4150 4750 4150
Wire Wire Line
	5200 4250 4750 4250
Wire Wire Line
	3100 3450 3800 3450
Text Label 2800 7400 0    50   ~ 0
TX
Text Label 2800 7800 0    50   ~ 0
RX
Entry Wire Line
	2700 7700 2800 7800
Entry Wire Line
	2700 7300 2800 7400
Wire Wire Line
	3950 5550 3950 5900
Connection ~ 3950 5900
Entry Wire Line
	2700 4850 2800 4750
Entry Wire Line
	2700 4950 2800 4850
Text Label 2800 4850 0    50   ~ 0
TX
Text Label 2800 4750 0    50   ~ 0
RX
Connection ~ 3300 4450
Wire Wire Line
	3800 4450 3300 4450
Wire Wire Line
	3800 3450 3800 4450
Wire Wire Line
	5200 4650 3800 4650
Wire Wire Line
	3950 5900 3950 6050
Wire Wire Line
	2650 8950 2850 8950
Wire Wire Line
	2850 8950 2850 8850
Wire Wire Line
	5200 4950 3900 4950
Entry Wire Line
	3800 5050 3900 4950
Entry Wire Line
	3800 5150 3900 5050
Wire Wire Line
	5200 5050 3900 5050
Text Label 3900 4950 0    50   ~ 0
INT0
Text Label 3900 5050 0    50   ~ 0
INT1
Text Label 3700 8550 2    50   ~ 0
INT0
Entry Wire Line
	3700 8550 3800 8450
Entry Wire Line
	3700 8850 3800 8750
Text Label 3900 8200 0    50   ~ 0
OC1A
Entry Wire Line
	3900 8200 3800 8100
Entry Wire Line
	3800 5350 3900 5250
Wire Wire Line
	5200 5250 3900 5250
Text Label 3900 5250 0    50   ~ 0
OC1A
$Comp
L Switch:SW_Push SW2
U 1 1 5E397CBC
P 8850 4750
F 0 "SW2" H 8850 5035 50  0000 C CNN
F 1 "\"result\"" H 8850 4944 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8850 4950 50  0001 C CNN
F 3 "~" H 8850 4950 50  0001 C CNN
	1    8850 4750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9050 4750 9350 4750
Wire Wire Line
	9350 4750 9350 4900
Wire Bus Line
	6950 1400 13250 1400
$Comp
L Device:R R3
U 1 1 5EB4EF62
P 8200 4750
F 0 "R3" V 7993 4750 50  0000 C CNN
F 1 "2кОм" V 8084 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8130 4750 50  0001 C CNN
F 3 "~" H 8200 4750 50  0001 C CNN
	1    8200 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	8650 4750 8350 4750
Wire Wire Line
	6400 4750 8050 4750
Connection ~ 3800 4450
Wire Wire Line
	3800 4450 3800 4650
$Comp
L power-gost:GND #PWR011
U 1 1 5EBFB2A3
P 4150 1750
F 0 "#PWR011" H 4150 1800 60  0001 C CNN
F 1 "GND" H 4150 1500 79  0001 C CNN
F 2 "" H 4150 1750 60  0000 C CNN
F 3 "" H 4150 1750 60  0001 C CNN
	1    4150 1750
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR07
U 1 1 5EC24C48
P 2350 4600
F 0 "#PWR07" H 2350 4650 60  0001 C CNN
F 1 "GND" H 2350 4350 79  0001 C CNN
F 2 "" H 2350 4600 60  0000 C CNN
F 3 "" H 2350 4600 60  0001 C CNN
	1    2350 4600
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR08
U 1 1 5EC59F7C
P 3450 3650
F 0 "#PWR08" H 3450 3700 60  0001 C CNN
F 1 "GND" H 3450 3400 79  0001 C CNN
F 2 "" H 3450 3650 60  0000 C CNN
F 3 "" H 3450 3650 60  0001 C CNN
	1    3450 3650
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR010
U 1 1 5EC8F37F
P 3950 6050
F 0 "#PWR010" H 3950 6100 60  0001 C CNN
F 1 "GND" H 3950 5800 79  0001 C CNN
F 2 "" H 3950 6050 60  0000 C CNN
F 3 "" H 3950 6050 60  0001 C CNN
	1    3950 6050
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR013
U 1 1 5EC9C8EC
P 5200 6100
F 0 "#PWR013" H 5200 6150 60  0001 C CNN
F 1 "GND" H 5200 5850 79  0001 C CNN
F 2 "" H 5200 6100 60  0000 C CNN
F 3 "" H 5200 6100 60  0001 C CNN
	1    5200 6100
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR015
U 1 1 5ECB6D40
P 9350 4900
F 0 "#PWR015" H 9350 4950 60  0001 C CNN
F 1 "GND" H 9350 4650 79  0001 C CNN
F 2 "" H 9350 4900 60  0000 C CNN
F 3 "" H 9350 4900 60  0001 C CNN
	1    9350 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:Speaker LS1
U 1 1 5E5188E5
P 4700 8200
F 0 "LS1" H 4870 8196 50  0000 L CNN
F 1 "Speaker" H 4870 8105 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_TDK_PS1240P02BT_D12.2mm_H6.5mm" H 4700 8000 50  0001 C CNN
F 3 "~" H 4690 8150 50  0001 C CNN
	1    4700 8200
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR014
U 1 1 5ED06D1E
P 4500 8350
F 0 "#PWR014" H 4500 8400 60  0001 C CNN
F 1 "GND" H 4500 8100 79  0001 C CNN
F 2 "" H 4500 8350 60  0000 C CNN
F 3 "" H 4500 8350 60  0001 C CNN
	1    4500 8350
	-1   0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR01
U 1 1 5ED146D4
P 1950 8700
F 0 "#PWR01" H 1950 8750 60  0001 C CNN
F 1 "GND" H 1950 8450 79  0001 C CNN
F 2 "" H 1950 8700 60  0000 C CNN
F 3 "" H 1950 8700 60  0001 C CNN
	1    1950 8700
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR02
U 1 1 5ED21C41
P 1950 8950
F 0 "#PWR02" H 1950 9000 60  0001 C CNN
F 1 "GND" H 1950 8700 79  0001 C CNN
F 2 "" H 1950 8950 60  0000 C CNN
F 3 "" H 1950 8950 60  0001 C CNN
	1    1950 8950
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR016
U 1 1 5ED70951
P 10000 8300
F 0 "#PWR016" H 10000 8350 60  0001 C CNN
F 1 "GND" H 10000 8050 79  0001 C CNN
F 2 "" H 10000 8300 60  0000 C CNN
F 3 "" H 10000 8300 60  0001 C CNN
	1    10000 8300
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR018
U 1 1 5ED8AAEE
P 11850 8300
F 0 "#PWR018" H 11850 8350 60  0001 C CNN
F 1 "GND" H 11850 8050 79  0001 C CNN
F 2 "" H 11850 8300 60  0000 C CNN
F 3 "" H 11850 8300 60  0001 C CNN
	1    11850 8300
	1    0    0    -1  
$EndComp
Connection ~ 4650 1400
Connection ~ 5250 1400
Wire Wire Line
	5250 1300 5250 1400
$Comp
L power:+5V #PWR09
U 1 1 5DCDA0BB
P 5250 1300
F 0 "#PWR09" H 5250 1150 50  0001 C CNN
F 1 "+5V" H 5265 1473 50  0000 C CNN
F 2 "" H 5250 1300 50  0001 C CNN
F 3 "" H 5250 1300 50  0001 C CNN
	1    5250 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 1700 5250 1750
Wire Wire Line
	5250 1500 5250 1400
Wire Wire Line
	4650 1400 5250 1400
$Comp
L Device:CP_Small C3
U 1 1 5DCC057E
P 5250 1600
F 0 "C3" H 5338 1646 50  0000 L CNN
F 1 "1 нФ" H 5338 1555 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 5250 1600 50  0001 C CNN
F 3 "~" H 5250 1600 50  0001 C CNN
	1    5250 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 6450 8800 6500
Wire Wire Line
	9200 6200 8800 6200
Wire Wire Line
	8800 6200 8800 6250
Wire Wire Line
	9200 6500 8800 6500
Wire Wire Line
	11100 6450 11100 6500
$Comp
L Device:CP_Small C7
U 1 1 5EE49F45
P 11100 6350
F 0 "C7" H 11188 6396 50  0000 L CNN
F 1 "1 мкФ" H 11188 6305 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 11100 6350 50  0001 C CNN
F 3 "~" H 11100 6350 50  0001 C CNN
	1    11100 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 6200 11100 6250
Wire Wire Line
	11100 6200 10800 6200
Wire Wire Line
	11100 6500 10800 6500
$Comp
L Device:CP_Small C8
U 1 1 5EE8E5F7
P 11150 6700
F 0 "C8" V 11250 6650 50  0000 L CNN
F 1 "1 мкФ" V 11050 6600 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.8" H 11150 6700 50  0001 C CNN
F 3 "~" H 11150 6700 50  0001 C CNN
	1    11150 6700
	0    1    1    0   
$EndComp
$Comp
L Device:CP_Small C6
U 1 1 5EE21197
P 8800 6350
F 0 "C6" H 8888 6396 50  0000 L CNN
F 1 "1 мкФ" H 8888 6305 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 8800 6350 50  0001 C CNN
F 3 "~" H 8800 6350 50  0001 C CNN
	1    8800 6350
	1    0    0    -1  
$EndComp
$Comp
L Interface_UART:MAX232 DD2
U 1 1 5E39EBED
P 10000 7100
F 0 "DD2" H 10000 8481 50  0000 C CNN
F 1 "MAX232" H 10000 8390 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 10050 6050 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 10000 7200 50  0001 C CNN
	1    10000 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C9
U 1 1 5EED398A
P 11150 7000
F 0 "C9" V 11250 6950 50  0000 L CNN
F 1 "1 мкФ" V 11050 6900 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x3" H 11150 7000 50  0001 C CNN
F 3 "~" H 11150 7000 50  0001 C CNN
	1    11150 7000
	0    1    1    0   
$EndComp
Wire Wire Line
	10800 6700 11050 6700
Wire Wire Line
	10800 7000 11050 7000
Wire Wire Line
	11250 7000 11550 7000
Wire Wire Line
	11550 7000 11550 7050
$Comp
L power-gost:GND #PWR021
U 1 1 5F01E76B
P 11550 7050
F 0 "#PWR021" H 11550 7100 60  0001 C CNN
F 1 "GND" H 11550 6800 79  0001 C CNN
F 2 "" H 11550 7050 60  0000 C CNN
F 3 "" H 11550 7050 60  0001 C CNN
	1    11550 7050
	1    0    0    -1  
$EndComp
Entry Wire Line
	13150 2450 13250 2350
Wire Wire Line
	11450 2450 13150 2450
Wire Wire Line
	11450 2550 13150 2550
Wire Wire Line
	11450 2650 13150 2650
Wire Wire Line
	11450 2750 13150 2750
Wire Wire Line
	3900 8200 4500 8200
$Comp
L power_upd:pwr XP1
U 1 1 5E32A102
P 3100 1950
F 0 "XP1" H 2608 2573 50  0000 C CNN
F 1 "pwr" H 2725 2570 50  0001 C CNN
F 2 "Connector:Banana_Jack_2Pin" H 2540 2139 50  0001 C CNN
F 3 "" H 3290 1589 50  0001 C CNN
	1    3100 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1650 3000 1750
Wire Wire Line
	3000 1750 3400 1750
Connection ~ 3400 1750
Wire Wire Line
	3000 1550 3000 1400
Wire Wire Line
	3000 1400 3400 1400
Connection ~ 3400 1400
$Comp
L Device:R R2
U 1 1 5E3666F3
P 3150 8550
F 0 "R2" V 2943 8550 50  0000 C CNN
F 1 "2кОм" V 3034 8550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3080 8550 50  0001 C CNN
F 3 "~" H 3150 8550 50  0001 C CNN
	1    3150 8550
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5E370AC0
P 3150 8850
F 0 "R4" V 2943 8850 50  0000 C CNN
F 1 "2кОм" V 3034 8850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3080 8850 50  0001 C CNN
F 3 "~" H 3150 8850 50  0001 C CNN
	1    3150 8850
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 8550 3700 8550
Wire Wire Line
	2650 8550 3000 8550
Wire Wire Line
	3300 8850 3700 8850
Wire Wire Line
	3000 8850 2850 8850
NoConn ~ 12150 7350
NoConn ~ 12150 8150
NoConn ~ 5200 3850
NoConn ~ 5200 3950
NoConn ~ 5200 4350
NoConn ~ 5200 4450
NoConn ~ 5200 4550
NoConn ~ 5200 5350
NoConn ~ 5200 5450
NoConn ~ 6400 4850
NoConn ~ 6400 4950
Wire Wire Line
	5200 4750 2800 4750
Wire Wire Line
	5200 4850 2800 4850
NoConn ~ 9200 7200
NoConn ~ 9200 7600
Wire Wire Line
	11850 7750 11850 8300
NoConn ~ 12150 7650
NoConn ~ 12150 7850
$Comp
L Device:R R13
U 1 1 5E6DA0ED
P 11150 7400
F 0 "R13" V 10943 7400 50  0000 C CNN
F 1 "470" V 11034 7400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 11080 7400 50  0001 C CNN
F 3 "~" H 11150 7400 50  0001 C CNN
	1    11150 7400
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5E6E41B8
P 11150 7800
F 0 "R14" V 10943 7800 50  0000 C CNN
F 1 "470" V 11034 7800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 11080 7800 50  0001 C CNN
F 3 "~" H 11150 7800 50  0001 C CNN
	1    11150 7800
	0    1    1    0   
$EndComp
Wire Wire Line
	11300 7800 11500 7800
Wire Wire Line
	11750 7800 11750 7550
Wire Wire Line
	11750 7550 12150 7550
Wire Wire Line
	12150 7450 11750 7450
Wire Wire Line
	11750 7450 11750 7400
Wire Wire Line
	11750 7400 11450 7400
NoConn ~ 10800 7600
NoConn ~ 10800 7200
Wire Wire Line
	9200 7800 2800 7800
Wire Wire Line
	9200 7400 2800 7400
Wire Wire Line
	11000 7400 10800 7400
Wire Wire Line
	11000 7800 10800 7800
$Comp
L Device:C C10
U 1 1 5E778ADD
P 11100 8200
F 0 "C10" H 11215 8246 50  0000 L CNN
F 1 "1.2 пФ" H 11215 8155 50  0000 L CNN
F 2 "Capacitor_THT:C_Axial_L3.8mm_D2.6mm_P7.50mm_Horizontal" H 11138 8050 50  0001 C CNN
F 3 "~" H 11100 8200 50  0001 C CNN
	1    11100 8200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 5E77A6C6
P 11500 8300
F 0 "C11" H 11615 8346 50  0000 L CNN
F 1 "1.2 пФ" H 11615 8255 50  0000 L CNN
F 2 "Capacitor_THT:C_Axial_L3.8mm_D2.6mm_P7.50mm_Horizontal" H 11538 8150 50  0001 C CNN
F 3 "~" H 11500 8300 50  0001 C CNN
	1    11500 8300
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 8050 11450 8050
Wire Wire Line
	11450 8050 11450 7400
Connection ~ 11450 7400
Wire Wire Line
	11450 7400 11300 7400
Wire Wire Line
	11500 8150 11500 7800
Connection ~ 11500 7800
Wire Wire Line
	11500 7800 11750 7800
$Comp
L power-gost:GND #PWR017
U 1 1 5E79A104
P 11100 8350
F 0 "#PWR017" H 11100 8400 60  0001 C CNN
F 1 "GND" H 11100 8100 79  0001 C CNN
F 2 "" H 11100 8350 60  0000 C CNN
F 3 "" H 11100 8350 60  0001 C CNN
	1    11100 8350
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR019
U 1 1 5E7B8E42
P 11500 8450
F 0 "#PWR019" H 11500 8500 60  0001 C CNN
F 1 "GND" H 11500 8200 79  0001 C CNN
F 2 "" H 11500 8450 60  0000 C CNN
F 3 "" H 11500 8450 60  0001 C CNN
	1    11500 8450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5E160F43
P 11500 6650
F 0 "#PWR0101" H 11500 6500 50  0001 C CNN
F 1 "+5V" H 11515 6823 50  0000 C CNN
F 2 "" H 11500 6650 50  0001 C CNN
F 3 "" H 11500 6650 50  0001 C CNN
	1    11500 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	11250 6700 11500 6700
Wire Wire Line
	11500 6700 11500 6650
$Comp
L power:+5V #PWR0102
U 1 1 5E18D184
P 10350 5850
F 0 "#PWR0102" H 10350 5700 50  0001 C CNN
F 1 "+5V" H 10365 6023 50  0000 C CNN
F 2 "" H 10350 5850 50  0001 C CNN
F 3 "" H 10350 5850 50  0001 C CNN
	1    10350 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 5850 10350 5900
Wire Wire Line
	10350 5900 10000 5900
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E1A1F82
P 6200 6300
F 0 "#FLG0101" H 6200 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 6200 6473 50  0000 C CNN
F 2 "" H 6200 6300 50  0001 C CNN
F 3 "~" H 6200 6300 50  0001 C CNN
	1    6200 6300
	1    0    0    -1  
$EndComp
$Comp
L power-gost:GND #PWR0103
U 1 1 5E1BA0DD
P 6200 6400
F 0 "#PWR0103" H 6200 6450 60  0001 C CNN
F 1 "GND" H 6200 6150 79  0001 C CNN
F 2 "" H 6200 6400 60  0000 C CNN
F 3 "" H 6200 6400 60  0001 C CNN
	1    6200 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 6300 6200 6400
NoConn ~ 6400 5050
NoConn ~ 6400 5150
NoConn ~ 6400 5250
NoConn ~ 6400 5350
Wire Wire Line
	9250 2050 7500 2050
Wire Wire Line
	9250 2150 7500 2150
Wire Wire Line
	9250 2250 7500 2250
Wire Wire Line
	9250 2350 7500 2350
Wire Wire Line
	9250 2450 7500 2450
Wire Wire Line
	9250 2550 7500 2550
Wire Wire Line
	9250 2650 7500 2650
Wire Wire Line
	9250 2750 7500 2750
NoConn ~ 5200 5150
Wire Bus Line
	6950 1400 6950 5800
Wire Bus Line
	13250 1400 13250 2900
Wire Bus Line
	4650 3100 4650 4300
Wire Bus Line
	2700 4750 2700 7850
Wire Bus Line
	3800 4900 3800 9850
Wire Bus Line
	7400 1950 7400 4650
$EndSCHEMATC
