EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема электрическая принципиальная"
Date ""
Rev ""
Comp "МГТУ им. Н.Э.Баумана Группа ИУ6-71Б"
Comment1 "Контроллер для испытательной камеры"
Comment2 "Гаипов М.М."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L _autosave-lab1-rescue:Earth-power #PWR013
U 1 1 5DD84A03
P 11450 8700
F 0 "#PWR013" H 11450 8450 50  0001 C CNN
F 1 "Earth" H 11450 8550 50  0001 C CNN
F 2 "" H 11450 8700 50  0001 C CNN
F 3 "~" H 11450 8700 50  0001 C CNN
	1    11450 8700
	1    0    0    -1  
$EndComp
Wire Wire Line
	11450 8550 11450 8700
$Comp
L _autosave-lab1-rescue:CP-Device C6
U 1 1 5DD875D5
P 10350 6600
F 0 "C6" H 10468 6646 50  0000 L CNN
F 1 "1 u" H 10468 6555 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_4x5.4" H 10388 6450 50  0001 C CNN
F 3 "~" H 10350 6600 50  0001 C CNN
	1    10350 6600
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:CP-Device C7
U 1 1 5DD88A12
P 12650 6600
F 0 "C7" H 12768 6646 50  0000 L CNN
F 1 "1 u" H 12768 6555 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 12688 6450 50  0001 C CNN
F 3 "~" H 12650 6600 50  0001 C CNN
	1    12650 6600
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:CP-Device C11
U 1 1 5DD89408
P 13300 6350
F 0 "C11" H 13418 6396 50  0000 L CNN
F 1 "1 u" H 13418 6305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 13338 6200 50  0001 C CNN
F 3 "~" H 13300 6350 50  0001 C CNN
	1    13300 6350
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:CP-Device C10
U 1 1 5DD8A4ED
P 13250 7100
F 0 "C10" H 13368 7146 50  0000 L CNN
F 1 "1 u" H 13368 7055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 13288 6950 50  0001 C CNN
F 3 "~" H 13250 7100 50  0001 C CNN
	1    13250 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	12250 6450 12650 6450
Wire Wire Line
	12650 6750 12250 6750
Wire Wire Line
	10650 6450 10350 6450
Wire Wire Line
	10650 6750 10350 6750
Wire Wire Line
	12250 6950 12800 6950
Wire Wire Line
	12800 6950 12800 6800
Wire Wire Line
	12800 6800 13300 6800
Wire Wire Line
	13300 6800 13300 6500
Wire Wire Line
	13300 6200 13300 6100
Wire Wire Line
	12250 7250 12950 7250
Wire Wire Line
	12950 7250 12950 6850
Wire Wire Line
	12950 6850 13250 6850
Wire Wire Line
	13250 6850 13250 6950
$Comp
L _autosave-lab1-rescue:Earth-power #PWR017
U 1 1 5DD95367
P 13250 7350
F 0 "#PWR017" H 13250 7100 50  0001 C CNN
F 1 "Earth" H 13250 7200 50  0001 C CNN
F 2 "" H 13250 7350 50  0001 C CNN
F 3 "~" H 13250 7350 50  0001 C CNN
	1    13250 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	13250 7250 13250 7350
$Comp
L _autosave-lab1-rescue:C-Device C9
U 1 1 5DDB07DC
P 13100 8400
F 0 "C9" H 13215 8446 50  0000 L CNN
F 1 "1.2 n" H 13215 8355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 13138 8250 50  0001 C CNN
F 3 "~" H 13100 8400 50  0001 C CNN
	1    13100 8400
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:C-Device C8
U 1 1 5DDB1099
P 12650 8400
F 0 "C8" H 12765 8446 50  0000 L CNN
F 1 "1.2 n" H 12765 8355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 12688 8250 50  0001 C CNN
F 3 "~" H 12650 8400 50  0001 C CNN
	1    12650 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	12450 7650 12250 7650
Wire Wire Line
	13700 7650 13700 7700
Wire Wire Line
	12750 7650 12900 7650
Wire Wire Line
	12450 8050 12250 8050
Wire Wire Line
	12750 8050 13100 8050
Wire Wire Line
	12650 8250 12650 8200
Wire Wire Line
	12650 8200 12900 8200
Wire Wire Line
	12900 8200 12900 7650
Connection ~ 12900 7650
Wire Wire Line
	13100 8250 13100 8050
Connection ~ 13100 8050
$Comp
L _autosave-lab1-rescue:Earth-power #PWR015
U 1 1 5DDC44F2
P 12650 8700
F 0 "#PWR015" H 12650 8450 50  0001 C CNN
F 1 "Earth" H 12650 8550 50  0001 C CNN
F 2 "" H 12650 8700 50  0001 C CNN
F 3 "~" H 12650 8700 50  0001 C CNN
	1    12650 8700
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:Earth-power #PWR016
U 1 1 5DDC4DC9
P 13100 8700
F 0 "#PWR016" H 13100 8450 50  0001 C CNN
F 1 "Earth" H 13100 8550 50  0001 C CNN
F 2 "" H 13100 8700 50  0001 C CNN
F 3 "~" H 13100 8700 50  0001 C CNN
	1    13100 8700
	1    0    0    -1  
$EndComp
Wire Wire Line
	12650 8700 12650 8550
Wire Wire Line
	13100 8550 13100 8700
Wire Wire Line
	3750 2550 3400 2550
Wire Wire Line
	3400 2550 3400 3000
Wire Wire Line
	3750 3000 3400 3000
Connection ~ 3400 3000
Wire Wire Line
	3400 3000 3400 3450
Wire Wire Line
	3750 3450 3400 3450
$Comp
L _autosave-lab1-rescue:R-Device R1
U 1 1 5DEE3953
P 4800 2550
F 0 "R1" V 4593 2550 50  0000 C CNN
F 1 "1 k" V 4684 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 4730 2550 50  0001 C CNN
F 3 "~" H 4800 2550 50  0001 C CNN
	1    4800 2550
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R2
U 1 1 5DEEA2DB
P 4800 3000
F 0 "R2" V 4593 3000 50  0000 C CNN
F 1 "1 k" V 4684 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 4730 3000 50  0001 C CNN
F 3 "~" H 4800 3000 50  0001 C CNN
	1    4800 3000
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R3
U 1 1 5DEEA711
P 4800 3450
F 0 "R3" V 4593 3450 50  0000 C CNN
F 1 "1 k" V 4684 3450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 4730 3450 50  0001 C CNN
F 3 "~" H 4800 3450 50  0001 C CNN
	1    4800 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3450 4650 3450
Wire Wire Line
	4150 3000 4650 3000
Wire Wire Line
	4150 2550 4650 2550
Entry Wire Line
	5600 2550 5700 2650
Wire Wire Line
	5600 2550 4950 2550
Entry Wire Line
	5600 3000 5700 3100
Wire Wire Line
	5600 3000 4950 3000
Entry Wire Line
	5600 3450 5700 3550
Wire Wire Line
	5600 3450 4950 3450
Text Label 5500 3000 0    50   ~ 0
d3
Text Label 5500 3450 0    50   ~ 0
d4
NoConn ~ 12250 7450
NoConn ~ 12250 7850
NoConn ~ 10650 7850
NoConn ~ 10650 7450
Text Label 7950 7650 0    50   ~ 0
Tx0
Wire Wire Line
	13100 8050 13700 8050
Wire Wire Line
	12900 7650 13700 7650
Wire Wire Line
	11700 6100 11700 6150
Connection ~ 11700 6100
Wire Wire Line
	13300 6100 11700 6100
Wire Wire Line
	11700 6150 11450 6150
Wire Wire Line
	11700 6050 11700 6100
$Comp
L _autosave-lab1-rescue:+5V-power #PWR014
U 1 1 5DD85D51
P 11700 6050
F 0 "#PWR014" H 11700 5900 50  0001 C CNN
F 1 "+5V" H 11715 6223 50  0000 C CNN
F 2 "" H 11700 6050 50  0001 C CNN
F 3 "" H 11700 6050 50  0001 C CNN
	1    11700 6050
	1    0    0    -1  
$EndComp
Connection ~ 3300 9150
$Comp
L _autosave-lab1-rescue:CP-Device C4
U 1 1 5DD5B013
P 5100 9450
F 0 "C4" H 5218 9496 50  0000 L CNN
F 1 "1 u" H 5218 9405 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 5138 9300 50  0001 C CNN
F 3 "~" H 5100 9450 50  0001 C CNN
	1    5100 9450
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:CP-Device C2
U 1 1 5DD5A843
P 4600 9450
F 0 "C2" H 4718 9496 50  0000 L CNN
F 1 "10 u" H 4718 9405 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 4638 9300 50  0001 C CNN
F 3 "~" H 4600 9450 50  0001 C CNN
	1    4600 9450
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:CP-Device C1
U 1 1 5DD598A2
P 3300 9400
F 0 "C1" H 3418 9446 50  0000 L CNN
F 1 "100 u" H 3418 9355 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 3338 9250 50  0001 C CNN
F 3 "~" H 3300 9400 50  0001 C CNN
	1    3300 9400
	1    0    0    -1  
$EndComp
Connection ~ 5100 9150
Wire Wire Line
	5100 8900 5100 9150
$Comp
L _autosave-lab1-rescue:+5V-power #PWR05
U 1 1 5DD58EEC
P 5100 8900
F 0 "#PWR05" H 5100 8750 50  0001 C CNN
F 1 "+5V" H 5115 9073 50  0000 C CNN
F 2 "" H 5100 8900 50  0001 C CNN
F 3 "" H 5100 8900 50  0001 C CNN
	1    5100 8900
	1    0    0    -1  
$EndComp
Connection ~ 4600 9700
Wire Wire Line
	5100 9700 4600 9700
Wire Wire Line
	5100 9600 5100 9700
Connection ~ 4600 9150
Wire Wire Line
	5100 9150 4600 9150
Wire Wire Line
	5100 9300 5100 9150
Wire Wire Line
	4600 9700 4100 9700
Wire Wire Line
	4600 9600 4600 9700
Wire Wire Line
	4600 9150 4600 9300
Wire Wire Line
	4400 9150 4600 9150
Connection ~ 4100 9700
Wire Wire Line
	4100 9800 4100 9700
$Comp
L _autosave-lab1-rescue:Earth-power #PWR03
U 1 1 5DD56A07
P 4100 9800
F 0 "#PWR03" H 4100 9550 50  0001 C CNN
F 1 "Earth" H 4100 9650 50  0001 C CNN
F 2 "" H 4100 9800 50  0001 C CNN
F 3 "~" H 4100 9800 50  0001 C CNN
	1    4100 9800
	1    0    0    -1  
$EndComp
Connection ~ 3300 9700
Wire Wire Line
	4100 9700 3300 9700
Wire Wire Line
	4100 9450 4100 9700
Wire Wire Line
	3800 9150 3300 9150
$Comp
L _autosave-lab1-rescue:LM7805_TO220-Regulator_Linear D1
U 1 1 5DD54E4A
P 4100 9150
F 0 "D1" H 4100 9392 50  0000 C CNN
F 1 "LM7805_TO220" H 4100 9301 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4100 9375 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 4100 9100 50  0001 C CNN
	1    4100 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 9700 3300 9550
Wire Wire Line
	2950 9500 2950 9700
Wire Wire Line
	3300 9150 3300 9250
Wire Wire Line
	2950 9350 2950 9150
$Comp
L _autosave-lab1-rescue:SW_Push-Switch SW3
U 1 1 5DEA7B72
P 3950 3450
F 0 "SW3" H 3950 3735 50  0000 C CNN
F 1 "\"Стоп\"" H 3950 3644 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P-B" H 3950 3650 50  0001 C CNN
F 3 "~" H 3950 3650 50  0001 C CNN
	1    3950 3450
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:SW_Push-Switch SW2
U 1 1 5DEA7345
P 3950 3000
F 0 "SW2" H 3950 3285 50  0000 C CNN
F 1 "\"Старт\"" H 3950 3194 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P-B" H 3950 3200 50  0001 C CNN
F 3 "~" H 3950 3200 50  0001 C CNN
	1    3950 3000
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:SW_Push-Switch SW1
U 1 1 5DEA6633
P 3950 2550
F 0 "SW1" H 3950 2835 50  0000 C CNN
F 1 "\"Дверь\"" H 3950 2744 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P-B" H 3950 2750 50  0001 C CNN
F 3 "~" H 3950 2750 50  0001 C CNN
	1    3950 2550
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R6
U 1 1 5DECB55D
P 7350 1750
F 0 "R6" V 7143 1750 50  0000 C CNN
F 1 "1 kOm" V 7234 1750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0614_L14.3mm_D5.7mm_P7.62mm_Vertical" V 7280 1750 50  0001 C CNN
F 3 "~" H 7350 1750 50  0001 C CNN
	1    7350 1750
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:+5V-power #PWR0101
U 1 1 5DF145A1
P 7350 1500
F 0 "#PWR0101" H 7350 1350 50  0001 C CNN
F 1 "+5V" H 7365 1673 50  0000 C CNN
F 2 "" H 7350 1500 50  0001 C CNN
F 3 "" H 7350 1500 50  0001 C CNN
	1    7350 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1500 7350 1600
Entry Wire Line
	5600 6300 5700 6400
Entry Wire Line
	5600 6400 5700 6500
$Comp
L _autosave-lab1-rescue:+5V-power #PWR0102
U 1 1 5DFD9BCA
P 4450 6700
F 0 "#PWR0102" H 4450 6550 50  0001 C CNN
F 1 "+5V" H 4465 6873 50  0000 C CNN
F 2 "" H 4450 6700 50  0001 C CNN
F 3 "" H 4450 6700 50  0001 C CNN
	1    4450 6700
	1    0    0    1   
$EndComp
Wire Wire Line
	4450 6700 4450 6600
$Comp
L _autosave-lab1-rescue:+5V-power #PWR0103
U 1 1 5DFEBB4F
P 4550 6700
F 0 "#PWR0103" H 4550 6550 50  0001 C CNN
F 1 "+5V" H 4565 6873 50  0000 C CNN
F 2 "" H 4550 6700 50  0001 C CNN
F 3 "" H 4550 6700 50  0001 C CNN
	1    4550 6700
	1    0    0    1   
$EndComp
Wire Wire Line
	4550 6700 4550 6600
Wire Wire Line
	4450 5800 4450 5650
NoConn ~ 3950 6100
Text Label 5450 6400 0    50   ~ 0
scl
Text Label 5400 6300 0    50   ~ 0
sda
$Comp
L _autosave-lab1-rescue:R-Device R4
U 1 1 5E06B734
P 6050 6550
F 0 "R4" V 5843 6550 50  0000 C CNN
F 1 "1 k" V 5934 6550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 5980 6550 50  0001 C CNN
F 3 "~" H 6050 6550 50  0001 C CNN
	1    6050 6550
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R5
U 1 1 5E095144
P 6050 6900
F 0 "R5" V 5843 6900 50  0000 C CNN
F 1 "1 k" V 5934 6900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 5980 6900 50  0001 C CNN
F 3 "~" H 6050 6900 50  0001 C CNN
	1    6050 6900
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:+5V-power #PWR0106
U 1 1 5E09D56C
P 6500 6300
F 0 "#PWR0106" H 6500 6150 50  0001 C CNN
F 1 "+5V" H 6515 6473 50  0000 C CNN
F 2 "" H 6500 6300 50  0001 C CNN
F 3 "" H 6500 6300 50  0001 C CNN
	1    6500 6300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 6550 6500 6550
Wire Wire Line
	6500 6300 6500 6550
Wire Wire Line
	6200 6900 6500 6900
Wire Wire Line
	6500 6900 6500 6550
Connection ~ 6500 6550
Entry Wire Line
	5700 6650 5800 6550
Entry Wire Line
	5700 7000 5800 6900
Wire Wire Line
	5900 6550 5800 6550
Wire Wire Line
	5900 6900 5800 6900
Text Label 5800 6550 0    50   ~ 0
scl
Text Label 5800 6900 0    50   ~ 0
sda
NoConn ~ 6300 2650
Wire Bus Line
	5700 1100 8900 1100
Entry Wire Line
	5700 2000 5800 1900
Text Label 5850 1900 0    50   ~ 0
reset
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0107
U 1 1 5E16D6FF
P 6900 5500
F 0 "#PWR0107" H 6900 5250 50  0001 C CNN
F 1 "Earth" H 6900 5350 50  0001 C CNN
F 2 "" H 6900 5500 50  0001 C CNN
F 3 "~" H 6900 5500 50  0001 C CNN
	1    6900 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 5350 6900 5500
$Comp
L _autosave-lab1-rescue:+5V-power #PWR0108
U 1 1 5E188E55
P 6900 2250
F 0 "#PWR0108" H 6900 2100 50  0001 C CNN
F 1 "+5V" H 6915 2423 50  0000 C CNN
F 2 "" H 6900 2250 50  0001 C CNN
F 3 "" H 6900 2250 50  0001 C CNN
	1    6900 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6900 2250 6900 2350
NoConn ~ 7000 2350
$Comp
L _autosave-lab1-rescue:CA56-12CGKWA-Display_Character HG1
U 1 1 5E1EB5D3
P 10900 1800
F 0 "HG1" H 10900 2467 50  0000 C CNN
F 1 "CA56-12CGKWA" H 10900 2376 50  0000 C CNN
F 2 "Display_7Segment:CA56-12CGKWA" H 10900 1200 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/CA56-12CGKWA(Ver.9A).pdf" H 10470 1830 50  0001 C CNN
	1    10900 1800
	1    0    0    -1  
$EndComp
NoConn ~ 9800 2200
$Comp
L _autosave-lab1-rescue:R-Device R8
U 1 1 5E2B97D6
P 9250 1600
F 0 "R8" V 9200 1400 50  0000 C CNN
F 1 "1 k" V 9134 1600 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9180 1600 50  0001 C CNN
F 3 "~" H 9250 1600 50  0001 C CNN
	1    9250 1600
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R9
U 1 1 5E2BA7BE
P 9250 1700
F 0 "R9" V 9200 1500 50  0000 C CNN
F 1 "1 k" V 9134 1700 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9180 1700 50  0001 C CNN
F 3 "~" H 9250 1700 50  0001 C CNN
	1    9250 1700
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R10
U 1 1 5E2C1A53
P 9250 1800
F 0 "R10" V 9200 1600 50  0000 C CNN
F 1 "1 k" V 9134 1800 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9180 1800 50  0001 C CNN
F 3 "~" H 9250 1800 50  0001 C CNN
	1    9250 1800
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R11
U 1 1 5E2C8DE4
P 9250 1900
F 0 "R11" V 9200 1700 50  0000 C CNN
F 1 "1 k" V 9134 1900 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9180 1900 50  0001 C CNN
F 3 "~" H 9250 1900 50  0001 C CNN
	1    9250 1900
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R12
U 1 1 5E2D004F
P 9250 2000
F 0 "R12" V 9200 1800 50  0000 C CNN
F 1 "1 k" V 9134 2000 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9180 2000 50  0001 C CNN
F 3 "~" H 9250 2000 50  0001 C CNN
	1    9250 2000
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R13
U 1 1 5E2D7212
P 9250 2100
F 0 "R13" V 9200 1900 50  0000 C CNN
F 1 "1 k" V 9134 2100 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9180 2100 50  0001 C CNN
F 3 "~" H 9250 2100 50  0001 C CNN
	1    9250 2100
	0    1    1    0   
$EndComp
Entry Wire Line
	9600 1600 9700 1500
Entry Wire Line
	9600 1700 9700 1600
Entry Wire Line
	9600 1800 9700 1700
Entry Wire Line
	9600 1900 9700 1800
Entry Wire Line
	9600 2000 9700 1900
Entry Wire Line
	9600 2100 9700 2000
Entry Wire Line
	9600 2200 9700 2100
Wire Wire Line
	9700 1500 9800 1500
Wire Wire Line
	9800 1600 9700 1600
Wire Wire Line
	9700 1700 9800 1700
Wire Wire Line
	9700 1800 9800 1800
Wire Wire Line
	9700 1900 9800 1900
Wire Wire Line
	9700 2000 9800 2000
Wire Wire Line
	9700 2100 9800 2100
Entry Wire Line
	9500 1500 9600 1600
Entry Wire Line
	9500 1600 9600 1700
Entry Wire Line
	9500 1700 9600 1800
Entry Wire Line
	9500 1800 9600 1900
Entry Wire Line
	9500 1900 9600 2000
Entry Wire Line
	9500 2000 9600 2100
Entry Wire Line
	9500 2100 9600 2200
Wire Wire Line
	9400 1500 9500 1500
Wire Wire Line
	9500 1600 9400 1600
Wire Wire Line
	9400 1700 9500 1700
Wire Wire Line
	9500 1800 9400 1800
Wire Wire Line
	9500 1900 9400 1900
Wire Wire Line
	9500 2000 9400 2000
Wire Wire Line
	9500 2100 9400 2100
Wire Wire Line
	12000 2200 12200 2200
Text Label 12200 1900 0    50   ~ 0
1
Text Label 12150 2000 0    50   ~ 0
2
Text Label 12150 2100 0    50   ~ 0
3
Text Label 12150 2200 0    50   ~ 0
4
Wire Wire Line
	9100 2000 9000 2000
Wire Wire Line
	9000 2100 9100 2100
$Comp
L _autosave-lab1-rescue:MAX232-Interface_UART DD7
U 1 1 5DD68D27
P 11450 7350
F 0 "DD7" H 11450 8731 50  0000 C CNN
F 1 "MAX232" H 11450 8640 50  0000 C CNN
F 2 "Transformer_SMD:Transformer_Ethernet_Bel_S558-5999-T7-F" H 11500 6300 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 11450 7450 50  0001 C CNN
	1    11450 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1800 9000 1800
Wire Wire Line
	9000 1700 9100 1700
Wire Wire Line
	9100 1600 9000 1600
Entry Wire Line
	8900 2200 9000 2100
Entry Wire Line
	8900 2100 9000 2000
Entry Wire Line
	8900 1800 9000 1700
Entry Wire Line
	8900 1700 9000 1600
Entry Wire Line
	8900 1600 9000 1500
Text Label 8050 4550 0    50   ~ 0
d2
Text Label 8650 4150 0    50   ~ 0
reset
Entry Wire Line
	8800 4150 8900 4250
Wire Wire Line
	8800 4150 7500 4150
Entry Wire Line
	8800 1900 8900 2000
Wire Wire Line
	7350 1900 8800 1900
Connection ~ 7350 1900
Entry Wire Line
	7650 4350 7750 4450
Entry Wire Line
	7650 4450 7750 4550
Text Label 7600 4350 0    50   ~ 0
Rx0
Text Label 7600 4450 0    50   ~ 0
Tx0
Entry Wire Line
	7750 7750 7850 7650
Entry Wire Line
	7750 8150 7850 8050
Wire Wire Line
	7850 7650 10650 7650
Entry Wire Line
	8450 2950 8550 3050
Entry Wire Line
	8450 3050 8550 3150
Entry Wire Line
	8450 3150 8550 3250
Text Label 8250 2950 0    50   ~ 0
b3
Text Label 8250 3050 0    50   ~ 0
b4
Text Label 8250 3150 0    50   ~ 0
b5
Wire Wire Line
	14550 8250 14450 8250
Wire Wire Line
	14250 7900 13700 7900
Wire Wire Line
	13700 7900 13700 8050
NoConn ~ 4950 6000
Wire Wire Line
	2950 9350 2550 9350
NoConn ~ 7500 3950
NoConn ~ 14500 7350
NoConn ~ 14500 8100
NoConn ~ 14500 8550
Wire Wire Line
	14550 7350 14500 7350
Wire Wire Line
	14550 8100 14500 8100
Wire Wire Line
	14550 8550 14500 8550
NoConn ~ 14500 7800
Wire Wire Line
	14550 7800 14500 7800
Wire Wire Line
	7500 4750 8800 4750
Text Label 8050 4650 0    50   ~ 0
d3
Text Label 8050 4750 0    50   ~ 0
d4
Entry Wire Line
	8800 2650 8900 2750
Entry Wire Line
	8800 2750 8900 2850
Wire Wire Line
	8800 2750 7500 2750
Entry Wire Line
	8800 2850 8900 2950
Text Label 8600 2650 0    50   ~ 0
b0
Text Label 8600 2750 0    50   ~ 0
b1
Text Label 8600 2850 0    50   ~ 0
b2
$Comp
L _autosave-lab1-rescue:RS-232-my_Library1 XP3
U 1 1 5F313C46
P 14950 7000
F 0 "XP3" H 15278 6195 50  0000 L CNN
F 1 "RS-232" H 15278 6104 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Male_Vertical_P2.77x2.84mm" H 14900 7450 50  0001 C CNN
F 3 "" H 14900 7450 50  0001 C CNN
	1    14950 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	14450 8250 14450 8400
Wire Wire Line
	14450 8400 14550 8400
Wire Wire Line
	14200 7950 14200 8100
Wire Wire Line
	14550 7950 14200 7950
$Comp
L _autosave-lab1-rescue:Earth-power #PWR018
U 1 1 5DDAD978
P 14200 8100
F 0 "#PWR018" H 14200 7850 50  0001 C CNN
F 1 "Earth" H 14200 7950 50  0001 C CNN
F 2 "" H 14200 8100 50  0001 C CNN
F 3 "~" H 14200 8100 50  0001 C CNN
	1    14200 8100
	1    0    0    -1  
$EndComp
Wire Wire Line
	13700 7700 13950 7700
Wire Wire Line
	13950 7700 13950 7500
Wire Wire Line
	13950 7500 14550 7500
Wire Wire Line
	14550 7650 14250 7650
Wire Wire Line
	14250 7650 14250 7900
$Comp
L _autosave-lab1-rescue:DS-201-my_Library1 XP1
U 1 1 5F583CCF
P 2250 9100
F 0 "XP1" H 2283 9215 50  0000 C CNN
F 1 "DS-201" H 2283 9124 50  0000 C CNN
F 2 "Connector:Banana_Jack_2Pin" H 2150 9150 50  0001 C CNN
F 3 "" H 2150 9150 50  0001 C CNN
	1    2250 9100
	1    0    0    -1  
$EndComp
Text Label 5500 2550 0    50   ~ 0
d2
Wire Wire Line
	2550 9500 2950 9500
Text Label 9700 1500 0    50   ~ 0
a
Text Label 9700 1600 0    50   ~ 0
b
Text Label 9700 1700 0    50   ~ 0
c
Text Label 9700 1800 0    50   ~ 0
d
Text Label 9700 1900 0    50   ~ 0
e
Text Label 9700 2000 0    50   ~ 0
f
Text Label 9700 2100 0    50   ~ 0
g
Text Label 9450 1500 0    50   ~ 0
a
Text Label 9450 1600 0    50   ~ 0
b
Text Label 9450 1700 0    50   ~ 0
c
Text Label 9450 1800 0    50   ~ 0
d
Text Label 9450 1900 0    50   ~ 0
e
Text Label 9450 2000 0    50   ~ 0
f
Text Label 9450 2100 0    50   ~ 0
g
Text Label 7900 8050 0    50   ~ 0
Rx0
Text Label 9000 1700 0    50   ~ 0
b2
Text Label 9000 1800 0    50   ~ 0
b3
Text Label 9000 2000 0    50   ~ 0
b5
Text Label 9000 2100 0    50   ~ 0
b6
Wire Wire Line
	8800 2650 7500 2650
Text Label 8600 3250 0    50   ~ 0
b6
Wire Wire Line
	7850 8050 10650 8050
Wire Wire Line
	5800 1900 6200 1900
Wire Wire Line
	4950 6400 5600 6400
Wire Wire Line
	4950 6300 5600 6300
NoConn ~ 3950 6400
$Comp
L _autosave-lab1-rescue:DS3231M-Timer_RTC DD3
U 1 1 5DE983FF
P 4450 6200
F 0 "DD3" H 4450 5619 50  0000 C CNN
F 1 "DS3231M" H 4450 5710 50  0000 C CNN
F 2 "Package_SO:SOIC-16W_7.5x10.3mm_P1.27mm" H 4450 5600 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/DS3231.pdf" H 4720 6250 50  0001 C CNN
	1    4450 6200
	-1   0    0    1   
$EndComp
NoConn ~ 7500 3350
$Comp
L _autosave-lab1-rescue:C-Device C15
U 1 1 5E093387
P 6200 2250
F 0 "C15" H 6315 2296 50  0000 L CNN
F 1 "22 p" H 6315 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 6238 2100 50  0001 C CNN
F 3 "~" H 6200 2250 50  0001 C CNN
	1    6200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2100 6200 1900
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0111
U 1 1 5E09338E
P 6200 2550
F 0 "#PWR0111" H 6200 2300 50  0001 C CNN
F 1 "Earth" H 6200 2400 50  0001 C CNN
F 2 "" H 6200 2550 50  0001 C CNN
F 3 "~" H 6200 2550 50  0001 C CNN
	1    6200 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2400 6200 2550
Connection ~ 6200 1900
Wire Wire Line
	6200 1900 7350 1900
Entry Wire Line
	8800 3250 8900 3350
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0109
U 1 1 5E1B9E23
P 10550 4600
F 0 "#PWR0109" H 10550 4350 50  0001 C CNN
F 1 "Earth-power" H 10550 4450 50  0001 C CNN
F 2 "" H 10550 4600 50  0001 C CNN
F 3 "" H 10550 4600 50  0001 C CNN
	1    10550 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3150 8450 3150
Wire Wire Line
	7500 3050 8450 3050
Wire Wire Line
	7500 2950 8450 2950
Wire Wire Line
	8800 2850 7500 2850
Wire Wire Line
	10050 4350 10250 4350
Wire Wire Line
	10550 4150 10550 2500
Wire Wire Line
	10550 2500 12200 2550
Wire Wire Line
	12200 2550 12200 2200
Wire Wire Line
	10550 4550 10550 4600
$Comp
L Transistor_BJT:2N3055 Q1
U 1 1 5E1BF58B
P 10450 4350
F 0 "Q1" H 10640 4396 50  0000 L CNN
F 1 "2N3055" H 10640 4305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-3" H 10650 4275 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N3055-D.PDF" H 10450 4350 50  0001 L CNN
	1    10450 4350
	1    0    0    -1  
$EndComp
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0110
U 1 1 5E2A0F86
P 10550 5200
F 0 "#PWR0110" H 10550 4950 50  0001 C CNN
F 1 "Earth-power" H 10550 5050 50  0001 C CNN
F 2 "" H 10550 5200 50  0001 C CNN
F 3 "" H 10550 5200 50  0001 C CNN
	1    10550 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 5150 10550 5200
$Comp
L Transistor_BJT:2N3055 Q2
U 1 1 5E2A0F8F
P 10450 4950
F 0 "Q2" H 10640 4996 50  0000 L CNN
F 1 "2N3055" H 10640 4905 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-3" H 10650 4875 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N3055-D.PDF" H 10450 4950 50  0001 L CNN
	1    10450 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 4750 11150 4750
Wire Wire Line
	11150 4750 11150 2800
Wire Wire Line
	11150 2800 12300 2750
Wire Wire Line
	12300 2100 12300 2750
Wire Wire Line
	12000 2100 12300 2100
Wire Wire Line
	9650 4450 9650 4950
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0112
U 1 1 5E2D99FC
P 10550 5800
F 0 "#PWR0112" H 10550 5550 50  0001 C CNN
F 1 "Earth-power" H 10550 5650 50  0001 C CNN
F 2 "" H 10550 5800 50  0001 C CNN
F 3 "" H 10550 5800 50  0001 C CNN
	1    10550 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 5750 10550 5800
$Comp
L Transistor_BJT:2N3055 Q3
U 1 1 5E2D9A05
P 10450 5550
F 0 "Q3" H 10640 5596 50  0000 L CNN
F 1 "2N3055" H 10640 5505 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-3" H 10650 5475 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N3055-D.PDF" H 10450 5550 50  0001 L CNN
	1    10450 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 5350 11400 5350
Wire Wire Line
	11400 5350 11350 2950
Wire Wire Line
	11350 2950 12500 2950
Wire Wire Line
	12500 2950 12550 2000
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0113
U 1 1 5E2FD976
P 10250 6300
F 0 "#PWR0113" H 10250 6050 50  0001 C CNN
F 1 "Earth-power" H 10250 6150 50  0001 C CNN
F 2 "" H 10250 6300 50  0001 C CNN
F 3 "" H 10250 6300 50  0001 C CNN
	1    10250 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 6050 9950 6050
Wire Wire Line
	10250 6250 10250 6300
$Comp
L Transistor_BJT:2N3055 Q4
U 1 1 5E2FD97F
P 10150 6050
F 0 "Q4" H 10340 6096 50  0000 L CNN
F 1 "2N3055" H 10340 6005 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-3" H 10350 5975 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N3055-D.PDF" H 10150 6050 50  0001 L CNN
	1    10150 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 6050 9450 6050
Wire Wire Line
	10250 5850 10450 5850
Wire Wire Line
	10450 5850 10450 5950
Wire Wire Line
	10450 5950 11300 5950
Wire Wire Line
	11300 5950 11300 5550
Wire Wire Line
	11300 5550 12850 5550
Wire Wire Line
	12850 5550 12800 1900
Wire Wire Line
	12000 1900 12800 1900
Wire Wire Line
	12000 2000 12550 2000
NoConn ~ 7500 3850
NoConn ~ 7500 3750
NoConn ~ 7500 3650
NoConn ~ 7500 3550
Wire Bus Line
	8350 3500 8350 5450
$Comp
L _autosave-lab1-rescue:R-Device R14
U 1 1 5E4327E4
P 9900 4350
F 0 "R14" V 9693 4350 50  0000 C CNN
F 1 "1 k" V 9784 4350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9830 4350 50  0001 C CNN
F 3 "~" H 9900 4350 50  0001 C CNN
	1    9900 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 4950 9750 4950
Wire Wire Line
	10050 4950 10250 4950
$Comp
L _autosave-lab1-rescue:R-Device R15
U 1 1 5E47CE49
P 9900 4950
F 0 "R15" V 9693 4950 50  0000 C CNN
F 1 "1 k" V 9784 4950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9830 4950 50  0001 C CNN
F 3 "~" H 9900 4950 50  0001 C CNN
	1    9900 4950
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R16
U 1 1 5E4A21EB
P 9900 5550
F 0 "R16" V 9693 5550 50  0000 C CNN
F 1 "1 k" V 9784 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9830 5550 50  0001 C CNN
F 3 "~" H 9900 5550 50  0001 C CNN
	1    9900 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	10050 5550 10250 5550
$Comp
L _autosave-lab1-rescue:R-Device R17
U 1 1 5E4F6D3B
P 9600 6050
F 0 "R17" V 9393 6050 50  0000 C CNN
F 1 "1 k" V 9484 6050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9530 6050 50  0001 C CNN
F 3 "~" H 9600 6050 50  0001 C CNN
	1    9600 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 5550 9450 5550
Wire Wire Line
	9450 5550 9450 4550
Wire Wire Line
	9350 4650 9350 6050
$Comp
L _autosave-lab1-rescue:R-Device R19
U 1 1 5DDB2E05
P 12600 8050
F 0 "R19" V 12393 8050 50  0000 C CNN
F 1 "470" V 12484 8050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 12530 8050 50  0001 C CNN
F 3 "~" H 12600 8050 50  0001 C CNN
	1    12600 8050
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:R-Device R18
U 1 1 5DDB24C3
P 12600 7650
F 0 "R18" V 12393 7650 50  0000 C CNN
F 1 "470" V 12484 7650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 12530 7650 50  0001 C CNN
F 3 "~" H 12600 7650 50  0001 C CNN
	1    12600 7650
	0    1    1    0   
$EndComp
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0104
U 1 1 5E00727B
P 3400 3750
F 0 "#PWR0104" H 3400 3500 50  0001 C CNN
F 1 "Earth" H 3400 3600 50  0001 C CNN
F 2 "" H 3400 3750 50  0001 C CNN
F 3 "~" H 3400 3750 50  0001 C CNN
	1    3400 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 3450 3400 3750
Connection ~ 3400 3450
Entry Wire Line
	8800 4750 8900 4850
$Comp
L _autosave-lab1-rescue:ATmega328P-PU-MCU_Microchip_ATmega DD1
U 1 1 5DF1E97A
P 6900 3850
F 0 "DD1" H 6256 3896 50  0000 R CNN
F 1 "ATmega328P-PU" H 6366 3805 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 6900 3850 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 6900 3850 50  0001 C CNN
	1    6900 3850
	1    0    0    -1  
$EndComp
NoConn ~ 7500 4050
NoConn ~ 7500 4850
NoConn ~ 7500 4950
NoConn ~ 7500 5050
$Comp
L _autosave-lab1-rescue:Earth-power #PWR0105
U 1 1 5E634824
P 4450 5650
F 0 "#PWR0105" H 4450 5400 50  0001 C CNN
F 1 "Earth" H 4450 5500 50  0001 C CNN
F 2 "" H 4450 5650 50  0001 C CNN
F 3 "~" H 4450 5650 50  0001 C CNN
	1    4450 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 4650 9350 4650
Text Label 9000 1600 0    50   ~ 0
b1
Text Label 9000 1500 0    50   ~ 0
b0
Wire Wire Line
	9000 1500 9100 1500
$Comp
L _autosave-lab1-rescue:R-Device R7
U 1 1 5E1F4142
P 9250 1500
F 0 "R7" V 9200 1300 50  0000 C CNN
F 1 "1 k" V 9134 1500 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P30.48mm_Horizontal" V 9180 1500 50  0001 C CNN
F 3 "~" H 9250 1500 50  0001 C CNN
	1    9250 1500
	0    1    1    0   
$EndComp
Text Label 9000 1900 0    50   ~ 0
b4
Entry Wire Line
	8900 1900 9000 1800
Entry Wire Line
	8900 2000 9000 1900
Wire Wire Line
	9000 1900 9100 1900
Wire Wire Line
	2950 9150 3300 9150
Wire Wire Line
	2950 9700 3300 9700
Wire Wire Line
	7500 4550 9450 4550
Wire Wire Line
	7500 3250 8800 3250
Wire Wire Line
	7500 4350 9750 4350
Wire Wire Line
	7500 4450 9650 4450
Wire Bus Line
	8550 2800 8550 6700
Wire Bus Line
	7750 4350 7750 8250
Wire Bus Line
	5700 1100 5700 7000
Wire Bus Line
	8900 1100 8900 5300
$EndSCHEMATC
