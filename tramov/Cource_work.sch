EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема электрическая \\n принципиальная"
Date "2019-12-04"
Rev ""
Comp "МГТУ им. Н.Э.Баумана \\n ИУ6-73"
Comment1 "Устройство для обработки \\n аналоговых сигналов"
Comment2 "Трамов И.Б."
Comment3 "Хохлов С.А."
Comment4 ""
$EndDescr
Wire Wire Line
	7950 2750 7850 2750
Wire Wire Line
	7850 2750 7850 2650
Wire Wire Line
	7850 2650 7450 2650
Wire Wire Line
	7950 2950 7450 2950
$Comp
L MCU_Microchip_ATmega:ATmega16-16AU U3
U 1 1 5DED207E
P 8550 4250
F 0 "U3" H 8550 2161 50  0000 C CNN
F 1 "ATmega16-16AU" H 8550 2070 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 8550 4250 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2466.pdf" H 8550 4250 50  0001 C CNN
	1    8550 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5DEDD167
P 7050 2600
F 0 "C3" V 6798 2600 50  0000 C CNN
F 1 "15пкФ" V 6889 2600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 2450 50  0001 C CNN
F 3 "~" H 7050 2600 50  0001 C CNN
	1    7050 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 2600 7350 2600
Wire Wire Line
	7350 2600 7350 2650
Wire Wire Line
	7350 2650 7450 2650
Connection ~ 7450 2650
Wire Wire Line
	7200 3000 7350 3000
Wire Wire Line
	7350 3000 7350 2950
Wire Wire Line
	7350 2950 7450 2950
Connection ~ 7450 2950
Wire Wire Line
	6900 2600 6800 2600
Wire Wire Line
	6800 2600 6800 3000
Wire Wire Line
	6900 3000 6800 3000
$Comp
L Device:Crystal Q2
U 1 1 5DEEBE9E
P 10200 5100
F 0 "Q2" V 10154 5231 50  0000 L CNN
F 1 "32кГц" V 10245 5231 50  0000 L CNN
F 2 "Crystal:Crystal_AT310_D3.0mm_L10.0mm_Horizontal" H 10200 5100 50  0001 C CNN
F 3 "~" H 10200 5100 50  0001 C CNN
	1    10200 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	9950 4900 10100 4900
Wire Wire Line
	10100 4900 10100 4950
Wire Wire Line
	10100 4950 10200 4950
Wire Wire Line
	9950 5300 10100 5300
Wire Wire Line
	10100 5300 10100 5250
Wire Wire Line
	10100 5250 10200 5250
Wire Wire Line
	9650 4900 9550 4900
Wire Wire Line
	9550 4900 9550 5300
Wire Wire Line
	9650 5300 9550 5300
Wire Wire Line
	10700 5050 10600 5050
Wire Wire Line
	10600 5050 10600 4950
Wire Wire Line
	10600 4950 10200 4950
Connection ~ 10200 4950
Wire Wire Line
	10700 5150 10600 5150
Wire Wire Line
	10600 5150 10600 5250
Wire Wire Line
	10600 5250 10200 5250
Connection ~ 10200 5250
Wire Wire Line
	10700 4750 10700 4350
Wire Wire Line
	10700 4350 9750 4350
Wire Wire Line
	10700 4850 10600 4850
Wire Wire Line
	10600 4850 10600 4450
Wire Wire Line
	10600 4450 10150 4450
Wire Wire Line
	9750 3900 9750 3950
Wire Wire Line
	10150 3900 9750 3900
Wire Wire Line
	10150 4250 10150 4450
Connection ~ 10150 4450
Wire Wire Line
	10150 4450 9150 4450
Wire Wire Line
	9750 4250 9750 4350
Connection ~ 9750 4350
Wire Wire Line
	9750 4350 9150 4350
$Comp
L Sensor_Temperature:LM35-D U5
U 1 1 5DF4CD16
P 10900 1300
F 0 "U5" H 10571 1346 50  0000 R CNN
F 1 "LM35-D" H 10571 1255 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 10900 900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm35.pdf" H 10900 1300 50  0001 C CNN
	1    10900 1300
	-1   0    0    -1  
$EndComp
NoConn ~ 11700 4950
NoConn ~ 11200 4550
NoConn ~ 9150 5250
NoConn ~ 9150 5350
NoConn ~ 9150 5450
NoConn ~ 9150 5550
NoConn ~ 9150 5650
NoConn ~ 9150 5750
NoConn ~ 9150 5950
NoConn ~ 9150 5050
NoConn ~ 9150 4950
NoConn ~ 9150 4850
NoConn ~ 9150 4750
NoConn ~ 9150 4650
NoConn ~ 9150 4550
NoConn ~ 9150 3750
NoConn ~ 9150 3650
NoConn ~ 9150 3550
NoConn ~ 9150 3450
NoConn ~ 9150 3250
NoConn ~ 9150 3150
NoConn ~ 9150 3050
NoConn ~ 9150 2950
NoConn ~ 9150 2850
NoConn ~ 9150 2750
NoConn ~ 7950 3150
NoConn ~ 8650 2250
Wire Wire Line
	8550 2250 8550 2200
$Comp
L Timer_RTC:DS1307+ U6
U 1 1 5DE7CE4D
P 11200 4950
F 0 "U6" H 11744 4996 50  0000 L CNN
F 1 "DS1307+" H 11744 4905 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 11200 4450 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/DS1307.pdf" H 11200 4750 50  0001 C CNN
	1    11200 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5E05B49D
P 7050 3000
F 0 "C4" V 6798 3000 50  0000 C CNN
F 1 "15пкФ" V 6889 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7088 2850 50  0001 C CNN
F 3 "~" H 7050 3000 50  0001 C CNN
	1    7050 3000
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5E05FEB4
P 9800 4900
F 0 "C6" V 9548 4900 50  0000 C CNN
F 1 "15пкФ" V 9639 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 9838 4750 50  0001 C CNN
F 3 "~" H 9800 4900 50  0001 C CNN
	1    9800 4900
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 5E061664
P 9800 5300
F 0 "C7" V 9548 5300 50  0000 C CNN
F 1 "15пкФ" V 9639 5300 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 9838 5150 50  0001 C CNN
F 3 "~" H 9800 5300 50  0001 C CNN
	1    9800 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	10150 3950 10150 3900
$Comp
L Sensor_Temperature:LM35-D U4
U 1 1 5E044449
P 9700 1300
F 0 "U4" H 9371 1346 50  0000 R CNN
F 1 "LM35-D" H 9371 1255 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9700 900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm35.pdf" H 9700 1300 50  0001 C CNN
	1    9700 1300
	-1   0    0    -1  
$EndComp
$Comp
L Device:Crystal Q1
U 1 1 5DEDA884
P 7450 2800
F 0 "Q1" V 7404 2931 50  0000 L CNN
F 1 "8МГц" V 7495 2931 50  0000 L CNN
F 2 "Crystal:Crystal_AT310_D3.0mm_L10.0mm_Horizontal" H 7450 2800 50  0001 C CNN
F 3 "~" H 7450 2800 50  0001 C CNN
	1    7450 2800
	0    1    1    0   
$EndComp
$Comp
L XP:KF350 U1
U 1 1 5E08D734
P 3750 7300
F 0 "U1" H 4107 7417 50  0000 C CNN
F 1 "KF350" H 4107 7326 50  0000 C CNN
F 2 "Cource_work:Power_Socket" H 4000 7300 50  0001 C CNN
F 3 "" H 4000 7300 50  0001 C CNN
	1    3750 7300
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:uA7805 U2
U 1 1 5DF05B52
P 6250 7650
F 0 "U2" H 6250 7892 50  0000 C CNN
F 1 "uA7805" H 6250 7801 50  0000 C CNN
F 2 "Cource_work:Stabilizer" H 6275 7500 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/ua78.pdf" H 6250 7600 50  0001 C CNN
	1    6250 7650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 7850 5200 7850
Wire Wire Line
	5200 7850 5200 8100
Wire Wire Line
	6250 8100 6250 7950
$Comp
L Device:CP C1
U 1 1 5DF41CAA
P 5500 7850
F 0 "C1" H 5618 7896 50  0000 L CNN
F 1 "100мкФ" H 5618 7805 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 5538 7700 50  0001 C CNN
F 3 "~" H 5500 7850 50  0001 C CNN
	1    5500 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 8100 5500 8100
Wire Wire Line
	5000 7650 5500 7650
$Comp
L Device:CP C5
U 1 1 5DF44CB9
P 7450 7850
F 0 "C5" H 7568 7896 50  0000 L CNN
F 1 "1мкФ" H 7568 7805 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 7488 7700 50  0001 C CNN
F 3 "~" H 7450 7850 50  0001 C CNN
	1    7450 7850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5DF459C6
P 6800 7850
F 0 "C2" H 6918 7896 50  0000 L CNN
F 1 "10мкФ" H 6918 7805 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 6838 7700 50  0001 C CNN
F 3 "~" H 6800 7850 50  0001 C CNN
	1    6800 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 7700 5500 7650
Connection ~ 5500 7650
Wire Wire Line
	5500 7650 5950 7650
Wire Wire Line
	5500 8000 5500 8100
Connection ~ 5500 8100
Wire Wire Line
	5500 8100 6250 8100
Wire Wire Line
	7450 7700 7450 7650
Wire Wire Line
	6550 7650 6800 7650
Wire Wire Line
	6800 7700 6800 7650
Connection ~ 6800 7650
Wire Wire Line
	6800 7650 7450 7650
Wire Wire Line
	7450 8000 7450 8100
Wire Wire Line
	7450 8100 6800 8100
Connection ~ 6250 8100
Wire Wire Line
	6800 8000 6800 8100
Connection ~ 6800 8100
Wire Wire Line
	6800 8100 6250 8100
Wire Wire Line
	7450 8100 7900 8100
Connection ~ 7450 8100
Wire Wire Line
	7450 7650 7900 7650
Connection ~ 7450 7650
Text Label 7900 7650 0    50   ~ 0
V
Text Label 7900 8100 0    50   ~ 0
G
$Comp
L Device:R R1
U 1 1 5DFA1B38
P 9750 4100
F 0 "R1" H 9820 4146 50  0000 L CNN
F 1 "10кОм" H 9820 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 9680 4100 50  0001 C CNN
F 3 "~" H 9750 4100 50  0001 C CNN
	1    9750 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5DFA2073
P 10150 4100
F 0 "R2" H 10220 4146 50  0000 L CNN
F 1 "10кОм" H 10220 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10080 4100 50  0001 C CNN
F 3 "~" H 10150 4100 50  0001 C CNN
	1    10150 4100
	1    0    0    -1  
$EndComp
NoConn ~ 10700 1600
NoConn ~ 10800 1600
NoConn ~ 11000 1600
NoConn ~ 11000 1000
NoConn ~ 10500 1200
NoConn ~ 9500 1600
NoConn ~ 9600 1600
NoConn ~ 9800 1600
NoConn ~ 9300 1200
NoConn ~ 9800 1000
NoConn ~ 9150 5850
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5DF130CA
P 5900 6900
F 0 "#FLG01" H 5900 6975 50  0001 C CNN
F 1 "PWR_FLAG" H 5900 7073 50  0000 C CNN
F 2 "" H 5900 6900 50  0001 C CNN
F 3 "~" H 5900 6900 50  0001 C CNN
	1    5900 6900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5DF15F5F
P 5900 6900
F 0 "#PWR01" H 5900 6750 50  0001 C CNN
F 1 "VCC" H 5917 7073 50  0000 C CNN
F 2 "" H 5900 6900 50  0001 C CNN
F 3 "" H 5900 6900 50  0001 C CNN
	1    5900 6900
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5DF29D03
P 8550 6250
F 0 "#PWR05" H 8550 6000 50  0001 C CNN
F 1 "GND" H 8555 6077 50  0000 C CNN
F 2 "" H 8550 6250 50  0001 C CNN
F 3 "" H 8550 6250 50  0001 C CNN
	1    8550 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5DF2A7A1
P 11200 5350
F 0 "#PWR013" H 11200 5100 50  0001 C CNN
F 1 "GND" H 11205 5177 50  0000 C CNN
F 2 "" H 11200 5350 50  0001 C CNN
F 3 "" H 11200 5350 50  0001 C CNN
	1    11200 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5DF31BE5
P 10900 1600
F 0 "#PWR011" H 10900 1350 50  0001 C CNN
F 1 "GND" H 10905 1427 50  0000 C CNN
F 2 "" H 10900 1600 50  0001 C CNN
F 3 "" H 10900 1600 50  0001 C CNN
	1    10900 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5DF35135
P 9700 1600
F 0 "#PWR09" H 9700 1350 50  0001 C CNN
F 1 "GND" H 9705 1427 50  0000 C CNN
F 2 "" H 9700 1600 50  0001 C CNN
F 3 "" H 9700 1600 50  0001 C CNN
	1    9700 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5DF38FA1
P 6800 3050
F 0 "#PWR03" H 6800 2800 50  0001 C CNN
F 1 "GND" H 6805 2877 50  0000 C CNN
F 2 "" H 6800 3050 50  0001 C CNN
F 3 "" H 6800 3050 50  0001 C CNN
	1    6800 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3050 6800 3000
Connection ~ 6800 3000
$Comp
L power:GND #PWR06
U 1 1 5DF418CC
P 9550 5350
F 0 "#PWR06" H 9550 5100 50  0001 C CNN
F 1 "GND" H 9555 5177 50  0000 C CNN
F 2 "" H 9550 5350 50  0001 C CNN
F 3 "" H 9550 5350 50  0001 C CNN
	1    9550 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 5350 9550 5300
Connection ~ 9550 5300
$Comp
L power:VCC #PWR012
U 1 1 5DF4BB47
P 11100 4550
F 0 "#PWR012" H 11100 4400 50  0001 C CNN
F 1 "VCC" H 11117 4723 50  0000 C CNN
F 2 "" H 11100 4550 50  0001 C CNN
F 3 "" H 11100 4550 50  0001 C CNN
	1    11100 4550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 5DF51268
P 10150 3850
F 0 "#PWR07" H 10150 3700 50  0001 C CNN
F 1 "VCC" H 10167 4023 50  0000 C CNN
F 2 "" H 10150 3850 50  0001 C CNN
F 3 "" H 10150 3850 50  0001 C CNN
	1    10150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 3850 10150 3900
Connection ~ 10150 3900
$Comp
L power:VCC #PWR010
U 1 1 5DF57561
P 10900 950
F 0 "#PWR010" H 10900 800 50  0001 C CNN
F 1 "VCC" H 10917 1123 50  0000 C CNN
F 2 "" H 10900 950 50  0001 C CNN
F 3 "" H 10900 950 50  0001 C CNN
	1    10900 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 950  10900 1000
$Comp
L power:VCC #PWR08
U 1 1 5DF5DA07
P 9700 950
F 0 "#PWR08" H 9700 800 50  0001 C CNN
F 1 "VCC" H 9717 1123 50  0000 C CNN
F 2 "" H 9700 950 50  0001 C CNN
F 3 "" H 9700 950 50  0001 C CNN
	1    9700 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 950  9700 1000
$Comp
L power:VCC #PWR04
U 1 1 5DF63B26
P 8550 2200
F 0 "#PWR04" H 8550 2050 50  0001 C CNN
F 1 "VCC" H 8567 2373 50  0000 C CNN
F 2 "" H 8550 2200 50  0001 C CNN
F 3 "" H 8550 2200 50  0001 C CNN
	1    8550 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2550 9150 2550
Wire Wire Line
	9300 1300 9300 2550
Wire Wire Line
	10500 1300 10400 1300
Wire Wire Line
	10400 1300 10400 2650
Wire Wire Line
	10400 2650 9150 2650
$Comp
L XP:Programmer U7
U 1 1 5E0803F2
P 11250 2850
F 0 "U7" H 10822 2496 50  0000 R CNN
F 1 "Programmer" H 10822 2405 50  0000 R CNN
F 2 "Cource_work:Programmer" H 11250 3000 50  0001 C CNN
F 3 "" H 11250 3000 50  0001 C CNN
	1    11250 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9650 3150 9650 4150
Wire Wire Line
	9650 4150 9150 4150
Wire Wire Line
	9600 3250 9600 3950
Wire Wire Line
	9600 3950 9150 3950
Wire Wire Line
	9550 3050 9550 4050
Wire Wire Line
	9550 4050 9150 4050
Wire Wire Line
	9450 3450 9450 3850
Wire Wire Line
	9450 3850 9150 3850
Wire Wire Line
	10100 3350 10100 1950
Wire Wire Line
	10100 1950 7850 1950
Wire Wire Line
	7950 2550 7850 2550
Wire Wire Line
	7850 2550 7850 1950
Wire Wire Line
	9550 3050 10750 3050
Wire Wire Line
	9650 3150 10750 3150
Wire Wire Line
	9600 3250 10750 3250
Wire Wire Line
	10100 3350 10750 3350
Wire Wire Line
	9450 3450 10750 3450
$Comp
L power:GND #PWR0101
U 1 1 5E0AEC58
P 10700 3650
F 0 "#PWR0101" H 10700 3400 50  0001 C CNN
F 1 "GND" H 10705 3477 50  0000 C CNN
F 2 "" H 10700 3650 50  0001 C CNN
F 3 "" H 10700 3650 50  0001 C CNN
	1    10700 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 3550 10700 3550
Wire Wire Line
	10700 3550 10700 3650
$EndSCHEMATC
