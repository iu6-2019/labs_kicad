EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5DD4DDFC
P 9650 3850
F 0 "R1" V 9443 3850 50  0000 C CNN
F 1 "1k" V 9534 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 9580 3850 50  0001 C CNN
F 3 "~" H 9650 3850 50  0001 C CNN
	1    9650 3850
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5DD4ED18
P 9900 5400
F 0 "R2" V 9693 5400 50  0000 C CNN
F 1 "100" V 9784 5400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 9830 5400 50  0001 C CNN
F 3 "~" H 9900 5400 50  0001 C CNN
	1    9900 5400
	0    1    1    0   
$EndComp
$Comp
L MCU_Microchip_PIC12:PIC12C508A-ISN U1
U 1 1 5DD4FC6F
P 7900 4150
F 0 "U1" H 8100 4650 50  0000 C CNN
F 1 "PIC12C508A-ISN" H 7850 4650 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 8500 4800 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/40139e.pdf" H 7900 4150 50  0001 C CNN
	1    7900 4150
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5DD51511
P 9200 3850
F 0 "D1" H 9193 4066 50  0000 C CNN
F 1 "LED" H 9193 3975 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9200 3850 50  0001 C CNN
F 3 "~" H 9200 3850 50  0001 C CNN
	1    9200 3850
	1    0    0    -1  
$EndComp
$Comp
L my_library:my_connector_3 X1
U 1 1 5DD56152
P 8900 5100
F 0 "X1" H 8692 5215 50  0000 C CNN
F 1 "my_connector_3" H 8692 5124 50  0000 C CNN
F 2 "Connector:Banana_Jack_3Pin" H 8900 5100 50  0001 C CNN
F 3 "" H 8900 5100 50  0001 C CNN
	1    8900 5100
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5DE0F9FB
P 9950 3750
F 0 "#PWR05" H 9950 3600 50  0001 C CNN
F 1 "VCC" H 9967 3923 50  0000 C CNN
F 2 "" H 9950 3750 50  0001 C CNN
F 3 "" H 9950 3750 50  0001 C CNN
	1    9950 3750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5DE1020F
P 7900 3250
F 0 "#PWR01" H 7900 3100 50  0001 C CNN
F 1 "VCC" H 7917 3423 50  0000 C CNN
F 2 "" H 7900 3250 50  0001 C CNN
F 3 "" H 7900 3250 50  0001 C CNN
	1    7900 3250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5DE108F6
P 9500 5200
F 0 "#PWR03" H 9500 5050 50  0001 C CNN
F 1 "VCC" H 9517 5373 50  0000 C CNN
F 2 "" H 9500 5200 50  0001 C CNN
F 3 "" H 9500 5200 50  0001 C CNN
	1    9500 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5DE11F93
P 7900 4900
F 0 "#PWR02" H 7900 4650 50  0001 C CNN
F 1 "GND" H 7905 4727 50  0000 C CNN
F 2 "" H 7900 4900 50  0001 C CNN
F 3 "" H 7900 4900 50  0001 C CNN
	1    7900 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5DE124A2
P 9500 5600
F 0 "#PWR04" H 9500 5350 50  0001 C CNN
F 1 "GND" H 9505 5427 50  0000 C CNN
F 2 "" H 9500 5600 50  0001 C CNN
F 3 "" H 9500 5600 50  0001 C CNN
	1    9500 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 3850 8500 3850
Wire Wire Line
	8500 3850 8500 4050
Wire Wire Line
	9350 3850 9500 3850
Wire Wire Line
	9800 3850 9950 3850
Wire Wire Line
	9950 3850 9950 3750
Wire Wire Line
	7900 4900 7900 4750
Wire Wire Line
	7900 3250 7900 3550
Wire Wire Line
	9350 5300 9500 5300
Wire Wire Line
	9500 5300 9500 5200
Wire Wire Line
	9500 5600 9500 5500
Wire Wire Line
	9500 5500 9350 5500
Wire Wire Line
	9750 5400 9350 5400
Wire Wire Line
	8500 4150 8900 4150
Wire Wire Line
	10050 5400 10350 5400
Text Label 8650 4150 0    50   ~ 0
INPUT
Text Label 10100 5400 0    50   ~ 0
INPUT
Text Label 8500 3950 0    50   ~ 0
uCtoLED
Text Label 9400 3850 1    50   ~ 0
LEDtoR
Text Label 9650 5400 3    50   ~ 0
INPUTtoR
NoConn ~ 8500 4250
NoConn ~ 7300 4050
NoConn ~ 7300 4150
NoConn ~ 7300 4250
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5DE149FE
P 9950 4400
F 0 "#FLG01" H 9950 4475 50  0001 C CNN
F 1 "PWR_FLAG" H 9950 4573 50  0000 C CNN
F 2 "" H 9950 4400 50  0001 C CNN
F 3 "~" H 9950 4400 50  0001 C CNN
	1    9950 4400
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5DE167CA
P 10350 4400
F 0 "#FLG02" H 10350 4475 50  0001 C CNN
F 1 "PWR_FLAG" H 10350 4573 50  0000 C CNN
F 2 "" H 10350 4400 50  0001 C CNN
F 3 "~" H 10350 4400 50  0001 C CNN
	1    10350 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5DE17500
P 10350 4600
F 0 "#PWR07" H 10350 4350 50  0001 C CNN
F 1 "GND" H 10355 4427 50  0000 C CNN
F 2 "" H 10350 4600 50  0001 C CNN
F 3 "" H 10350 4600 50  0001 C CNN
	1    10350 4600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 5DE16CEB
P 9950 4600
F 0 "#PWR06" H 9950 4450 50  0001 C CNN
F 1 "VCC" H 9967 4773 50  0000 C CNN
F 2 "" H 9950 4600 50  0001 C CNN
F 3 "" H 9950 4600 50  0001 C CNN
	1    9950 4600
	1    0    0    1   
$EndComp
Wire Wire Line
	9950 4600 9950 4400
Wire Wire Line
	10350 4400 10350 4600
Text Notes 10200 3850 0    50   ~ 0
Комментарий
$EndSCHEMATC
